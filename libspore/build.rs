use std::{env, path::PathBuf};

fn main() {
    let avif_config = pkg_config::Config::new()
        .atleast_version("1.0.0")
        .probe("libavif")
        .unwrap();

    // The bindgen::Builder is the main entry point
    // to bindgen, and lets you build up options for
    // the resulting bindings.
    let bindings = bindgen::Builder::default()
        // The input header we would like to generate
        // bindings for.
        .header("wrapper.h")
        // Tell cargo to invalidate the built crate whenever any of the
        // included header files changed.
        .parse_callbacks(Box::new(bindgen::CargoCallbacks::new()))
        // Options
        .prepend_enum_name(false)
        .derive_default(true)
        .derive_debug(true)
        // Add Include path
        .clang_args(
            avif_config
                .include_paths
                .iter()
                .map(|include| format!("-I{}", include.display())),
        )
        // Finish the builder and generate the bindings.
        .generate()
        // Unwrap the Result and panic on failure.
        .expect("Unable to generate bindings");

    // Write the bindings to the $OUT_DIR/bindings.rs file.
    let out_path = PathBuf::from(env::var("OUT_DIR").unwrap());
    bindings
        .write_to_file(out_path.join("libavif.rs"))
        .expect("Couldn't write bindings!");
}
