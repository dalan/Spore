use approx::assert_abs_diff_eq;
use camino::{Utf8Path, Utf8PathBuf};
use celes::Country;
use chrono::{DateTime, Utc};
use geo_types::Point;
use image::GenericImageView;
use libspore::file::{Location, Resolution};
use libspore::{self, Collection, Database, File};
use libspore::{Item, file};
use std::fs;
use std::sync::Mutex;
use std::time::Duration;

fn check_thumbnail(file: &File, created: bool) {
    if created {
        // Test in db
        assert!(file.thumbnail().is_some());

        // Test in FS
        let thumbnail_path = file.thumbnail().unwrap();
        assert!(thumbnail_path.exists());

        // Open it
        let thumbnail = file.thumbnail_image().unwrap();

        // Test size
        let (thumbnail_width, thumbnail_height) = thumbnail.dimensions();
        assert!(thumbnail_width <= 500);
        assert!(thumbnail_height <= 500);

        // Test image ratio
        match file.typ() {
            file::Type::Image | file::Type::PanoramicImage => {
                let (img_width, img_height) = file.image().unwrap().unwrap().dimensions();
                assert_abs_diff_eq!(
                    thumbnail_width as f64 / thumbnail_height as f64,
                    img_width as f64 / img_height as f64,
                    epsilon = 0.01
                );
            }
            _ => (),
        }
    } else {
        assert!(file.thumbnail().is_none())
    }
}

fn check_collection(collection: &Collection, collection_path: &Utf8Path) {
    assert_eq!(collection.name(), "c_1");
    assert_eq!(collection.id(), 1);
    let main_folder = collection.folder();
    assert_eq!(main_folder.name(), "images_1");
    assert_eq!(main_folder.path(), collection_path);
    assert_eq!(main_folder.id(), 1);
}

fn check_collection_files(
    collection: &Collection,
    new_files: bool,
    thumbnail_created: bool,
    metadata_changed: bool,
) {
    let main_folder = collection.folder();
    assert_eq!(main_folder.folders().len(), 1);

    // Image 1 JPG with date from exif
    let img_1 = &main_folder.files()[1];
    assert_eq!(img_1.name(), "img_1");
    assert_eq!(img_1.id(), 1);
    if metadata_changed {
        assert_eq!(
            img_1.time(),
            &DateTime::<Utc>::from_timestamp(1668090085, 0)
                .unwrap()
                .naive_utc()
        );
    } else {
        assert_eq!(
            img_1.time(),
            &DateTime::<Utc>::from_timestamp(1662917125, 0)
                .unwrap()
                .naive_utc()
        );
    }
    assert_eq!(img_1.location(), None);
    assert_eq!(img_1.typ(), file::Type::Image);
    assert_eq!(img_1.resolution(), Some(Resolution::new(100, 150)).as_ref());
    assert_eq!(img_1.duration(), None);
    check_thumbnail(img_1, thumbnail_created);

    // Image 2 webp with date from exif
    let img_2 = &main_folder.files()[0];
    assert_eq!(img_2.name(), "img_2");
    assert_eq!(img_2.id(), 8);
    assert_eq!(
        img_2.time(),
        &DateTime::<Utc>::from_timestamp(1662809815, 0)
            .unwrap()
            .naive_utc()
    );
    assert_eq!(img_2.location(), None);
    if metadata_changed {
        assert_eq!(img_2.typ(), file::Type::PanoramicImage);
    } else {
        assert_eq!(img_2.typ(), file::Type::Image);
    }
    assert_eq!(img_2.resolution(), Some(Resolution::new(100, 150)).as_ref());
    assert_eq!(img_2.duration(), None);
    check_thumbnail(img_2, thumbnail_created);

    // Image 3 avif with date from exif
    let img_3 = &main_folder.files()[2];
    assert_eq!(img_3.name(), "img_3");
    assert_eq!(img_3.id(), 9);
    assert_eq!(
        img_3.time(),
        &DateTime::<Utc>::from_timestamp(1694345815, 0)
            .unwrap()
            .naive_utc()
    );
    assert_eq!(img_3.location(), None);
    assert_eq!(img_3.typ(), file::Type::Image);
    assert_eq!(img_3.resolution(), Some(Resolution::new(100, 150)).as_ref());
    assert_eq!(img_3.duration(), None);
    check_thumbnail(img_3, thumbnail_created);

    // Image 2 jxl with date from exif
    let img_4 = &main_folder.files()[3];
    assert_eq!(img_4.name(), "img_4");
    assert_eq!(img_4.id(), 10);
    assert_eq!(
        img_4.time(),
        &DateTime::<Utc>::from_timestamp(1694349415, 0)
            .unwrap()
            .naive_utc()
    );
    assert_eq!(img_4.location(), None);
    assert_eq!(img_4.typ(), file::Type::Image);
    //assert_eq!(img_4.resolution(), Some(Resolution::new(100, 150)).as_ref());
    assert_eq!(img_4.duration(), None);
    check_thumbnail(img_4, thumbnail_created);

    // Folder 1
    let folder_1 = main_folder.folder("folder_1").unwrap();
    assert_eq!(folder_1.folders().len(), if new_files { 2 } else { 1 });

    if new_files {
        // Image 5 JPG with date from exif
        let img_5 = &folder_1.files()[0];
        assert_eq!(img_5.name(), "img_5");
        assert_eq!(img_5.id(), 13);
        assert_eq!(
            img_5.time(),
            &DateTime::<Utc>::from_timestamp(1663427655, 0)
                .unwrap()
                .naive_utc()
        );
        assert_eq!(img_5.location(), None);
        assert_eq!(img_5.typ(), file::Type::Image);
        assert_eq!(img_5.resolution(), Some(Resolution::new(100, 150)).as_ref());
        assert_eq!(img_5.duration(), None);
        check_thumbnail(img_5, thumbnail_created);
    }

    // Folder 2
    let folder_2 = folder_1.folder("folder_2").unwrap();
    assert_eq!(folder_2.folders().len(), 0);

    // Image 3 with date/time from filename and GPS infos
    let img_3 = &folder_2.files()[0];
    assert_eq!(img_3.name(), "img_3");
    assert_eq!(img_3.id(), 2);
    assert_eq!(
        img_3.time(),
        &DateTime::<Utc>::from_timestamp(1663020876, 0)
            .unwrap()
            .naive_utc()
    );
    if metadata_changed {
        assert_eq!(
            img_3.location().unwrap(),
            &Location {
                point: Point::new(-48.223333333333336, 9.880833333333333),
                city: "Hermanus".to_string(),
                admin1: "Western Cape".to_string(),
                admin2: "Overberg District Municipality".to_string(),
                country: Country::south_africa()
            }
        );
    } else {
        assert_eq!(
            img_3.location().unwrap(),
            &Location {
                point: Point::new(48.223333333333336, 9.880833333333333),
                city: "Laupheim".to_string(),
                admin1: "Baden-Wuerttemberg".to_string(),
                admin2: "Tuebingen Region".to_string(),
                country: Country::germany()
            }
        );
    }
    assert_eq!(img_3.typ(), file::Type::Image);
    assert_eq!(img_3.resolution(), Some(Resolution::new(100, 150)).as_ref());
    assert_eq!(img_3.duration(), None);
    check_thumbnail(img_3, thumbnail_created);

    // Image 4 with date from filename and panoramic
    let img_4 = &folder_2.files()[if new_files { 2 } else { 1 }];
    assert_eq!(img_4.name(), "img_4");
    assert_eq!(img_4.id(), 3);
    assert_eq!(
        img_4.time(),
        &DateTime::<Utc>::from_timestamp(1663372800, 0)
            .unwrap()
            .naive_utc()
    );
    assert_eq!(img_4.location(), None);
    assert_eq!(img_4.typ(), file::Type::PanoramicImage);
    assert_eq!(
        img_4.resolution(),
        Some(Resolution::new(1000, 1500)).as_ref()
    );
    assert_eq!(img_4.duration(), None);
    check_thumbnail(img_4, thumbnail_created);

    // GPX
    let gpx_1 = &folder_2.files()[if new_files { 3 } else { 2 }];
    assert_eq!(gpx_1.name(), "gpx_1");
    assert_eq!(gpx_1.id(), 4);
    assert_eq!(
        gpx_1.time(),
        &DateTime::<Utc>::from_timestamp(1680791809, 0)
            .unwrap()
            .naive_utc()
    );
    assert_eq!(
        gpx_1.location().unwrap(),
        &Location {
            point: Point::new(51.206933, 9.712004),
            city: "Hessisch Lichtenau".to_string(),
            admin1: "Hesse".to_string(),
            admin2: "".to_string(),
            country: Country::germany()
        }
    );
    assert_eq!(gpx_1.duration(), Some(Duration::from_secs(12)).as_ref());
    check_thumbnail(gpx_1, thumbnail_created);

    // Video 1 MKV with date from filesystem
    let video_1 = &folder_2.files()[if new_files { 6 } else { 5 }];
    assert_eq!(video_1.name(), "video_1");
    assert_eq!(video_1.id(), 5);
    assert!(
        video_1.time()
            > &DateTime::<Utc>::from_timestamp(1663356570, 469731291)
                .unwrap()
                .naive_utc()
    );
    assert_eq!(video_1.location(), None);
    assert_eq!(video_1.typ(), file::Type::Video);
    assert_eq!(
        video_1.resolution(),
        Some(Resolution::new(100, 150)).as_ref()
    );
    assert_eq!(video_1.duration(), Some(Duration::from_millis(40)).as_ref());
    check_thumbnail(video_1, thumbnail_created);

    // Video 2 MKV with date from file
    let video_2 = &folder_2.files()[if new_files { 4 } else { 3 }];
    assert_eq!(video_2.name(), "video_2");
    assert_eq!(video_2.id(), 6);
    assert_eq!(
        video_2.time(),
        &DateTime::<Utc>::from_timestamp(1687530236, 000000000)
            .unwrap()
            .naive_utc()
    );
    assert_eq!(video_2.location(), None);
    assert_eq!(video_2.typ(), file::Type::Video);
    assert_eq!(
        video_2.resolution(),
        Some(Resolution::new(100, 150)).as_ref()
    );
    assert_eq!(video_2.duration(), Some(Duration::from_millis(40)).as_ref());
    check_thumbnail(video_2, thumbnail_created);

    // Video 2 MKV with date from file and location
    let video_3 = &folder_2.files()[if new_files { 5 } else { 4 }];
    assert_eq!(video_3.name(), "video_3");
    assert_eq!(video_3.id(), 7);
    assert_eq!(
        video_3.time(),
        &DateTime::<Utc>::from_timestamp(1687713732, 000000000)
            .unwrap()
            .naive_utc()
    );
    assert_eq!(
        video_3.location(),
        Some(&Location {
            point: Point::new(48.856614, 2.352221),
            city: "Paris".to_string(),
            admin1: "Ile-de-France".to_string(),
            admin2: "Paris".to_string(),
            country: Country::france()
        })
    );
    assert_eq!(video_3.typ(), file::Type::Video);
    assert_eq!(
        video_3.resolution(),
        Some(Resolution::new(100, 150)).as_ref()
    );
    assert_eq!(video_3.duration(), Some(Duration::from_millis(40)).as_ref());
    check_thumbnail(video_3, thumbnail_created);

    if new_files {
        // Image 6
        let img_6 = &folder_2.files()[1];
        assert_eq!(img_6.name(), "img_6");
        assert_eq!(img_6.id(), 11);
        assert_eq!(
            img_6.time(),
            &DateTime::<Utc>::from_timestamp(1663254865, 0)
                .unwrap()
                .naive_utc()
        );
        assert_eq!(img_6.location(), None);
        assert_eq!(img_6.typ(), file::Type::Image);
        assert_eq!(img_6.resolution(), Some(Resolution::new(100, 150)).as_ref());
        assert_eq!(img_6.duration(), None);
        check_thumbnail(img_6, thumbnail_created);
    }

    if new_files {
        let folder_3 = folder_1.folder("folder_3").unwrap();
        assert_eq!(folder_3.folders().len(), 0);

        // Image 7
        let img_7 = &folder_3.files()[0];
        assert_eq!(img_7.name(), "img_7");
        assert_eq!(img_7.id(), 12);
        assert_eq!(
            img_7.time(),
            &DateTime::<Utc>::from_timestamp(1664816905, 0)
                .unwrap()
                .naive_utc()
        );
        assert_eq!(img_7.location(), None);
        assert_eq!(img_7.typ(), file::Type::Image);
        assert_eq!(img_7.resolution(), Some(Resolution::new(100, 150)).as_ref());
        assert_eq!(img_7.duration(), None);
        check_thumbnail(img_7, thumbnail_created);
    }
}

fn change_metadatas(path: &Utf8Path) {
    // Date changed on img_1
    let file_1_path = path.join("2023-12-16 20∶46∶00 - img_1.jpg");
    let img_1_metadata = rexiv2::Metadata::new_from_path(&file_1_path).unwrap();
    img_1_metadata
        .set_tag_string("Exif.Photo.DateTimeOriginal", "2022:11:10 14:21:25")
        .unwrap();
    img_1_metadata.save_to_file(&file_1_path).unwrap();

    // Type change on img_2
    let file_2_path = path.join("img_2.webp");
    let img_2_metadata = rexiv2::Metadata::new_from_path(&file_2_path).unwrap();
    img_2_metadata
        .set_tag_numeric("Xmp.GPano.UsePanoramaViewer", 1)
        .unwrap();
    img_2_metadata.save_to_file(&file_2_path).unwrap();

    // Location change on img_3
    let file_3_path = path.join("folder_1/folder_2/2022-09-12 22∶14∶36 - img_3.jpg");
    let img_3_metadata = rexiv2::Metadata::new_from_path(&file_3_path).unwrap();
    img_3_metadata
        .set_tag_string("Exif.GPSInfo.GPSLatitudeRef", "S")
        .unwrap();
    img_3_metadata.save_to_file(&file_3_path).unwrap();
}

#[test]
fn test_db() {
    libspore::init().unwrap();

    let tmp_dir = tempfile::tempdir().unwrap();
    let tmp_path = dunce::canonicalize(tmp_dir.path()).unwrap();
    let db_path = Utf8PathBuf::from_path_buf(tmp_path.join("test.db3")).unwrap();
    let thumbnails_dir = Utf8PathBuf::from_path_buf(tmp_path.join("thumbnails")).unwrap();
    let collection_1_path = Utf8PathBuf::from_path_buf(tmp_path.join("images_1")).unwrap();
    fs_extra::dir::copy(
        "tests/images_1",
        &tmp_dir,
        &fs_extra::dir::CopyOptions::new(),
    )
    .unwrap();
    fs::create_dir(&thumbnails_dir).unwrap();

    // Create database
    let mut db = Database::new(&db_path, "blop", &thumbnails_dir).unwrap();
    assert_eq!(db.name(), "blop");

    // Add a collection
    let collection_index = db.add_collection("c_1", &collection_1_path).unwrap();
    assert_eq!(collection_index, 0);
    check_collection(&db.collections()[collection_index], &collection_1_path);

    // Update the collection and test
    db.update(0, None, |_state| {}).unwrap();
    check_collection_files(&db.collections()[0], false, false, false);

    // Update the collection with now files and folder
    fs::copy(
        "tests/images_2/img_5.jpg",
        collection_1_path.join_os("folder_1/img_5.jpg"),
    )
    .unwrap();
    fs::copy(
        "tests/images_2/img_6.jpg",
        collection_1_path.join_os("folder_1/folder_2/img_6.jpg"),
    )
    .unwrap();
    fs::create_dir(collection_1_path.join_os("folder_1/folder_3")).unwrap();
    fs::copy(
        "tests/images_2/img_7.jpg",
        collection_1_path.join_os("folder_1/folder_3/img_7.jpg"),
    )
    .unwrap();
    db.update(
        collection_index,
        Some(&collection_1_path.join("folder_1")),
        |_state| {},
    )
    .unwrap();
    check_collection_files(&db.collections()[0], true, false, false);

    // Update the collection with deleted files and folders
    fs::remove_file(collection_1_path.join_os("folder_1/img_5.jpg")).unwrap();
    fs::remove_file(collection_1_path.join_os("folder_1/folder_2/img_6.jpg")).unwrap();
    fs::remove_file(collection_1_path.join_os("folder_1/folder_3/img_7.jpg")).unwrap();
    assert!(
        db.update(
            collection_index,
            Some(Utf8Path::new(if cfg!(unix) { "/f" } else { "c:\\f" })),
            |_state| {}
        )
        .is_err()
    );
    db.update(
        collection_index,
        Some(Utf8Path::new("folder_1")),
        |_state| {},
    )
    .unwrap();
    check_collection_files(&db.collections()[0], false, false, false);

    // Generate thumbnails
    db.generate_thumbnail(collection_index, Mutex::new(|_| ()))
        .unwrap();
    check_collection_files(&db.collections()[0], false, true, false);

    // Update metadatas
    change_metadatas(&collection_1_path);
    assert!(
        db.update_metadatas(
            collection_index,
            Some(Utf8Path::new(if cfg!(unix) { "/f" } else { "c:\\f" })),
            |_state| {}
        )
        .is_err()
    );
    db.update_metadatas(collection_index, None, |_| {}).unwrap();
    check_collection_files(&db.collections()[0], false, true, true);

    // Close database, reopen it and check the values
    db.close().unwrap();
    let mut db = Database::open(&db_path, &thumbnails_dir).unwrap();
    assert_eq!(db.name(), "blop");
    check_collection(&db.collections()[0], &collection_1_path);
    check_collection_files(&db.collections()[0], false, true, true);

    // Remove collection
    db.remove_collection(0).unwrap();
    assert_eq!(db.collections().len(), 0);

    // Close database
    db.close().unwrap();

    tmp_dir.close().unwrap();
}
