use crate::database::UpdateState;
use crate::error::Result;
use crate::file::{Resolution, Type};
use crate::{Folder, Item, file, folder};
use camino::{Utf8Path, Utf8PathBuf};
use chrono::{DateTime, NaiveDate, NaiveDateTime, TimeZone, Utc};
use directories::ProjectDirs;
use geo_types::Point;
use gpx::Gpx;
use lazy_regex::regex;
use lazy_static::lazy_static;
use log::{error, warn};
use rusqlite::Statement;
use std::{
    collections::HashMap,
    fs::{self, File},
    io::BufReader,
    time::Duration,
};

pub(crate) fn read_folder(
    main_folder: &mut Folder,
    folders_datas: &mut HashMap<i64, Vec<folder::Datas>>,
    files_datas: &mut HashMap<i64, Vec<file::Datas>>,
    root_collection_dir: &Utf8Path,
    thumbnails_dir: &Utf8Path,
) -> Result<()> {
    // Get files
    if let Some(folder_files_datas) = files_datas.remove(&main_folder.id()) {
        for file_data in folder_files_datas {
            let filename = Utf8PathBuf::from(&file_data.filename);

            // Check thumbnail
            let mut thumbnail_path = thumbnails_dir.join(
                main_folder
                    .path()
                    .join(&filename)
                    .strip_prefix(root_collection_dir)
                    .unwrap(),
            );
            thumbnail_path.set_extension("avif");
            let opt_thumbnail = if thumbnail_path.exists() {
                Some(thumbnail_path)
            } else {
                None
            };

            main_folder.add_file(file_data, opt_thumbnail);
        }
    }

    // Get folders
    if let Some(current_folder_datas) = folders_datas.remove(&main_folder.id()) {
        for folder_data in current_folder_datas {
            main_folder.add_folder(folder_data);
        }
    }

    // Read subfolders
    for folder in main_folder.folders_mut() {
        read_folder(
            folder,
            folders_datas,
            files_datas,
            root_collection_dir,
            thumbnails_dir,
        )?;
    }

    Ok(())
}

/// Get the default database directory
///
/// Return (db_dir, thumbnails_dir)
pub(crate) fn get_default_dirs() -> Result<(Utf8PathBuf, Utf8PathBuf)> {
    let proj_dirs = ProjectDirs::from("fr", "dalan", "spore").unwrap();
    let data_dir = proj_dirs.data_dir();
    fs::create_dir_all(data_dir)?;
    let thumbnails_dir = proj_dirs.cache_dir().join("thumbnails");
    fs::create_dir_all(&thumbnails_dir)?;
    Ok((
        Utf8PathBuf::from_path_buf(data_dir.join("collections.db3")).unwrap(),
        Utf8PathBuf::from_path_buf(thumbnails_dir).unwrap(),
    ))
}

/// Delete folders
pub(crate) fn delete_folder(
    main_folder: &Folder,
    delete_folders_stm: &mut Statement,
    delete_files_stm: &mut Statement,
) -> Result<()> {
    for file in main_folder.files() {
        delete_files_stm.execute((file.id(),))?;
    }

    for folder in main_folder.folders() {
        delete_folder(folder, delete_folders_stm, delete_files_stm)?;
    }

    delete_folders_stm.execute((main_folder.id(),))?;

    Ok(())
}

pub(crate) fn delete_empty_folders<F>(
    delete_stm: &mut Statement,
    main_folder: &mut Folder,
    progress_callback: &mut F,
) where
    F: FnMut(UpdateState),
{
    let mut folders_to_remove = Vec::new();

    for (folder_index, folder) in main_folder.folders_mut().iter_mut().enumerate() {
        //progress_callback(UpdateState::Progress);
        if folder.folders().is_empty() && folder.files().is_empty() {
            match delete_stm.execute((folder.id(),)) {
                Ok(1) => {
                    // Delete
                    folders_to_remove.push(folder_index);
                }
                Ok(n) => {
                    error!(
                        "Delete {} entries instead of 1 when deleting folder id {}",
                        n,
                        folder.id()
                    );
                }
                Err(e) => warn!("Cannot delete folder `{}` ({})", folder.name(), e),
            }
        } else {
            delete_empty_folders(delete_stm, folder, progress_callback);
        }
    }

    // Remove folder in reverse order to avoid index issue
    for folder_remove_index in folders_to_remove.iter().rev() {
        main_folder.remove_folder(*folder_remove_index);
    }
}

pub(crate) fn get_files_info(file_path: &Utf8Path) -> Result<file::Datas> {
    let mut location = None;
    let mut date = None;
    let mut resolution = None;
    let mut duration = None;
    let mut name = file_path.file_stem().unwrap_or("").to_string();

    // Infer type
    let infer_type = infer::get_from_path(file_path)?.unwrap();

    // Try to get missing type
    let mut typ = match infer_type.matcher_type() {
        infer::MatcherType::Video => Some(file::Type::Video),
        infer::MatcherType::Image => Some(file::Type::Image),
        _ => None,
    };

    // Get XMP/EXIF metadata
    if let Some(file::Type::Image) = typ {
        if let Ok(metadata) = rexiv2::Metadata::new_from_path(file_path.as_std_path()) {
            location = metadata
                .get_gps_info()
                .map(|gps_info| Point::new(gps_info.latitude, gps_info.longitude));

            date = metadata
                .get_tag_string("Exif.Photo.DateTimeOriginal")
                .ok()
                .and_then(|exif_date_str| {
                    NaiveDateTime::parse_from_str(&exif_date_str, "%Y:%m:%d %H:%M:%S").ok()
                });

            if metadata.get_tag_numeric("Xmp.GPano.UsePanoramaViewer") == 1 {
                typ = Some(file::Type::PanoramicImage)
            }

            resolution = match (metadata.get_pixel_width(), metadata.get_pixel_height()) {
                (0, 0) => None,
                (x, y) => Some(Resolution::new(x, y)),
            };
        }
    }

    // Try to get missing date from filename
    let date_time_regex = regex!(r"^(\d{4}).?(\d{2}).?(\d{2}) ?(\d{2}).?(\d{2}).?(\d{2})");
    let date_regex = regex!(r"^(\d{4}).?(\d{2}).?(\d{2})");

    let file_date_opt = if let Some(captures) = date_time_regex.captures(&name) {
        //name.drain(captures.get(0).unwrap().range());
        NaiveDate::from_ymd_opt(
            captures[1].parse().unwrap(),
            captures[2].parse().unwrap(),
            captures[3].parse().unwrap(),
        )
        .and_then(|date| {
            date.and_hms_opt(
                captures[4].parse().unwrap(),
                captures[5].parse().unwrap(),
                captures[6].parse().unwrap(),
            )
        })
        .map(|date| (date, captures.get(0).unwrap().range()))
    } else if let Some(captures) = date_regex.captures(&name) {
        NaiveDate::from_ymd_opt(
            captures[1].parse().unwrap(),
            captures[2].parse().unwrap(),
            captures[3].parse().unwrap(),
        )
        .map(|date| date.and_hms_opt(0, 0, 0).unwrap())
        .map(|date| (date, captures.get(0).unwrap().range()))
    } else {
        None
    };

    if let Some((file_date, file_date_range)) = file_date_opt {
        // Set date if no date is set
        date = date.or(Some(file_date));

        // Delete date from name
        name.drain(file_date_range);
        name = name
            .trim_start()
            .trim_start_matches('-')
            .trim_start()
            .to_string();
    }

    // Read MKV
    match infer_type.mime_type() {
        "video/x-matroska" | "video/webm" => {
            let (mkv_loc, mkv_date, mkv_res, mkv_duration) = get_mkv_file_info(file_path)?;
            resolution = resolution.or(mkv_res);
            duration = duration.or(mkv_duration);
            date = date.or(mkv_date);
            location = location.or(mkv_loc);
        }
        _ => (),
    }

    // Read GPX
    if file_path.extension().unwrap_or("") == "gpx" {
        let gpx: Gpx = gpx::read(BufReader::new(File::open(file_path)?))?;
        if let Some(track) = gpx.tracks.first() {
            if !track.segments.is_empty() {
                if let Some(first_waypoint) = track.segments.first().unwrap().points.first() {
                    // Get location
                    let gpx_point = first_waypoint.point();
                    location = Some(Point::new(gpx_point.y(), gpx_point.x()));

                    // Get date
                    if let Some(time) = first_waypoint.time {
                        date = Some(DateTime::parse_from_rfc3339(&time.format()?)?.naive_utc());

                        // Get duration
                        if let Some(last_waypoint) = track.segments.last().unwrap().points.last() {
                            if let Some(last_time) = last_waypoint.time {
                                let last_date =
                                    DateTime::parse_from_rfc3339(&last_time.format()?)?.naive_utc();
                                duration = Some((last_date - date.unwrap()).to_std().unwrap())
                            }
                        }
                    }
                }
            }
        }

        typ = Some(Type::Gpx);
    }

    // Get missing date from file metadata
    if date.is_none() {
        let metadata = fs::metadata(file_path)?;
        let timestamp = metadata
            .created()
            .unwrap_or_else(|_| metadata.modified().unwrap())
            .duration_since(std::time::UNIX_EPOCH)?;
        date = Some(
            DateTime::<Utc>::from_timestamp(timestamp.as_secs() as i64, timestamp.subsec_nanos())
                .unwrap()
                .naive_utc(),
        );
    };

    Ok(file::Datas {
        id: -1,
        filename: file_path.into(),
        name,
        time: date.unwrap(),
        location,
        typ: typ.unwrap(),
        resolution,
        duration,
    })
}

fn get_mkv_file_info(
    file_path: &Utf8Path,
) -> Result<(
    Option<Point>,
    Option<NaiveDateTime>,
    Option<Resolution>,
    Option<Duration>,
)> {
    let mkv = matroska::open(file_path)?;

    let duration = mkv.info.duration;

    // Get resolution from first video track
    let resolution = mkv
        .video_tracks()
        .next()
        .map(|video_track| match &video_track.settings {
            matroska::Settings::Video(video) => match (video.display_width, video.display_height) {
                (Some(width), Some(height)) => Resolution::new(
                    (video.pixel_width * width / height) as i32,
                    (video.pixel_height) as i32,
                ),
                _ => Resolution::new(video.pixel_width as i32, video.pixel_height as i32),
            },
            _ => unreachable!(),
        });

    // Get location (ISO 6709)
    let gnss_regex = regex!(r"^([+-][0-9]{2}\.[0-9]+)([+-][0-9]{3}\.[0-9]+)/$");
    let loc = mkv
        .tags
        .iter()
        .find_map(|tag| {
            tag.simple
                .iter()
                .find(|simple_tag| simple_tag.name == "_GNSS_LOCATION")
        })
        .and_then(|gnss_simple_tag| {
            if let Some(matroska::TagValue::String(value)) = &gnss_simple_tag.value {
                gnss_regex.captures(value).map(|captures| {
                    let lat: f64 = captures[1].parse().unwrap();
                    let lon: f64 = captures[2].parse().unwrap();
                    Point::new(lat, lon)
                })
            } else {
                None
            }
        });

    // Get date time
    lazy_static! {
        static ref TZ_FINDER: tzf_rs::Finder = tzf_rs::Finder::new();
    }
    let date = mkv.info.date_utc.map(|date_utc| {
        let utc_date_time: DateTime<Utc> = date_utc.into();

        // If a location is set, use it to convert the UTC date/time
        if let Some(loc) = loc {
            let tz: chrono_tz::Tz = TZ_FINDER.get_tz_name(loc.y(), loc.x()).parse().unwrap();
            let loc_date = tz.from_utc_datetime(&utc_date_time.naive_utc());
            loc_date.naive_local()
        } else {
            utc_date_time.naive_utc()
        }
    });

    Ok((loc, date, resolution, duration))
}
