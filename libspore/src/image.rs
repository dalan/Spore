use crate::{Error, error::Result, libavif::*};
use camino::Utf8Path;
use fast_image_resize::{self as fr, ResizeAlg, ResizeOptions};
use image::{DynamicImage, ImageBuffer, ImageDecoder, ImageReader, RgbImage, Rgba, RgbaImage};
use jxl_oxide::{JxlThreadPool, integration::JxlDecoder};
use log::{debug, trace, warn};
use rust_embed::RustEmbed;
use std::{
    fs,
    io::Cursor,
    ptr::{self, slice_from_raw_parts},
    slice,
    thread::available_parallelism,
};

/// Fast resize using RGB8
pub fn resize(img: &DynamicImage, max_width: u32, max_height: u32) -> Result<RgbImage> {
    // Create source image
    let width = img.width();
    let height = img.height();
    let src_image = fr::images::Image::from_vec_u8(
        width,
        height,
        img.to_rgb8().into_raw(),
        fr::PixelType::U8x3,
    )?;

    // Create container for data of destination image
    let (dst_width, dst_height) = if width > height {
        (max_width, (max_width * img.height()) / img.width())
    } else {
        ((max_height * img.width()) / img.height(), max_height)
    };
    let mut dst_image = fr::images::Image::new(dst_width, dst_height, src_image.pixel_type());

    // Create Resizer instance and resize source image into buffer of destination image
    let mut resizer = fr::Resizer::new();
    resizer.resize(
        &src_image,
        &mut dst_image,
        Some(
            &ResizeOptions::default()
                .resize_alg(ResizeAlg::Convolution(
                    fast_image_resize::FilterType::Lanczos3,
                ))
                .fit_into_destination(None),
        ),
    )?;

    // Create Dynamic image
    let img_buffer =
        RgbImage::from_raw(dst_image.width(), dst_image.height(), dst_image.into_vec()).unwrap();

    Ok(img_buffer)
}

/// Load an image
pub fn load(path: &Utf8Path, mt: bool) -> Result<DynamicImage> {
    trace!("Read data from `{}`", path);
    let file_datas = fs::read(path)?;

    if let Some(infer_type) = infer::get(&file_datas) {
        let (mut img, icc) = match infer_type.mime_type() {
            "image/avif" => {
                trace!("Decode avif");
                load_avif(&file_datas, mt)?
            }
            "image/jxl" => {
                let mut decoder = if mt {
                    JxlDecoder::new(Cursor::new(file_datas))?
                } else {
                    JxlDecoder::with_thread_pool(Cursor::new(file_datas), JxlThreadPool::none())?
                };
                let icc = decoder.icc_profile()?;
                (DynamicImage::from_decoder(decoder)?, icc)
            }
            _ => {
                let mut decoder = ImageReader::new(Cursor::new(file_datas))
                    .with_guessed_format()?
                    .into_decoder()?;
                let icc = decoder.icc_profile()?;
                (DynamicImage::from_decoder(decoder)?, icc)
            }
        };

        // Set ICC profile
        if let Some(icc) = icc {
            debug!("Use ICC profile");

            let srgb_profile = lcms2::Profile::new_srgb();
            let icc_profile = lcms2::Profile::new_icc(&icc)?;

            match &mut img {
                DynamicImage::ImageRgb8(image_buffer) => {
                    let t = lcms2::Transform::new(
                        &icc_profile,
                        lcms2::PixelFormat::RGB_8,
                        &srgb_profile,
                        lcms2::PixelFormat::RGB_8,
                        lcms2::Intent::Perceptual,
                    )?;
                    t.transform_in_place(image_buffer);
                }
                DynamicImage::ImageRgba8(image_buffer) => {
                    let t = lcms2::Transform::new(
                        &icc_profile,
                        lcms2::PixelFormat::RGBA_8,
                        &srgb_profile,
                        lcms2::PixelFormat::RGBA_8,
                        lcms2::Intent::Perceptual,
                    )?;
                    t.transform_in_place(image_buffer);
                }
                DynamicImage::ImageRgb16(image_buffer) => {
                    let t = lcms2::Transform::new(
                        &icc_profile,
                        lcms2::PixelFormat::RGB_16,
                        &srgb_profile,
                        lcms2::PixelFormat::RGB_16,
                        lcms2::Intent::Perceptual,
                    )?;
                    t.transform_in_place(bytemuck::cast_slice_mut::<u16, u8>(
                        image_buffer.as_mut(),
                    ));
                }
                DynamicImage::ImageRgba16(image_buffer) => {
                    let t = lcms2::Transform::new(
                        &icc_profile,
                        lcms2::PixelFormat::RGBA_16,
                        &srgb_profile,
                        lcms2::PixelFormat::RGBA_16,
                        lcms2::Intent::Perceptual,
                    )?;
                    t.transform_in_place(bytemuck::cast_slice_mut::<u16, u8>(
                        image_buffer.as_mut(),
                    ));
                }
                _ => warn!("ICC profile not supported on this image type"),
            }
        }

        Ok(img)
    } else {
        Err(Error::NoThumbnail)
    }
}

#[derive(RustEmbed)]
#[folder = "icc"]
struct EmbedICC;

fn get_icc(color_primary: u16) -> Option<Vec<u8>> {
    match color_primary {
        9 => Some(EmbedICC::get("BT2020.icc").unwrap().data.to_vec()),
        12 => Some(EmbedICC::get("P3-D65.icc").unwrap().data.to_vec()),
        _ => None,
    }
}

/// Load avif image and its ICC profile
fn load_avif(file_data: &[u8], mt: bool) -> Result<(DynamicImage, Option<Vec<u8>>)> {
    let nb_thread = if mt {
        available_parallelism().unwrap().into()
    } else {
        1
    };

    /* Create decoder  */
    let decoder = unsafe { avifDecoderCreate().as_mut().unwrap() };
    decoder.codecChoice = AVIF_CODEC_CHOICE_DAV1D;
    decoder.maxThreads = nb_thread as i32;
    decoder.ignoreExif = AVIF_TRUE as i32;
    decoder.ignoreXMP = AVIF_TRUE as i32;

    /* Decode image  */
    let image = unsafe { avifImageCreateEmpty().as_mut().unwrap() };
    let result =
        unsafe { avifDecoderReadMemory(decoder, image, file_data.as_ptr(), file_data.len()) };

    /* Destroy decoder */
    unsafe { avifDecoderDestroy(decoder) };

    /* Check result */
    if result != AVIF_RESULT_OK {
        return Err(Error::Avif {});
    }

    /* Get ICC */
    let icc = if image.icc.data.is_null() {
        get_icc(image.colorPrimaries)
    } else {
        Some(unsafe { slice::from_raw_parts(image.icc.data, image.icc.size).to_vec() })
    };

    /* Convert to RGB image */
    let mut rgb_image = avifRGBImage::default();
    unsafe { avifRGBImageSetDefaults(&mut rgb_image, image) };
    rgb_image.maxThreads = nb_thread as i32;
    rgb_image.format = AVIF_RGB_FORMAT_RGBA;

    let img = if image.depth == 8 {
        rgb_image.depth = 8;
        rgb_image.rowBytes = image.width * 4;
        rgb_image.avoidLibYUV = AVIF_FALSE as i32;
        let mut buffer = RgbaImage::new(image.width, image.height);
        rgb_image.pixels = buffer.as_mut_ptr();

        /* Convert */
        unsafe { avifImageYUVToRGB(image, &mut rgb_image) };
        unsafe { avifImageDestroy(image) };

        DynamicImage::ImageRgba8(buffer)
    } else {
        rgb_image.depth = 16;
        rgb_image.rowBytes = image.width * 8;
        let mut buffer = ImageBuffer::<Rgba<u16>, Vec<u16>>::new(image.width, image.height);
        rgb_image.pixels = buffer.as_mut_ptr() as *mut u8;

        /* Convert */
        unsafe { avifImageYUVToRGB(image, &mut rgb_image) };
        unsafe { avifImageDestroy(image) };

        DynamicImage::ImageRgba16(buffer)
    };

    Ok((img, icc))
}

/// Save image as avif file at 8bit resolution
pub fn save_avif(
    img: &DynamicImage,
    path: &Utf8Path,
    mt: bool,
    quality: i32,
    speed: i32,
) -> Result<()> {
    let nb_thread = if mt {
        available_parallelism().unwrap().into()
    } else {
        1
    };

    /* Create RGB image */
    let rgba_image = img.to_rgba8();
    let mut rgb_image = avifRGBImage::default();
    rgb_image.maxThreads = nb_thread as i32;
    rgb_image.format = AVIF_RGB_FORMAT_RGBA;
    rgb_image.depth = 8;
    rgb_image.width = img.width();
    rgb_image.height = img.height();
    rgb_image.chromaUpsampling = AVIF_CHROMA_UPSAMPLING_BILINEAR;
    rgb_image.ignoreAlpha = 0;
    rgb_image.alphaPremultiplied = 0;
    rgb_image.isFloat = 0;
    rgb_image.rowBytes = img.width() * 4;
    rgb_image.pixels = rgba_image.as_ptr() as *mut u8;
    rgb_image.avoidLibYUV = AVIF_FALSE as i32;
    rgb_image.chromaDownsampling = AVIF_CHROMA_DOWNSAMPLING_BEST_QUALITY;

    /* Convert to YUV */
    let image = unsafe { avifImageCreate(img.width(), img.height(), 8, AVIF_PIXEL_FORMAT_YUV444) };
    unsafe { avifImageAllocatePlanes(image, AVIF_PLANES_YUV) };
    unsafe { avifImageRGBToYUV(image, &rgb_image) };

    /* Create encoder */
    let encoder = unsafe { avifEncoderCreate().as_mut().unwrap() };
    encoder.quality = quality;
    encoder.speed = speed;
    encoder.maxThreads = nb_thread as i32;
    encoder.codecChoice = AVIF_CODEC_CHOICE_RAV1E;

    /* Encode image */
    let mut avif_data = avifRWData {
        data: ptr::null_mut(),
        size: 0,
    };
    let result = unsafe { avifEncoderWrite(encoder, image, &mut avif_data) };

    let ret = if result == AVIF_RESULT_OK {
        /* Write image */
        let data = unsafe { &*slice_from_raw_parts(avif_data.data, avif_data.size) };
        fs::write(path, data)?;

        Ok(())
    } else {
        Err(Error::Avif {})
    };

    /* Destroy object */
    unsafe {
        avifRWDataFree(&mut avif_data);
        avifEncoderDestroy(encoder);
        avifImageDestroy(image);
    }

    ret
}
