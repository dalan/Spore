use crate::database::Item;
use crate::file::{self, File, Type};
use camino::{Utf8Path, Utf8PathBuf};
use chrono::Datelike;
use smallvec::SmallVec;
use std::{cmp::Ordering, iter::FusedIterator};

pub(crate) struct Datas {
    pub id: i64,
    pub name: Utf8PathBuf,
}

/// A folder
///  A folder
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct Folder {
    /// ID in database
    id: i64,
    /// Folder full path
    path: Utf8PathBuf,
    /// List of files
    files: Vec<File>,
    /// List of subfolders
    folders: Vec<Folder>,
}

impl Folder {
    /// Create a folder
    pub(crate) fn new(data: Datas) -> Self {
        Self {
            id: data.id,
            path: data.name,
            files: Vec::new(),
            folders: Vec::new(),
        }
    }

    /// Get a reference to the folder's name.
    pub fn name(&self) -> &str {
        self.path.file_name().unwrap()
    }

    /// Get a reference to the folder's path.
    pub fn path(&self) -> &Utf8Path {
        &self.path
    }

    /// Get a reference to the folder's files.
    pub fn files(&self) -> &[File] {
        &self.files
    }

    /// Get a reference to the folder's files.
    pub fn files_mut(&mut self) -> &mut [File] {
        &mut self.files
    }

    /// Get a reference to the folder's folders.
    pub fn folders(&self) -> &[Folder] {
        &self.folders
    }

    /// Get a reference to the folder's folders.
    pub fn folders_mut(&mut self) -> &mut [Folder] {
        &mut self.folders
    }

    /// Get a file
    pub fn file(&self, filename: &str) -> Option<(usize, &File)> {
        self.files
            .iter()
            .enumerate()
            .find(|(_, file)| file.filename().file_name().unwrap() == filename)
    }

    /// Get a subfolder
    pub fn folder(&self, name: &str) -> Option<&Folder> {
        self.folders.iter().find(|folder| folder.name() == name)
    }

    /// Get a mutable subfolder
    pub fn folder_mut(&mut self, name: &str) -> Option<&mut Folder> {
        self.folders.iter_mut().find(|file| file.name() == name)
    }

    // Get a subfolder folder index
    pub fn folder_index(&self, name: &str) -> Option<usize> {
        self.folders
            .iter()
            .enumerate()
            .find(|(_, folder)| folder.name() == name)
            .map(|(index, _)| index)
    }

    /// Remove a file
    pub fn remove_file(&mut self, index: usize) {
        self.files.remove(index);
    }

    /// Remove a folder
    pub fn remove_folder(&mut self, index: usize) {
        self.folders.remove(index);
    }

    /// Add a subfolder
    pub(crate) fn add_folder(&mut self, mut data: Datas) -> &mut Folder {
        data.name = self.path.join(data.name);
        self.folders.push(Folder::new(data));
        self.folders.last_mut().unwrap()
    }

    /// Add a file
    pub(crate) fn add_file(
        &mut self,
        mut file_data: file::Datas,
        thumbnail: Option<impl Into<Utf8PathBuf>>,
    ) -> &mut File {
        file_data.filename = self.path.join(file_data.filename);
        let file = File::new(file_data, thumbnail);
        self.files.push(file);
        self.files.last_mut().unwrap()
    }

    /// Sort
    pub fn sort(&mut self) {
        self.files.sort_unstable();
        self.folders.sort_unstable();
        for folder in &mut self.folders {
            folder.sort();
        }
    }

    /// Get a file iterator
    pub fn iter(&self) -> FileIterator {
        FileIterator::new(self)
    }

    /// Get all files recurssivly with a filter
    pub fn get_all_files_mut<P>(&mut self, filter_fn: P) -> Vec<&mut File>
    where
        P: Fn(&&mut File) -> bool + Clone,
    {
        let mut files: Vec<&mut File> = self.files.iter_mut().filter(filter_fn.clone()).collect();
        for folder in &mut self.folders {
            files.extend(folder.get_all_files_mut(filter_fn.clone()));
        }
        files
    }
}

impl PartialOrd for Folder {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for Folder {
    fn cmp(&self, other: &Self) -> Ordering {
        self.path.cmp(&other.path)
    }
}

impl Item for Folder {
    fn id(&self) -> i64 {
        self.id
    }
}

impl<'a> IntoIterator for &'a Folder {
    type Item = &'a File;

    type IntoIter = FileIterator<'a>;

    fn into_iter(self) -> Self::IntoIter {
        self.iter()
    }
}

/// File filter option
#[derive(Clone, Debug, Default)]
pub struct FileFilterOption {
    /// Year
    year: Option<i32>,
    /// Month
    month: Option<u32>,
    /// Day
    day: Option<u32>,
    /// Country
    country: String,
    /// Region
    region: String,
    /// Department
    department: String,
    /// City
    city: String,
    /// Filename
    filename: String,
    /// Type
    typ: Option<Type>,
    /// Folder name
    folder: String,
    /// Minimum resolution
    min_res: Option<f32>,
}

impl FileFilterOption {
    /// Add year filter
    pub fn with_year(&mut self, year: i32) {
        self.year = Some(year);
    }

    /// Add month filter
    pub fn with_month(&mut self, month: u32) {
        self.month = Some(month);
    }

    /// Add day filter
    pub fn with_day(&mut self, day: u32) {
        self.day = Some(day);
    }

    /// Add country filter
    pub fn with_country(&mut self, country: impl ToString) {
        self.country = country.to_string().to_lowercase();
    }

    /// Add region filter
    pub fn with_region(&mut self, region: impl ToString) {
        self.region = region.to_string().to_lowercase();
    }

    /// Add department filter
    pub fn with_department(&mut self, department: impl ToString) {
        self.department = department.to_string().to_lowercase();
    }

    /// Add city filter
    pub fn with_city(&mut self, city: impl ToString) {
        self.city = city.to_string().to_lowercase();
    }

    /// Add filename filter
    pub fn with_filename(&mut self, filename: impl ToString) {
        self.filename = filename.to_string();
    }

    /// Add type filter
    pub fn with_typ(&mut self, typ: Type) {
        self.typ = Some(typ);
    }

    /// Add folder filter
    pub fn with_folder(&mut self, folder: impl ToString) {
        self.folder = folder.to_string();
    }

    /// Add minimum resolution filter
    pub fn with_min_res(&mut self, min_res: f32) {
        self.min_res = Some(min_res);
    }
}

/// Iterator max depth with optimization
const ITERATOR_MAX_DEPTH: usize = 10;

/// A file iterator in a folder
#[derive(Debug)]
pub struct FileIterator<'a> {
    /// The main folder to iterate on
    main_folder: &'a Folder,
    /// Current folder and subfolders index
    folder_indexes: SmallVec<[usize; ITERATOR_MAX_DEPTH]>,
    /// File index in current folder
    file_index: usize,
}

impl<'a> FileIterator<'a> {
    /// Create a file iterator
    ///
    /// The iterator point to the next file
    fn new(main_folder: &'a Folder) -> Self {
        let mut folder_index = SmallVec::new();
        Self::find_photo_folder(main_folder, &mut folder_index);
        FileIterator {
            main_folder,
            folder_indexes: folder_index,
            file_index: 0,
        }
    }

    /// Find the next folder with files and update indexes
    fn find_photo_folder(
        main_folder: &Folder,
        indexes: &mut SmallVec<[usize; ITERATOR_MAX_DEPTH]>,
    ) -> bool {
        if main_folder.folders().is_empty() {
            !main_folder.files().is_empty()
        } else {
            indexes.push(0);
            for (index, folder) in main_folder.folders().iter().enumerate() {
                *indexes.last_mut().unwrap() = index;
                if Self::find_photo_folder(folder, indexes) {
                    return true;
                }
            }
            indexes.pop();
            !main_folder.files().is_empty()
        }
    }

    /// Filter files
    pub fn filter_file(self, options: FileFilterOption) -> impl Iterator<Item = &'a File> {
        self.filter(move |file| {
            if let Some(year) = options.year {
                file.time().date().year() == year
            } else {
                true
            }
        })
        .filter(move |file| {
            if let Some(month) = options.month {
                file.time().date().month() == month
            } else {
                true
            }
        })
        .filter(move |file| {
            if let Some(day) = options.day {
                file.time().date().day() == day
            } else {
                true
            }
        })
        .filter(move |file| {
            if options.country.is_empty() {
                true
            } else if let Some(location) = file.location() {
                location
                    .country
                    .long_name
                    .to_lowercase()
                    .contains(&options.country)
            } else {
                false
            }
        })
        .filter(move |file| {
            if options.region.is_empty() {
                true
            } else if let Some(location) = file.location() {
                location.admin1.to_lowercase().contains(&options.region)
            } else {
                false
            }
        })
        .filter(move |file| {
            if options.department.is_empty() {
                true
            } else if let Some(location) = file.location() {
                location.admin2.to_lowercase().contains(&options.department)
            } else {
                false
            }
        })
        .filter(move |file| {
            if options.city.is_empty() {
                true
            } else if let Some(location) = file.location() {
                location.city.to_lowercase().contains(&options.city)
            } else {
                false
            }
        })
        .filter(move |file| {
            if options.filename.is_empty() {
                true
            } else {
                file.name().contains(&options.filename)
            }
        })
        .filter(move |file| {
            if let Some(typ) = options.typ {
                file.typ() == typ
            } else {
                true
            }
        })
        .filter(move |file| {
            if options.folder.is_empty() {
                true
            } else {
                file.filename()
                    .parent()
                    .unwrap()
                    .as_str()
                    .contains(&options.folder)
            }
        })
        .filter(move |file| {
            if let Some(min_res) = options.min_res {
                if let Some(resolution) = file.resolution() {
                    resolution.mp() > min_res
                } else {
                    false
                }
            } else {
                true
            }
        })
    }
}

impl<'a> Iterator for FileIterator<'a> {
    type Item = &'a File;

    fn next(&mut self) -> Option<Self::Item> {
        // Get folder
        let mut folders = SmallVec::<[_; ITERATOR_MAX_DEPTH]>::new();
        folders.push(self.main_folder);
        self.folder_indexes.iter().for_each(|index| {
            folders.push(&folders.last().unwrap().folders()[*index]);
        });

        // Get file
        if let Some(file) = folders.last().unwrap().files().get(self.file_index) {
            self.file_index += 1;

            // Find the next folder if no more file
            if folders.last().unwrap().files().len() == self.file_index
                && !self.folder_indexes.is_empty()
            {
                // Find next folder
                for _ in 0..self.folder_indexes.len() {
                    // Remove current folder
                    let new_index = self.folder_indexes.pop().unwrap() + 1;
                    folders.pop();

                    // Try to find in sibling
                    if let Some(folder) = folders.last().unwrap().folders().get(new_index) {
                        self.folder_indexes.push(new_index);
                        if Self::find_photo_folder(folder, &mut self.folder_indexes) {
                            break;
                        }
                        self.folder_indexes.pop();
                    }
                    // Or in parent
                    else if !folders.last().unwrap().files.is_empty() {
                        break;
                    }
                }

                self.file_index = 0;
            }
            Some(file)
        } else {
            None
        }
    }
}

impl<'a> FusedIterator for FileIterator<'a> {}

#[cfg(test)]
mod tests {
    use super::*;
    use chrono::{DateTime, Utc};
    use file::Resolution;
    use geo_types::Point;

    #[test]
    fn test_folder() {
        let mut folder_1 = Folder::new(Datas {
            id: 1,
            name: "folder_1".into(),
        });
        assert_eq!(folder_1.path(), Utf8Path::new("folder_1"));
        assert_eq!(folder_1.name(), "folder_1");
        assert_eq!(folder_1.id(), 1);
        assert!(folder_1.files().is_empty());
        assert!(folder_1.folders().is_empty());
        {
            let folder_2 = folder_1.add_folder(Datas {
                id: 2,
                name: "folder_2".into(),
            });
            assert_eq!(folder_2.path(), Utf8Path::new("folder_1/folder_2"));
            assert_eq!(folder_2.name(), "folder_2");
            assert_eq!(folder_2.id(), 2);
            assert!(folder_2.files().is_empty());
            assert!(folder_2.folders().is_empty());
            assert_eq!(folder_1.folders.len(), 1);
            let file = folder_1.add_file(
                file::Datas {
                    id: 1,
                    filename: "toto".into(),
                    name: "toto".to_string(),
                    time: DateTime::<Utc>::from_timestamp(1661886110, 125632784)
                        .unwrap()
                        .naive_utc(),
                    location: Some(Point::new(1.25, 25.365)),
                    typ: Type::Image,
                    resolution: Some(Resolution::new(500, 250)),
                    duration: None,
                },
                Some("tutu.jpg"),
            );

            assert_eq!(file.filename(), Utf8Path::new("folder_1/toto"));
            assert_eq!(
                *file.time(),
                DateTime::<Utc>::from_timestamp(1661886110, 125632784)
                    .unwrap()
                    .naive_utc()
            );
            assert_eq!(file.location().unwrap().point, Point::new(1.25, 25.365));
            assert_eq!(file.typ(), Type::Image);
            assert_eq!(file.id(), 1);
            assert_eq!(file.thumbnail(), Some(Utf8Path::new("tutu.jpg")));
            assert_eq!(file.resolution(), Some(Resolution::new(500, 250)).as_ref());
            assert_eq!(file.duration(), None);
        }

        assert_eq!(folder_1.folder("folder_2").unwrap().name(), "folder_2");
        assert_eq!(folder_1.file("toto").unwrap().1.name(), "toto");
        folder_1.remove_file(0);
        assert_eq!(folder_1.files().len(), 0);
        folder_1.remove_folder(0);
        assert_eq!(folder_1.folders().len(), 0);
    }

    #[test]
    fn test_sort() {
        let mut folder = Folder::new(Datas {
            id: 1,
            name: "folder_1".into(),
        });
        folder.add_folder(Datas {
            id: 2,
            name: "folder_10".into(),
        });
        folder.add_folder(Datas {
            id: 3,
            name: "folder_12".into(),
        });
        folder.add_folder(Datas {
            id: 4,
            name: "folder_11".into(),
        });
        folder.add_file(
            file::Datas {
                id: 1,
                filename: "b".into(),
                name: "b".to_string(),
                time: DateTime::<Utc>::from_timestamp(1661886110, 125632784)
                    .unwrap()
                    .naive_utc(),
                location: Some(Point::new(1.25, 25.365)),
                typ: Type::Image,
                resolution: None,
                duration: None,
            },
            None::<&str>,
        );
        folder.add_file(
            file::Datas {
                id: 2,
                filename: "a".into(),
                name: "a".to_string(),
                time: DateTime::<Utc>::from_timestamp(1661886110, 125632784)
                    .unwrap()
                    .naive_utc(),
                location: Some(Point::new(1.25, 25.365)),
                typ: Type::Image,
                resolution: None,
                duration: None,
            },
            None::<&str>,
        );
        folder.add_file(
            file::Datas {
                id: 3,
                filename: "c".into(),
                name: "c".to_string(),
                time: DateTime::<Utc>::from_timestamp(1661886100, 125632784)
                    .unwrap()
                    .naive_utc(),
                location: Some(Point::new(1.25, 25.365)),
                typ: Type::Image,
                resolution: None,
                duration: None,
            },
            None::<&str>,
        );
        folder.sort();
        assert_eq!(folder.folder_index("folder_10"), Some(0));
        assert_eq!(folder.folder_index("folder_11"), Some(1));
        assert_eq!(folder.folder_index("folder_12"), Some(2));
        assert_eq!(
            folder.folders()[0].path(),
            Utf8Path::new("folder_1/folder_10")
        );
        assert_eq!(
            folder.folders()[1].path(),
            Utf8Path::new("folder_1/folder_11")
        );
        assert_eq!(
            folder.folders()[2].path(),
            Utf8Path::new("folder_1/folder_12")
        );
        assert_eq!(folder.files()[0].filename(), Utf8Path::new("folder_1/c"));
        assert_eq!(folder.files()[1].filename(), Utf8Path::new("folder_1/a"));
        assert_eq!(folder.files()[2].filename(), Utf8Path::new("folder_1/b"));
    }

    #[test]
    fn test_file_iterator() {
        // Empty folder
        let mut folder = Folder::new(Datas {
            id: 1,
            name: "folder_1".into(),
        });
        assert!(folder.iter().next().is_none());

        // One file
        folder.add_file(
            file::Datas {
                id: 1,
                filename: "a".into(),
                name: "a".to_string(),
                time: DateTime::<Utc>::from_timestamp(0, 0).unwrap().naive_utc(),
                location: None,
                typ: Type::Image,
                resolution: None,
                duration: None,
            },
            None::<&str>,
        );
        let mut iter = folder.iter();
        assert_eq!(iter.next().unwrap().id(), 1);
        assert!(iter.next().is_none());

        // Two file
        folder.add_file(
            file::Datas {
                id: 2,
                filename: "b".into(),
                name: "b".to_string(),
                time: DateTime::<Utc>::from_timestamp(0, 0).unwrap().naive_utc(),
                location: None,
                typ: Type::Image,
                resolution: None,
                duration: None,
            },
            None::<&str>,
        );
        let mut iter = folder.iter();
        assert_eq!(iter.next().unwrap().id(), 1);
        assert_eq!(iter.next().unwrap().id(), 2);
        assert!(iter.next().is_none());

        // Subfolder
        folder
            .add_folder(Datas {
                id: 2,
                name: "folder_10".into(),
            })
            .add_folder(Datas {
                id: 3,
                name: "folder_100".into(),
            });
        let mut iter = folder.iter();
        assert_eq!(iter.next().unwrap().id(), 1);
        assert_eq!(iter.next().unwrap().id(), 2);
        assert!(iter.next().is_none());

        // Subfolder with files
        folder.folders_mut()[0].folders_mut()[0].add_file(
            file::Datas {
                id: 3,
                filename: "c".into(),
                name: "c".to_string(),
                time: DateTime::<Utc>::from_timestamp(0, 0).unwrap().naive_utc(),
                location: None,
                typ: Type::Image,
                resolution: None,
                duration: None,
            },
            None::<&str>,
        );
        folder.folders_mut()[0].folders_mut()[0].add_file(
            file::Datas {
                id: 4,
                filename: "d".into(),
                name: "d".to_string(),
                time: DateTime::<Utc>::from_timestamp(0, 0).unwrap().naive_utc(),
                location: None,
                typ: Type::Image,
                resolution: None,
                duration: None,
            },
            None::<&str>,
        );
        let mut iter = folder.iter();
        assert_eq!(iter.next().unwrap().id(), 3);
        assert_eq!(iter.next().unwrap().id(), 4);
        assert_eq!(iter.next().unwrap().id(), 1);
        assert_eq!(iter.next().unwrap().id(), 2);
        assert!(iter.next().is_none());

        // Subfolder brother empty
        folder.add_folder(Datas {
            id: 4,
            name: "folder_20".into(),
        });
        let mut iter = folder.iter();
        assert_eq!(iter.next().unwrap().id(), 3);
        assert_eq!(iter.next().unwrap().id(), 4);
        assert_eq!(iter.next().unwrap().id(), 1);
        assert_eq!(iter.next().unwrap().id(), 2);
        assert!(iter.next().is_none());

        // Subfolder brother with files
        folder.folders_mut()[1].add_file(
            file::Datas {
                id: 5,
                filename: "e".into(),
                name: "e".to_string(),
                time: DateTime::<Utc>::from_timestamp(0, 0).unwrap().naive_utc(),
                location: None,
                typ: Type::Image,
                resolution: None,
                duration: None,
            },
            None::<&str>,
        );
        let mut iter = folder.iter();
        assert_eq!(iter.next().unwrap().id(), 3);
        assert_eq!(iter.next().unwrap().id(), 4);
        assert_eq!(iter.next().unwrap().id(), 5);
        assert_eq!(iter.next().unwrap().id(), 1);
        assert_eq!(iter.next().unwrap().id(), 2);
        assert!(iter.next().is_none());

        // Midle Subfolder with files
        folder.folders_mut()[0].add_file(
            file::Datas {
                id: 6,
                filename: "f".into(),
                name: "f".to_string(),
                time: DateTime::<Utc>::from_timestamp(0, 0).unwrap().naive_utc(),
                location: None,
                typ: Type::Image,
                resolution: None,
                duration: None,
            },
            None::<&str>,
        );
        let mut iter = folder.iter();
        assert_eq!(iter.next().unwrap().id(), 3);
        assert_eq!(iter.next().unwrap().id(), 4);
        assert_eq!(iter.next().unwrap().id(), 6);
        assert_eq!(iter.next().unwrap().id(), 5);
        assert_eq!(iter.next().unwrap().id(), 1);
        assert_eq!(iter.next().unwrap().id(), 2);
        assert!(iter.next().is_none());

        // Get all files
        let files = folder.get_all_files_mut(|_| true);
        let mut iter = files.iter();
        assert_eq!(iter.next().unwrap().id(), 1);
        assert_eq!(iter.next().unwrap().id(), 2);
        assert_eq!(iter.next().unwrap().id(), 6);
        assert_eq!(iter.next().unwrap().id(), 3);
        assert_eq!(iter.next().unwrap().id(), 4);
        assert_eq!(iter.next().unwrap().id(), 5);
        assert!(iter.next().is_none());
    }

    fn create_test_file_filter_folder() -> Folder {
        let mut folder = Folder::new(Datas {
            id: 1,
            name: "folder".into(),
        });

        let subfolder_1 = folder.add_folder(Datas {
            id: 2,
            name: "subfolder_1".into(),
        });
        // Year
        subfolder_1.add_file(
            file::Datas {
                id: 1,
                filename: "a".into(),
                name: "a".to_string(),
                time: DateTime::<Utc>::from_timestamp(31536000, 0)
                    .unwrap()
                    .naive_utc(),
                location: Some(Point::new(60.020952, 10.678711)),
                typ: Type::Image,
                duration: None,
                resolution: Some(Resolution::new(200, 100)),
            },
            None::<&str>,
        );
        // Month
        subfolder_1.add_file(
            file::Datas {
                id: 2,
                filename: "a".into(),
                name: "a".to_string(),
                time: DateTime::<Utc>::from_timestamp(2678400, 0)
                    .unwrap()
                    .naive_utc(),
                location: Some(Point::new(60.020952, 10.678711)),
                typ: Type::Image,
                duration: None,
                resolution: Some(Resolution::new(200, 100)),
            },
            None::<&str>,
        );
        // Day
        subfolder_1.add_file(
            file::Datas {
                id: 3,
                filename: "a".into(),
                name: "a".to_string(),
                time: DateTime::<Utc>::from_timestamp(86400, 0)
                    .unwrap()
                    .naive_utc(),
                location: Some(Point::new(60.020952, 10.678711)),
                typ: Type::Image,
                duration: None,
                resolution: Some(Resolution::new(200, 100)),
            },
            None::<&str>,
        );
        // Country
        subfolder_1.add_file(
            file::Datas {
                id: 4,
                filename: "a".into(),
                name: "a".to_string(),
                time: DateTime::<Utc>::from_timestamp(0, 0).unwrap().naive_utc(),
                location: Some(Point::new(48.356249, 5.976563)),
                typ: Type::Image,
                duration: None,
                resolution: Some(Resolution::new(200, 100)),
            },
            None::<&str>,
        );
        // Region
        subfolder_1.add_file(
            file::Datas {
                id: 5,
                filename: "a".into(),
                name: "a".to_string(),
                time: DateTime::<Utc>::from_timestamp(0, 0).unwrap().naive_utc(),
                location: Some(Point::new(53.041213, -1.669922)),
                typ: Type::Image,
                duration: None,
                resolution: Some(Resolution::new(200, 100)),
            },
            None::<&str>,
        );
        // Department
        subfolder_1.add_file(
            file::Datas {
                id: 6,
                filename: "a".into(),
                name: "a".to_string(),
                time: DateTime::<Utc>::from_timestamp(0, 0).unwrap().naive_utc(),
                location: Some(Point::new(48.661943, 12.216797)),
                typ: Type::Image,
                duration: None,
                resolution: Some(Resolution::new(200, 100)),
            },
            None::<&str>,
        );
        // City
        subfolder_1.add_file(
            file::Datas {
                id: 7,
                filename: "a".into(),
                name: "a".to_string(),
                time: DateTime::<Utc>::from_timestamp(0, 0).unwrap().naive_utc(),
                location: Some(Point::new(42.875964, -7.954102)),
                typ: Type::Image,
                duration: None,
                resolution: Some(Resolution::new(200, 100)),
            },
            None::<&str>,
        );
        // Filename
        subfolder_1.add_file(
            file::Datas {
                id: 8,
                filename: "a".into(),
                name: "toto".to_string(),
                time: DateTime::<Utc>::from_timestamp(0, 0).unwrap().naive_utc(),
                location: Some(Point::new(60.020952, 10.678711)),
                typ: Type::Image,
                duration: None,
                resolution: Some(Resolution::new(200, 100)),
            },
            None::<&str>,
        );
        // Type
        subfolder_1.add_file(
            file::Datas {
                id: 9,
                filename: "a".into(),
                name: "a".to_string(),
                time: DateTime::<Utc>::from_timestamp(0, 0).unwrap().naive_utc(),
                location: Some(Point::new(60.020952, 10.678711)),
                typ: Type::Video,
                duration: None,
                resolution: Some(Resolution::new(200, 100)),
            },
            None::<&str>,
        );
        // Min res
        subfolder_1.add_file(
            file::Datas {
                id: 10,
                filename: "a".into(),
                name: "a".to_string(),
                time: DateTime::<Utc>::from_timestamp(0, 0).unwrap().naive_utc(),
                location: Some(Point::new(60.020952, 10.678711)),
                typ: Type::Image,
                duration: None,
                resolution: Some(Resolution::new(2000, 1000)),
            },
            None::<&str>,
        );
        let subfolder_2 = folder.add_folder(Datas {
            id: 2,
            name: "subfolder_2".into(),
        });
        // Folder
        subfolder_2.add_file(
            file::Datas {
                id: 11,
                filename: "a".into(),
                name: "a".to_string(),
                time: DateTime::<Utc>::from_timestamp(0, 0).unwrap().naive_utc(),
                location: Some(Point::new(60.020952, 10.678711)),
                typ: Type::Image,
                duration: None,
                resolution: Some(Resolution::new(200, 100)),
            },
            None::<&str>,
        );
        folder
    }

    #[test]
    fn test_file_filter_all() {
        let folder = create_test_file_filter_folder();

        let filter_option = FileFilterOption::default();
        let files = folder.iter().filter_file(filter_option).collect::<Vec<_>>();
        assert_eq!(files.len(), 11);
    }

    #[test]
    fn test_file_filter_year() {
        let folder = create_test_file_filter_folder();

        let mut filter_option = FileFilterOption::default();
        filter_option.with_year(1971);
        let files = folder.iter().filter_file(filter_option).collect::<Vec<_>>();
        assert_eq!(files.len(), 1);
        assert_eq!(files[0].id(), 1);
    }

    #[test]
    fn test_file_filter_month() {
        let folder = create_test_file_filter_folder();

        let mut filter_option = FileFilterOption::default();
        filter_option.with_month(2);
        let files = folder.iter().filter_file(filter_option).collect::<Vec<_>>();
        assert_eq!(files.len(), 1);
        assert_eq!(files[0].id(), 2);
    }

    #[test]
    fn test_file_filter_day() {
        let folder = create_test_file_filter_folder();

        let mut filter_option = FileFilterOption::default();
        filter_option.with_day(2);
        let files = folder.iter().filter_file(filter_option).collect::<Vec<_>>();
        assert_eq!(files.len(), 1);
        assert_eq!(files[0].id(), 3);
    }

    #[test]
    fn test_file_filter_country() {
        let folder = create_test_file_filter_folder();
        let mut filter_option = FileFilterOption::default();

        filter_option.with_country("Fr");
        let files = folder
            .iter()
            .filter_file(filter_option.clone())
            .collect::<Vec<_>>();
        assert_eq!(files.len(), 1);
        assert_eq!(files[0].id(), 4);

        filter_option.with_country("france");
        let files = folder.iter().filter_file(filter_option).collect::<Vec<_>>();
        assert_eq!(files.len(), 1);
        assert_eq!(files[0].id(), 4);
    }

    #[test]
    fn test_file_filter_region() {
        let folder = create_test_file_filter_folder();
        let mut filter_option = FileFilterOption::default();

        filter_option.with_region("England");
        let files = folder
            .iter()
            .filter_file(filter_option.clone())
            .collect::<Vec<_>>();
        assert_eq!(files.len(), 1);
        assert_eq!(files[0].id(), 5);

        filter_option.with_region("en");
        let files = folder.iter().filter_file(filter_option).collect::<Vec<_>>();
        assert_eq!(files.len(), 1);
        assert_eq!(files[0].id(), 5);
    }

    #[test]
    fn test_file_filter_department() {
        let folder = create_test_file_filter_folder();
        let mut filter_option = FileFilterOption::default();

        filter_option.with_department("Lower Bavaria");
        let files = folder
            .iter()
            .filter_file(filter_option.clone())
            .collect::<Vec<_>>();
        assert_eq!(files.len(), 1);
        assert_eq!(files[0].id(), 6);

        filter_option.with_department("bav");
        let files = folder.iter().filter_file(filter_option).collect::<Vec<_>>();
        assert_eq!(files.len(), 1);
        assert_eq!(files[0].id(), 6);
    }

    #[test]
    fn test_file_filter_city() {
        let folder = create_test_file_filter_folder();
        let mut filter_option = FileFilterOption::default();

        filter_option.with_city("Melide");
        let files = folder
            .iter()
            .filter_file(filter_option.clone())
            .collect::<Vec<_>>();
        assert_eq!(files.len(), 1);
        assert_eq!(files[0].id(), 7);

        filter_option.with_city("mel");
        let files = folder.iter().filter_file(filter_option).collect::<Vec<_>>();
        assert_eq!(files.len(), 1);
        assert_eq!(files[0].id(), 7);
    }

    #[test]
    fn test_file_filter_filename() {
        let folder = create_test_file_filter_folder();
        let mut filter_option = FileFilterOption::default();

        filter_option.with_filename("toto");
        let files = folder
            .iter()
            .filter_file(filter_option.clone())
            .collect::<Vec<_>>();
        assert_eq!(files.len(), 1);
        assert_eq!(files[0].id(), 8);

        filter_option.with_filename("o");
        let files = folder.iter().filter_file(filter_option).collect::<Vec<_>>();
        assert_eq!(files.len(), 1);
        assert_eq!(files[0].id(), 8);
    }

    #[test]
    fn test_file_filter_folder() {
        let folder = create_test_file_filter_folder();
        let mut filter_option = FileFilterOption::default();

        filter_option.with_folder("subfolder_2");
        let files = folder
            .iter()
            .filter_file(filter_option.clone())
            .collect::<Vec<_>>();
        assert_eq!(files.len(), 1);
        assert_eq!(files[0].id(), 11);

        filter_option.with_folder("2");
        let files = folder.iter().filter_file(filter_option).collect::<Vec<_>>();
        assert_eq!(files.len(), 1);
        assert_eq!(files[0].id(), 11);
    }

    #[test]
    fn test_file_filter_type() {
        let folder = create_test_file_filter_folder();
        let mut filter_option = FileFilterOption::default();

        filter_option.with_typ(Type::Video);
        let files = folder.iter().filter_file(filter_option).collect::<Vec<_>>();
        assert_eq!(files.len(), 1);
        assert_eq!(files[0].id(), 9);
    }

    #[test]
    fn test_file_filter_min_res() {
        let folder = create_test_file_filter_folder();
        let mut filter_option = FileFilterOption::default();

        filter_option.with_min_res(1.);
        let files = folder.iter().filter_file(filter_option).collect::<Vec<_>>();
        assert_eq!(files.len(), 1);
        assert_eq!(files[0].id(), 10);
    }
}
