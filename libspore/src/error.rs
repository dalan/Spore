use thiserror::Error;

use crate::image_loader;

#[derive(Error, Debug)]
pub enum Error {
    #[error("SQlite error")]
    Sqlite {
        #[from]
        source: rusqlite::Error,
    },
    #[error("I/O error")]
    Io {
        #[from]
        source: std::io::Error,
    },
    #[error("Invalid path")]
    Path {
        #[from]
        source: camino::FromPathBufError,
    },
    #[error("Database logic error")]
    DatabaseLogic(String),
    #[error("Date and time parse error")]
    DateTimeParse {
        #[from]
        source: chrono::format::ParseError,
    },
    #[error("Invalid system time")]
    SystemTime {
        #[from]
        source: std::time::SystemTimeError,
    },
    #[error("Database already exist")]
    DatabaseAlreadyExist,
    #[error("Database not exist")]
    DatabaseNotExist,
    #[error("Error when using image")]
    Image {
        #[from]
        source: image::error::ImageError,
    },
    #[error("Error on image buffer")]
    ConvertImageBuffer {
        #[from]
        source: fast_image_resize::ImageBufferError,
    },
    #[error("Error on image resize")]
    ImageResize {
        #[from]
        source: fast_image_resize::ResizeError,
    },
    #[error("Error on database migration")]
    SqliteMigration {
        #[from]
        source: rusqlite_migration::Error,
    },
    #[error("No thumbnail")]
    NoThumbnail,
    #[error("Cannot read mkv file")]
    Mastroska {
        #[from]
        source: matroska::Error,
    },
    #[error("Cannot read gpx file")]
    Gpx {
        #[from]
        source: gpx::errors::GpxError,
    },
    #[error("Cannot read svg file")]
    Svg {
        #[from]
        source: resvg::usvg::Error,
    },
    #[error("Cannot read or write avif file")]
    Avif {},
    #[error("Error when reading exiv metadatas")]
    Exiv2 {
        #[from]
        source: rexiv2::Rexiv2Error,
    },
    #[error("Invalid subfolder")]
    InvalidSubFolder,
    #[error("Image loader crash")]
    ImageLoaderCrash {
        #[from]
        source: flume::SendError<image_loader::Command>,
    },
    #[error("Acnnot use image ICC profile")]
    ICCConvert {
        #[from]
        source: lcms2::Error,
    },
}

pub type Result<T, E = Error> = std::result::Result<T, E>;
