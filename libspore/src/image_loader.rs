use crate::error::Result;
use crate::{File, Item};
use camino::Utf8PathBuf;
use flume::{Receiver, Sender, unbounded};
use image::DynamicImage;
use log::{debug, error};
use quick_cache::sync::Cache;
use std::collections::HashSet;
use std::sync::Mutex;
use std::{
    num::NonZero,
    sync::{
        Arc,
        atomic::{AtomicUsize, Ordering},
    },
    thread,
};

const FULL_IMAGE_CACHE_SIZE: usize = 5;

pub enum Destination {
    Thumbnail,
    Full(usize),
}

pub enum Command {
    Load(Utf8PathBuf, i64, Destination),
    End,
}

pub enum Msg {
    ThumbnailAvailable(i64),
    ImageAvailable(i64),
}

pub struct ImageLoader {
    nb_thread: usize,
    thumbnails_cache: Arc<Cache<i64, Arc<DynamicImage>>>,
    full_cache: Arc<Cache<i64, Arc<DynamicImage>>>,
    command_sender: Sender<Command>,
    command_receiver: Receiver<Command>,
    msg_receiver: Receiver<Msg>,
    full_index: Arc<AtomicUsize>,
}

impl ImageLoader {
    pub fn new() -> Self {
        // Create cache
        let thumbnails_cache = Arc::new(Cache::new(1000));
        let full_cache = Arc::new(Cache::new(FULL_IMAGE_CACHE_SIZE));

        // Get nb thread
        let nb_thread = thread::available_parallelism()
            .unwrap_or(NonZero::new(2).unwrap())
            .get()
            - 1;

        // Create message
        let (msg_sender, msg_receiver) = unbounded();

        let full_index = Arc::new(AtomicUsize::new(0));
        let full_in_progress = Arc::new(Mutex::new(HashSet::new()));

        // Create threads
        let (command_sender, command_receiver) = unbounded();
        for thread_index in 0..nb_thread {
            thread::spawn({
                let command_receiver = command_receiver.clone();
                let thumbnails_cache = thumbnails_cache.clone();
                let full_cache = full_cache.clone();
                let msg_sender = msg_sender.clone();
                let full_index = full_index.clone();
                let in_progress = full_in_progress.clone();
                move || {
                    load_thread(
                        thread_index,
                        command_receiver,
                        thumbnails_cache,
                        full_cache,
                        msg_sender,
                        full_index,
                        in_progress,
                    )
                }
            });
        }

        debug!("Create image loader with {} threads", nb_thread);

        Self {
            nb_thread,
            thumbnails_cache,
            full_cache,
            command_sender,
            command_receiver,
            msg_receiver,
            full_index,
        }
    }

    /// Ask loading thumbnail
    pub fn load_thumbnail(&self, file: &File) -> Result<()> {
        if let Some(thumbnail) = file.thumbnail() {
            self.command_sender.send(Command::Load(
                thumbnail.to_path_buf(),
                file.id(),
                Destination::Thumbnail,
            ))?;
        }
        Ok(())
    }

    /// Get a thumbnail from the cache if it is available
    pub fn get_thumbnail(&self, id: i64) -> Option<Arc<DynamicImage>> {
        self.thumbnails_cache.peek(&id)
    }

    /// Ask loading image
    pub fn load(&self, file: &File) -> Result<()> {
        let index = self.full_index.fetch_add(1, Ordering::Relaxed);
        self.command_sender.send(Command::Load(
            file.filename().to_path_buf(),
            file.id(),
            Destination::Full(index),
        ))?;
        Ok(())
    }

    /// Get a image from the cache if it is available
    pub fn get(&self, id: i64) -> Option<Arc<DynamicImage>> {
        self.full_cache.peek(&id)
    }

    /// Clear all image from cache
    pub fn clear(&self) {
        debug!("Clear image loader");

        // Clear cache
        self.thumbnails_cache.clear();
        self.full_cache.clear();

        // Clear all commands
        while let Ok(_) = self.command_receiver.try_recv() {}
    }

    /// Get message receiver
    pub fn get_msg_receiver(&self) -> Receiver<Msg> {
        self.msg_receiver.clone()
    }
}

impl Default for ImageLoader {
    fn default() -> Self {
        Self::new()
    }
}

impl Drop for ImageLoader {
    fn drop(&mut self) {
        for _ in 0..self.nb_thread {
            let _ = self.command_sender.send(Command::End);
        }
    }
}

fn load_thread(
    thread_index: usize,
    command_receiver: Receiver<Command>,
    thumbnails_cache: Arc<Cache<i64, Arc<DynamicImage>>>,
    full_cache: Arc<Cache<i64, Arc<DynamicImage>>>,
    msg_sender: Sender<Msg>,
    full_index: Arc<AtomicUsize>,
    full_in_progress: Arc<Mutex<HashSet<i64>>>,
) {
    loop {
        match command_receiver.recv().unwrap_or(Command::End) {
            Command::End => break,
            Command::Load(path, id, destination) => match destination {
                Destination::Thumbnail => {
                    debug!("Load thumbnail `{}` id {}", path, id);

                    match crate::image::load(&path, false) {
                        Ok(img) => {
                            thumbnails_cache.insert(id, Arc::new(img));
                            let _ = msg_sender.send(Msg::ThumbnailAvailable(id));
                        }
                        Err(e) => error!("Cannot load thumbnail `{}`: {}", path, e),
                    }
                }
                Destination::Full(index) => {
                    let mut full_in_progress_guard = full_in_progress.lock().unwrap();
                    if index + FULL_IMAGE_CACHE_SIZE >= full_index.load(Ordering::Relaxed)
                        && !full_in_progress_guard.contains(&id)
                        && full_cache.peek(&id).is_none()
                    {
                        full_in_progress_guard.insert(id);
                        drop(full_in_progress_guard);

                        debug!("Load image `{}` id {}", path, id);

                        match crate::image::load(&path, true) {
                            Ok(img) => {
                                full_cache.insert(id, Arc::new(img));
                                let _ = msg_sender.send(Msg::ImageAvailable(id));
                            }
                            Err(e) => {
                                error!("Cannot load thumbnail `{}`: {}", path, e)
                            }
                        }

                        full_in_progress.lock().unwrap().remove(&id);
                    }
                }
            },
        }
    }
    debug!("End of image loader thread {}", thread_index);
}
