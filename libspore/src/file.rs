use crate::database::Item;
use crate::error::{Error, Result};
use camino::{Utf8Path, Utf8PathBuf};
use celes::Country;
use chrono::NaiveDateTime;
use geo_svg::ToSvg;
use geo_types::{MultiLineString, Point};
use gpx::Gpx;
use image::{DynamicImage, RgbImage};
use lazy_static::lazy_static;
use log::warn;
use num::{Rational32, ToPrimitive};
use reverse_geocoder::ReverseGeocoder;
use std::io::Read;
use std::{
    cmp::Ordering,
    fmt::Display,
    fs,
    io::BufReader,
    path::PathBuf,
    process::{Command, Stdio},
    time::Duration,
};
use strum_macros::{Display, EnumString, FromRepr};
use which::which;

/// File type
#[derive(Copy, Clone, Debug, PartialEq, PartialOrd, FromRepr, Display, EnumString)]
#[strum(ascii_case_insensitive)]
pub enum Type {
    /// Image
    #[strum(serialize = "Image", serialize = "i")]
    Image,
    /// Video
    #[strum(serialize = "Video", serialize = "v")]
    Video,
    /// Panoramic image
    #[strum(serialize = "Panoramic", serialize = "p")]
    PanoramicImage,
    /// GPX
    #[strum(serialize = "Gpx", serialize = "g")]
    Gpx,
}

/// Location
#[derive(Clone, Debug, PartialEq)]
pub struct Location {
    /// Coordinate
    pub point: Point,
    /// City
    pub city: String,
    /// Administrative district 1
    pub admin1: String,
    /// Administrative district 2
    pub admin2: String,
    /// Country
    pub country: Country,
}

impl Location {
    /// Create a location based on coordinate
    fn new(point: Point) -> Self {
        lazy_static! {
            static ref GEOCODER: ReverseGeocoder = ReverseGeocoder::new();
        }
        let result = GEOCODER.search(point.x_y());
        Location {
            point,
            city: result.record.name.clone(),
            admin1: result.record.admin1.clone(),
            admin2: result.record.admin2.clone(),
            country: Country::from_alpha2(&result.record.cc).unwrap(),
        }
    }
}

impl Display for Location {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{} - {} - {} - {}",
            self.city, self.admin2, self.admin1, self.country
        )?;
        Ok(())
    }
}

/// Image/video resolution
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct Resolution {
    /// x length
    x: i32,
    /// y length
    y: i32,
}

impl Resolution {
    /// Create based image size
    pub const fn new(x: i32, y: i32) -> Self {
        Self { x, y }
    }

    /// Get x length
    pub const fn x(&self) -> i32 {
        self.x
    }

    /// Get y length
    pub const fn y(&self) -> i32 {
        self.y
    }

    /// Get ratio
    ///
    /// Try to approxiate ratio based on classic image/video ratio
    pub fn ratio(&self) -> Rational32 {
        const KNOWN_RATIO: [Rational32; 7] = [
            Rational32::new_raw(1, 1),
            Rational32::new_raw(1, 2),
            Rational32::new_raw(2, 1),
            Rational32::new_raw(2, 3),
            Rational32::new_raw(3, 2),
            Rational32::new_raw(16, 9),
            Rational32::new_raw(9, 16),
        ];

        let ratio = Rational32::new(self.x, self.y);

        if let Some(known_ratio) = KNOWN_RATIO.iter().find(|known_ratio| {
            (ratio.to_f32().unwrap_or(0.) - known_ratio.to_f32().unwrap()).abs() < 0.01
        }) {
            *known_ratio
        } else {
            ratio
        }
    }

    /// Get size in megapixel
    pub fn mp(&self) -> f32 {
        (self.x() * self.y()) as f32 / 1000000.
    }
}

impl Display for Resolution {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}×{} ({:.1}MP) with ratio {}",
            self.x(),
            self.y(),
            self.mp(),
            self.ratio()
        )?;
        Ok(())
    }
}

pub(crate) struct Datas {
    pub id: i64,
    pub filename: Utf8PathBuf,
    pub name: String,
    pub time: NaiveDateTime,
    pub location: Option<Point>,
    pub typ: Type,
    pub resolution: Option<Resolution>,
    pub duration: Option<Duration>,
}

/// A file
#[derive(Clone, Debug)]
pub struct File {
    /// ID in database
    id: i64,
    /// Full filename
    filename: Utf8PathBuf,
    /// File name
    name: String,
    /// Date/time
    time: NaiveDateTime,
    /// Location
    location: Option<Location>,
    /// File type
    typ: Type,
    /// Thumbnail path
    thumbnail: Option<Utf8PathBuf>,
    /// Resolution
    resolution: Option<Resolution>,
    /// Duration
    duration: Option<Duration>,
}

impl File {
    /// Create a file
    pub(crate) fn new(data: Datas, thumbnail: Option<impl Into<Utf8PathBuf>>) -> Self {
        Self {
            id: data.id,
            filename: data.filename,
            name: data.name,
            location: data.location.map(Location::new),
            typ: data.typ,
            time: data.time,
            thumbnail: thumbnail.map(|t| t.into()),
            resolution: data.resolution,
            duration: data.duration,
        }
    }

    /// Generate a thumbnail and set it
    pub fn generate_thumbnail<P: Into<Utf8PathBuf>>(&mut self, thumbnail_path: P) -> Result<()> {
        const THUMBNAIL_MAX_SIZE: u32 = 500;
        let mut thumbnail_path = thumbnail_path.into();

        match self.typ() {
            Type::Image | Type::PanoramicImage => {
                thumbnail_path.set_extension("avif");

                // Read source image from file
                let img = crate::image::load(self.filename(), false)?;

                // Resize if needed
                let img = if img.width() < THUMBNAIL_MAX_SIZE && img.height() < THUMBNAIL_MAX_SIZE {
                    img.to_rgb8()
                } else {
                    crate::image::resize(&img, THUMBNAIL_MAX_SIZE, THUMBNAIL_MAX_SIZE)?
                };

                // Write destination image as avif file
                crate::image::save_avif(
                    &DynamicImage::ImageRgb8(img),
                    &thumbnail_path,
                    false,
                    45,
                    8,
                )?;

                // Set thumbnail
                self.thumbnail = Some(thumbnail_path);
            }
            Type::Video => {
                lazy_static! {
                    static ref FFMPEG: Option<PathBuf> = {
                        if let Ok(ffmpeg) = which("ffmpeg") {
                            Some(ffmpeg)
                        } else {
                            warn!("Cannot generate video thumbnails because ffmpeg is not found !");
                            None
                        }
                    };
                }
                if let Some(ffmpeg) = &*FFMPEG {
                    thumbnail_path.set_extension("avif");
                    if Command::new(ffmpeg)
                        .args([
                            "-i",
                            self.filename().as_str(),
                            "-vf",
                            "select=eq(n,0)",
                            "-vf",
                            &format!(
                                "scale={}:{}:force_original_aspect_ratio=decrease",
                                THUMBNAIL_MAX_SIZE, THUMBNAIL_MAX_SIZE
                            ),
                            "-vframes",
                            "1",
                            thumbnail_path.as_str(),
                        ])
                        .stdout(Stdio::null())
                        .stderr(Stdio::null())
                        .status()?
                        .success()
                    {
                        self.thumbnail = Some(thumbnail_path);
                    } else {
                        warn!("Cannot generate thumbnail for `{}`", self.filename());
                    }
                }
            }
            Type::Gpx => {
                let gpx: Gpx = gpx::read(BufReader::new(fs::File::open(self.filename())?))?;
                if let Some(track) = gpx.tracks.first() {
                    // Get track line
                    let mut multiline = track.multilinestring();

                    // Multiply coord to have a bigger image
                    multiline.iter_mut().for_each(|line| {
                        line.coords_mut().for_each(|coord| {
                            coord.x *= 100000.;
                            coord.y *= 100000.;
                        });
                    });

                    // Convert to SVG
                    let svg_data = multiline
                        .to_svg()
                        .with_fill_opacity(0.0)
                        .with_stroke_width(2.)
                        .with_stroke_color(geo_svg::Color::Named("red"))
                        .to_string();

                    // Create SVG tree
                    let rtree = resvg::usvg::Tree::from_data(
                        svg_data.as_bytes(),
                        &resvg::usvg::Options::default(),
                    )?;

                    // Calculate ratio and width/height
                    let size = rtree.size();
                    let ratio = if size.width() > size.height() {
                        THUMBNAIL_MAX_SIZE as f32 / size.width()
                    } else {
                        THUMBNAIL_MAX_SIZE as f32 / size.height()
                    };
                    let width = (size.width() * ratio) as u32;
                    let height = (size.height() * ratio) as u32;

                    // Render
                    let mut pixmap = resvg::tiny_skia::Pixmap::new(width, height).unwrap();
                    resvg::render(
                        &rtree,
                        resvg::tiny_skia::Transform::from_scale(ratio, ratio),
                        &mut pixmap.as_mut(),
                    );

                    // Write destination image as avif file in lossless quality
                    thumbnail_path.set_extension("avif");
                    crate::image::save_avif(
                        &DynamicImage::ImageRgb8(
                            RgbImage::from_vec(width, height, pixmap.take()).unwrap(),
                        ),
                        &thumbnail_path,
                        false,
                        100,
                        8,
                    )?;

                    // Set thumbnail
                    self.thumbnail = Some(thumbnail_path);
                } else {
                    warn!("No track in GPX file `{}`", self.filename());
                }
            }
        }
        Ok(())
    }

    /// Get a reference to the file's time.
    pub fn time(&self) -> &NaiveDateTime {
        &self.time
    }

    /// Set the file's time.
    pub fn set_time(&mut self, time: NaiveDateTime) {
        self.time = time
    }

    /// Get a reference to the file's location.
    pub fn location(&self) -> Option<&Location> {
        self.location.as_ref()
    }

    /// Set the file's location.
    pub fn set_location(&mut self, point: Option<Point>) {
        self.location = point.map(Location::new);
    }

    /// Get a reference to the file's filename.
    pub fn filename(&self) -> &Utf8Path {
        &self.filename
    }

    /// Get a reference to the file's name.
    pub fn name(&self) -> &str {
        &self.name
    }

    /// Get a reference to the file's typ.
    pub fn typ(&self) -> Type {
        self.typ
    }

    /// Set the file's typ.
    pub fn set_typ(&mut self, typ: Type) {
        self.typ = typ;
    }

    /// Get a reference to the file's resolution.
    pub fn resolution(&self) -> Option<&Resolution> {
        self.resolution.as_ref()
    }

    /// Set the file's resolution.
    pub fn set_resolution(&mut self, resolution: Option<Resolution>) {
        self.resolution = resolution;
    }

    /// Get a reference to the file's duration.
    pub fn duration(&self) -> Option<&Duration> {
        self.duration.as_ref()
    }

    /// Set the file's duration.
    pub fn set_duration(&mut self, duration: Option<Duration>) {
        self.duration = duration;
    }

    /// Get a reference to the file's thumbnail.
    pub fn thumbnail(&self) -> Option<&Utf8Path> {
        self.thumbnail.as_deref()
    }

    /// Get image
    ///
    /// Return None if it's not an image
    pub fn image(&self) -> Result<Option<DynamicImage>> {
        match self.typ() {
            Type::Image | Type::PanoramicImage => {
                Ok(Some(crate::image::load(self.filename(), true)?))
            }
            Type::Video => Ok(None),
            Type::Gpx => Ok(None),
        }
    }

    /// Get track
    ///
    /// Return None if it's not a Gpx
    pub fn track(&self) -> Result<Option<MultiLineString>> {
        let gpx: Gpx = gpx::read(BufReader::new(fs::File::open(self.filename())?))?;
        Ok(gpx.tracks.first().map(|track| track.multilinestring()))
    }

    /// Get thumbnail image
    pub fn thumbnail_image(&self) -> Result<DynamicImage> {
        if let Some(thumb_path) = self.thumbnail() {
            Ok(crate::image::load(thumb_path, false)?)
        } else {
            Err(Error::NoThumbnail)
        }
    }

    /// Get reader
    pub fn reader(&self) -> Result<impl Read + Send> {
        Ok(fs::File::open(self.filename())?)
    }

    /// Get pano infos
    pub fn pano_info(&self) -> Result<PanoInfos> {
        let metadata = rexiv2::Metadata::new_from_path(self.filename())?;
        let cropped_area_image_height_pixels =
            metadata.get_tag_numeric("Xmp.GPano.CroppedAreaImageHeightPixels");
        let cropped_area_image_width_pixels =
            metadata.get_tag_numeric("Xmp.GPano.CroppedAreaImageWidthPixels");
        let cropped_area_left_pixels = metadata.get_tag_numeric("Xmp.GPano.CroppedAreaLeftPixels");
        let cropped_area_top_pixels = metadata.get_tag_numeric("Xmp.GPano.CroppedAreaTopPixels");
        let full_pano_height_pixels = metadata.get_tag_numeric("Xmp.GPano.FullPanoHeightPixels");
        let full_pano_width_pixels = metadata.get_tag_numeric("Xmp.GPano.FullPanoWidthPixels");

        Ok(PanoInfos {
            cropped_area_image_height_pixels,
            cropped_area_image_width_pixels,
            cropped_area_left_pixels,
            cropped_area_top_pixels,
            full_pano_height_pixels,
            full_pano_width_pixels,
        })
    }
}

impl Item for File {
    fn id(&self) -> i64 {
        self.id
    }
}

impl PartialEq for File {
    fn eq(&self, other: &Self) -> bool {
        self.filename() == other.filename() && self.time() == other.time()
    }
}

impl Eq for File {}

impl PartialOrd for File {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        match self.time().partial_cmp(other.time()) {
            Some(Ordering::Equal) => self.filename().partial_cmp(&other.filename),
            ord => ord,
        }
    }
}

impl Ord for File {
    fn cmp(&self, other: &Self) -> Ordering {
        self.partial_cmp(other).unwrap()
    }
}

#[derive(Debug, Clone)]
pub struct PanoInfos {
    pub cropped_area_image_height_pixels: i32,
    pub cropped_area_image_width_pixels: i32,
    pub cropped_area_left_pixels: i32,
    pub cropped_area_top_pixels: i32,
    pub full_pano_height_pixels: i32,
    pub full_pano_width_pixels: i32,
}

#[cfg(test)]
mod tests {
    use super::*;
    use chrono::{DateTime, Utc};

    #[test]
    fn test_file() {
        let file = File::new(
            Datas {
                id: 1,
                filename: "toto.jpg".into(),
                name: "toto".to_string(),
                time: DateTime::<Utc>::from_timestamp(1661886110, 125632784)
                    .unwrap()
                    .naive_utc(),
                location: Some(Point::new(1.25, 25.365)),
                typ: Type::Image,
                resolution: Some(Resolution::new(500, 250)),
                duration: None,
            },
            Some("tutu.jpg"),
        );
        assert_eq!(file.filename(), Utf8Path::new("toto.jpg"));
        assert_eq!(file.name(), "toto");
        assert_eq!(
            *file.time(),
            DateTime::<Utc>::from_timestamp(1661886110, 125632784)
                .unwrap()
                .naive_utc()
        );
        assert_eq!(
            file.location(),
            Some(Location {
                point: Point::new(1.25, 25.365),
                city: "Kisangani".to_string(),
                admin1: "Eastern Province".to_string(),
                admin2: "".to_string(),
                country: Country::the_democratic_republic_of_the_congo()
            })
            .as_ref()
        );
        assert_eq!(file.typ(), Type::Image);
        assert_eq!(file.id(), 1);
        assert_eq!(file.thumbnail(), Some(Utf8Path::new("tutu.jpg")));
        assert_eq!(file.resolution(), Some(Resolution::new(500, 250)).as_ref());
        assert_eq!(file.resolution().unwrap().ratio(), Rational32::new(2, 1));
        assert_eq!(file.duration(), None);
    }
}
