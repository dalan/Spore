use crate::{Folder, Item};

/// A collection
#[derive(Clone, Debug)]
pub struct Collection {
    id: i64,
    name: String,
    folder: Folder,
}

impl Collection {
    /// Create a collection
    pub(crate) fn new<S: ToString>(id: i64, name: S, folder: Folder) -> Self {
        Self {
            id,
            name: name.to_string(),
            folder,
        }
    }

    /// Get a reference to the collection's name.
    pub fn name(&self) -> &str {
        self.name.as_ref()
    }

    /// Get a reference to the collection's folder.
    pub fn folder(&self) -> &Folder {
        &self.folder
    }

    /// Get a mutable reference to the collection's folder.
    pub fn folder_mut(&mut self) -> &mut Folder {
        &mut self.folder
    }
}

impl Item for Collection {
    fn id(&self) -> i64 {
        self.id
    }
}

#[test]
fn test_collection() {
    let collection = Collection::new(
        1,
        "toto",
        Folder::new(crate::folder::Datas {
            id: 1,
            name: "folder".into(),
        }),
    );
    assert_eq!(collection.id(), 1);
}
