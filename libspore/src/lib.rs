pub mod collection;
pub mod database;
pub mod error;
pub mod file;
pub mod folder;
pub mod image;
pub mod image_loader;
mod libavif;
pub mod utility;

pub use collection::Collection;
pub use database::{Database, Item};
pub use error::*;
pub use file::File;
pub use folder::Folder;
pub use image::resize;

/// Init libspore
///
/// This must be called in a thread-safe fashion before using libspore.
pub fn init() -> error::Result<()> {
    rexiv2::initialize()?;
    rexiv2::set_log_level(rexiv2::LogLevel::MUTE);
    Ok(())
}
