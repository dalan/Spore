use crate::error::{Error, Result};
use crate::file::Resolution;
use crate::{Collection, Folder};
use crate::{folder, utility::*};
use camino::{Utf8Path, Utf8PathBuf};
use chrono::{DateTime, Utc};
use geo_types::Point;
use jwalk::WalkDir;
use lazy_static::lazy_static;
use log::{debug, error, info, warn};
use pathdiff::diff_utf8_paths;
use rayon::prelude::*;
use rusqlite::Connection;
use rusqlite_migration::{M, Migrations};
use std::borrow::Cow;
use std::collections::{BTreeSet, HashMap};
use std::fs;
use std::sync::Mutex;
use std::time::Duration;

// Define migrations. These are applied atomically.
lazy_static! {
    static ref MIGRATIONS: Migrations<'static> = Migrations::new(vec![
        M::up(
            r#"
            CREATE TABLE `Informations` (
                "name"	TEXT
            );
            CREATE TABLE "Collections" (
                "id"	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
                "name"	TEXT
            );
            CREATE TABLE "Folders" (
                "id"	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
                "name"	TEXT NOT NULL,
                "parent"	INTEGER,
                "collection"	INTEGER,
                FOREIGN KEY(`parent`) REFERENCES `Folders`(`id`),
                FOREIGN KEY(`collection`) REFERENCES `Collections`(`id`)
            );
            CREATE TABLE "Files" (
                "id"	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
                "name"	TEXT NOT NULL,
                "time"	INTEGER,
                "location_x"	REAL,
                "location_y"	REAL,
                "type"	INTEGER NOT NULL,
                "parent"	INTEGER NOT NULL,
                FOREIGN KEY(`parent`) REFERENCES `Folders`(`id`)
            );
        "#
        ),
        M::up(
            r#"
            ALTER TABLE "Files" ADD COLUMN "res_x" INTEGER;
            ALTER TABLE "Files" ADD COLUMN "res_y" INTEGER;
            ALTER TABLE "Files" ADD COLUMN "duration" INTEGER;
        "#
        ),
        M::up(
            r#"
            ALTER TABLE "Files" RENAME COLUMN "name" TO "filename";
            ALTER TABLE "Files" ADD COLUMN "name" TEXT NOT NULL DEFAULT "";
        "#
        ),
    ]);
}

/// Item store in database
pub trait Item {
    /// Get database ID
    fn id(&self) -> i64;
}

/// Part of update in progress
pub enum UpdatePart {
    /// Get files
    Get,
    /// Delete files
    DeleteFiles,
    /// Deletes folders
    DeleteFolders,
    /// Add files
    Add,
    /// Save database
    DbSave,
}

/// Part of update state
pub enum UpdateState {
    /// Init a new state
    Init { part: UpdatePart, size: usize },
    /// Progress in the current state
    Progress,
    /// End of the state
    End,
}

/// Simple state
pub enum SimpleState {
    /// Init
    Init { size: usize },
    /// Progress
    Progress,
    /// End
    End,
}

/// The database
pub struct Database {
    /// Name
    name: String,
    /// List of collections
    collections: Vec<Collection>,
    /// SQLITE connection
    connection: Connection,
    /// Thumbnails dir
    thumbnails_dir: Utf8PathBuf,
}

impl Database {
    /// Create a new database
    pub fn new<P, S>(path: P, name: S, thumbnails_dir: P) -> Result<Self>
    where
        P: AsRef<Utf8Path>,
        S: ToString,
    {
        let name = name.to_string();

        info!("Create database {} in `{}`", &name, path.as_ref());

        // Open SQLITE database
        let mut connection = Connection::open(path.as_ref().as_std_path())?;

        // Create database
        MIGRATIONS.to_latest(&mut connection)?;

        // Apply some PRAGMA. These are often better applied outside of migrations, as some needs to be
        // executed for each connection (like `foreign_keys`) or to be executed outside transactions
        // (`journal_mode` is a noop in a transaction).
        connection
            .pragma_update(None, "journal_mode", "WAL")
            .unwrap();
        connection
            .pragma_update(None, "foreign_keys", "ON")
            .unwrap();

        // Insert name in database
        connection.execute(r#"INSERT INTO `Informations` (name) VALUES (?1)"#, (&name,))?;

        let db = Database {
            name,
            collections: Vec::new(),
            connection,
            thumbnails_dir: thumbnails_dir.as_ref().to_owned(),
        };
        Ok(db)
    }

    /// Open a database
    pub fn open<P>(path: P, thumbnails_dir: P) -> Result<Self>
    where
        P: AsRef<Utf8Path>,
    {
        info!("Open database in `{}`", path.as_ref());

        // Open SQLITE database
        let mut connection = Connection::open(path.as_ref().as_std_path())?;

        MIGRATIONS.to_latest(&mut connection).unwrap();

        // Get name
        let name: String =
            connection.query_row(r#"SELECT name from `Informations`"#, [], |row| row.get(0))?;

        // Get collections
        debug!("Get collections");
        let mut get_coll_stmt = connection.prepare("SELECT * FROM `Collections`")?;
        let collections_err_it = get_coll_stmt.query_map([], |row| {
            let id = row.get(0)?;
            let name: String = row.get(1)?;

            let main_folder = connection.query_row(
                "SELECT id, name FROM `Folders` WHERE collection=?",
                [id],
                |row| {
                    let path: String = row.get(1)?;
                    Ok(Folder::new(folder::Datas {
                        id: row.get(0)?,
                        name: path.into(),
                    }))
                },
            )?;

            Ok(Collection::new(id, name, main_folder))
        })?;
        let mut collections = Vec::new();
        for collections_err in collections_err_it {
            collections.push(collections_err?);
        }
        get_coll_stmt.finalize()?;

        // Get files datas
        debug!("Get files datas");
        let mut files_datas = HashMap::new();
        let mut get_files_stm = connection.prepare(
            "SELECT parent,id,filename,name,time,location_x,location_y,type,res_x,res_y,duration FROM `Files` ORDER BY time",
        )?;
        let files_it_err = get_files_stm.query_map([], |row| {
            let parent_id: i64 = row.get(0)?;
            let id: i64 = row.get(1)?;
            let filename: String = row.get(2)?;
            let name: String = row.get(3)?;
            let time_ns: i64 = row.get(4)?;
            let location_x: Option<f64> = row.get(5)?;
            let location_y: Option<f64> = row.get(6)?;
            let typ = row.get(7)?;
            let res_x: Option<i32> = row.get(8)?;
            let res_y: Option<i32> = row.get(9)?;
            let dur_ms: Option<u64> = row.get(10)?;
            let loc = match (location_x, location_y) {
                (Some(location_x), Some(location_y)) => Some(Point::new(location_x, location_y)),
                _ => None,
            };
            let resolution = match (res_x, res_y) {
                (Some(res_x), Some(res_y)) => Some(Resolution::new(res_x, res_y)),
                _ => None,
            };
            Ok((
                parent_id,
                crate::file::Datas {
                    id,
                    filename: filename.into(),
                    name,
                    time: DateTime::<Utc>::from_timestamp(
                        time_ns / 1_000_000_000,
                        (time_ns % 1_000_000_000) as u32,
                    )
                    .unwrap()
                    .naive_utc(),
                    location: loc,
                    typ: crate::file::Type::from_repr(typ).unwrap(),
                    resolution,
                    duration: dur_ms.map(Duration::from_millis),
                },
            ))
        })?;
        for file_err in files_it_err {
            let (id, datas) = file_err?;
            files_datas.entry(id).or_insert_with(Vec::new).push(datas)
        }

        // Get folders datas
        debug!("Get folders datas");
        let mut folders_datas = HashMap::new();
        let mut get_folders_stm = connection.prepare(
            "SELECT parent,id,name FROM `Folders` WHERE parent IS NOT NULL ORDER BY name",
        )?;
        let folders_it_err = get_folders_stm.query_map([], |row| {
            let parent_id: i64 = row.get(0)?;
            let id: i64 = row.get(1)?;
            let name: String = row.get(2)?;
            Ok((
                parent_id,
                folder::Datas {
                    id,
                    name: name.into(),
                },
            ))
        })?;
        for folder_err in folders_it_err {
            let (id, datas) = folder_err?;
            folders_datas.entry(id).or_insert_with(Vec::new).push(datas)
        }

        // Get files and folders
        debug!("Get files and folders");
        for collection in collections.iter_mut() {
            let collection_dir = collection.folder().path().parent().unwrap().to_owned();
            let thumbnails_dir = thumbnails_dir.as_ref().join(collection.name());
            read_folder(
                collection.folder_mut(),
                &mut folders_datas,
                &mut files_datas,
                &collection_dir,
                &thumbnails_dir,
            )?;
        }
        get_folders_stm.finalize()?;
        get_files_stm.finalize()?;

        let db = Database {
            name,
            collections,
            connection,
            thumbnails_dir: thumbnails_dir.as_ref().to_owned(),
        };
        Ok(db)
    }

    /// Open a database with the default path
    pub fn open_default() -> Result<Self> {
        let (default_db_dir, default_thumbnails_dir) = get_default_dirs()?;
        if !default_db_dir.exists() {
            Err(Error::DatabaseNotExist)
        } else {
            Self::open(default_db_dir, default_thumbnails_dir)
        }
    }

    /// Create a new database with the default path
    pub fn new_default<S: ToString>(name: S) -> Result<Self> {
        let (default_db_dir, default_thumbnails_dir) = get_default_dirs()?;
        if default_db_dir.exists() {
            Err(Error::DatabaseAlreadyExist)
        } else {
            Self::new(default_db_dir, name, default_thumbnails_dir)
        }
    }

    /// Get a reference to the collection's folder.
    pub fn collections(&self) -> &[Collection] {
        &self.collections
    }

    /// Get a reference to the database's name.
    pub fn name(&self) -> &str {
        self.name.as_ref()
    }

    /// Close the connection
    pub fn close(self) -> Result<()> {
        if let Err((_, e)) = self.connection.close() {
            return Err(Error::Sqlite { source: e });
        }
        Ok(())
    }

    /// Add a collection and return its index
    pub fn add_collection<S, P>(&mut self, name: S, path: P) -> Result<usize>
    where
        S: ToString,
        P: Into<Utf8PathBuf>,
    {
        let name = name.to_string();
        let path = path.into();

        info!("Add collection {} in `{}`", name, path);

        // Transaction
        let tx = self.connection.transaction()?;

        // Create in database
        tx.execute("INSERT INTO `Collections` (name) VALUES (?1)", (&name,))?;
        let collection_id = tx.last_insert_rowid();
        tx.execute(
            "INSERT INTO `Folders` (name, collection) VALUES (?1, ?2)",
            (path.as_str(), collection_id),
        )?;
        let folder_id = tx.last_insert_rowid();

        // Create folder
        let folder = Folder::new(folder::Datas {
            id: folder_id,
            name: Utf8PathBuf::try_from(dunce::canonicalize(path)?)?,
        });

        // Commit transaction
        tx.commit()?;

        // Create collection
        let collection = Collection::new(collection_id, name, folder);

        self.collections.push(collection);
        Ok(self.collections.len() - 1)
    }

    /// Remove a collection
    pub fn remove_collection(&mut self, collection_index: usize) -> Result<()> {
        info!("Remove a collection");

        // Transaction
        let tx = self.connection.transaction()?;
        let mut delete_file_stat = tx.prepare("DELETE FROM `Files` WHERE `id`=?1")?;
        let mut delete_folder_stat = tx.prepare("DELETE FROM `Folders` WHERE `id`=?1")?;

        // Delete in memory
        delete_folder(
            self.collections[collection_index].folder(),
            &mut delete_folder_stat,
            &mut delete_file_stat,
        )?;

        // Delete in database
        tx.execute(
            "DELETE FROM `Collections` WHERE `id`=?1",
            (self.collections[collection_index].id(),),
        )?;

        // Delete thumbnails
        let thumbnail_path = self
            .thumbnails_dir
            .join(self.collections[collection_index].name());
        fs::remove_dir_all(thumbnail_path)?;

        // Commit transaction
        delete_file_stat.finalize()?;
        delete_folder_stat.finalize()?;
        tx.commit()?;

        self.collections.remove(collection_index);

        Ok(())
    }

    // Update a database
    pub fn update<F>(
        &mut self,
        collection_index: usize,
        folder: Option<&Utf8Path>,
        mut progress_callback: F,
    ) -> Result<()>
    where
        F: FnMut(UpdateState),
    {
        let collection = &mut self.collections[collection_index];

        // Get path to update
        let path = if let Some(folder) = folder {
            if folder.is_absolute() {
                if !folder.starts_with(collection.folder().path()) {
                    return Err(Error::InvalidSubFolder);
                }
                Cow::Borrowed(folder)
            } else {
                Cow::Owned(collection.folder().path().join(folder))
            }
        } else {
            Cow::Borrowed(collection.folder().path())
        };

        // Get files and folders
        info!("Get files list");
        progress_callback(UpdateState::Init {
            size: 0,
            part: UpdatePart::Get,
        });
        let walk_files: BTreeSet<_> = WalkDir::new(path.as_std_path())
            .into_iter()
            // Get only files filenames
            .filter_map(|entry| {
                entry.ok().and_then(|entry| {
                    if entry.file_type().is_dir() {
                        None
                    } else {
                        entry.path().try_into().ok()
                    }
                })
            })
            // Get only image, video and GPX files
            .filter(|path: &Utf8PathBuf| match infer::get_from_path(path) {
                Ok(Some(typ)) => match typ.matcher_type() {
                    infer::MatcherType::Image | infer::MatcherType::Video => {
                        progress_callback(UpdateState::Progress);
                        true
                    }
                    infer::MatcherType::Text => {
                        if path.extension().unwrap_or("") == "gpx" {
                            progress_callback(UpdateState::Progress);
                            true
                        } else {
                            false
                        }
                    }
                    _ => false,
                },
                Ok(None) => false,
                Err(e) => {
                    warn!("Cannot read file `{}`: {}", path, e);
                    false
                }
            })
            .collect();
        progress_callback(UpdateState::End);

        // Get files and folder in the database
        info!("Get database files list");
        let db_files: BTreeSet<_> = collection
            .folder()
            .iter()
            // Filter base on path
            .filter(|file| file.filename().starts_with(path.as_std_path()))
            .map(|file| file.filename().to_owned())
            .collect();

        // Initiate transaction
        let tx = self.connection.transaction()?;
        let mut delete_file_stat = tx.prepare("DELETE FROM `Files` WHERE `id`=?1")?;
        let mut delete_folder_stat = tx.prepare("DELETE FROM `Folders` WHERE `id`=?1")?;
        let mut insert_folder_stat =
            tx.prepare("INSERT INTO `Folders` (name, parent) VALUES (?1, ?2)")?;
        let mut insert_file_stat = tx.prepare("INSERT INTO `Files` (filename, name, time, location_x, location_y, type, res_x, res_y, duration, parent) VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8, ?9, ?10)")?;

        // Delete old entries
        let to_delete: Vec<_> = db_files.difference(&walk_files).collect();
        info!("Delete old files");
        progress_callback(UpdateState::Init {
            size: to_delete.len(),
            part: UpdatePart::DeleteFiles,
        });
        for path_to_delete in to_delete {
            debug!("Delete `{}`", path_to_delete);
            progress_callback(UpdateState::Progress);

            // Get file and folder
            let relative_path =
                diff_utf8_paths(path_to_delete, collection.folder().path()).unwrap();
            let parent_folder_to_delete = relative_path
                .parent()
                .unwrap()
                .iter()
                .fold(collection.folder_mut(), |parent_folder, tmp_path| {
                    parent_folder.folder_mut(tmp_path).unwrap()
                });
            let (file_to_delete_index, file_to_delete) = parent_folder_to_delete
                .file(relative_path.file_name().unwrap())
                .unwrap();

            // Delete in database
            match delete_file_stat.execute((file_to_delete.id(),)) {
                Ok(1) => {
                    // Delete thumbnail
                    if let Some(thumbnail_path) = file_to_delete.thumbnail() {
                        if let Err(e) = fs::remove_file(thumbnail_path) {
                            warn!("Cannot delete thumbnail `{}`: {}", &thumbnail_path, e)
                        }
                    }
                    // Delete
                    parent_folder_to_delete.remove_file(file_to_delete_index);
                }
                Ok(n) => {
                    error!(
                        "Delete {} entries instead of 1 when deleting file id {}",
                        n,
                        file_to_delete.id()
                    )
                }
                Err(e) => warn!("Cannot delete file `{}` ({})", file_to_delete.name(), e),
            }
        }
        progress_callback(UpdateState::End);

        // Delete empty folder
        info!("Delete empty folders");
        progress_callback(UpdateState::Init {
            size: 0,
            part: UpdatePart::DeleteFolders,
        });
        delete_empty_folders(
            &mut delete_folder_stat,
            collection.folder_mut(),
            &mut progress_callback,
        );
        progress_callback(UpdateState::End);

        // Add new entries
        let to_add: Vec<_> = walk_files.difference(&db_files).collect();
        info!("Add new files");
        progress_callback(UpdateState::Init {
            size: to_add.len(),
            part: UpdatePart::Add,
        });
        for path_to_add in to_add {
            debug!("Add `{}`", path_to_add);
            progress_callback(UpdateState::Progress);

            let relative_path = diff_utf8_paths(path_to_add, collection.folder().path()).unwrap();

            let mut current_folder = collection.folder_mut();

            // Add missing folders and set current folder
            for folder_to_add in relative_path.parent().unwrap().iter() {
                if let Some(folder_index) = current_folder.folder_index(folder_to_add) {
                    current_folder = &mut current_folder.folders_mut()[folder_index];
                } else {
                    let path = current_folder.path().join(folder_to_add);
                    debug!("Add folder `{}`", &path);

                    let new_folder_id = insert_folder_stat
                        .insert((path.file_name().unwrap(), current_folder.id()))?;

                    current_folder = current_folder.add_folder(folder::Datas {
                        id: new_folder_id,
                        name: folder_to_add.into(),
                    });
                }
            }

            // Add file
            match get_files_info(path_to_add) {
                Ok(mut file_data) => {
                    // Add in database
                    file_data.id = insert_file_stat.insert((
                        path_to_add.file_name().unwrap(),
                        &file_data.name,
                        file_data.time.and_utc().timestamp_nanos_opt(),
                        file_data.location.map(|loc| loc.x()),
                        file_data.location.map(|loc| loc.y()),
                        file_data.typ as i64,
                        file_data.resolution.as_ref().map(|res| res.x()),
                        file_data.resolution.as_ref().map(|res| res.y()),
                        file_data.duration.map(|dur| dur.as_millis() as u64),
                        current_folder.id(),
                    ))?;

                    // Add in folder
                    current_folder.add_file(file_data, None::<&str>);
                }
                Err(e) => error!("Cannot add file `{}`: {}", path_to_add, e),
            }
        }
        progress_callback(UpdateState::End);

        // Commit transaction
        delete_file_stat.finalize()?;
        delete_folder_stat.finalize()?;
        insert_folder_stat.finalize()?;
        insert_file_stat.finalize()?;
        progress_callback(UpdateState::Init {
            size: 0,
            part: UpdatePart::DbSave,
        });
        tx.commit()?;
        progress_callback(UpdateState::End);

        collection.folder_mut().sort();

        Ok(())
    }

    /// Generate missing thumbnails
    pub fn generate_thumbnail<F>(
        &mut self,
        collection_index: usize,
        progress_callback: Mutex<F>,
    ) -> Result<()>
    where
        F: FnMut(SimpleState) + Send + Sync,
    {
        let root_collection_dir = self.collections[collection_index]
            .folder()
            .path()
            .parent()
            .unwrap()
            .to_owned();
        let thumbnails_dir = self
            .thumbnails_dir
            .join(self.collections[collection_index].name());
        let files = self.collections[collection_index]
            .folder_mut()
            .get_all_files_mut(|file| file.thumbnail().is_none());

        progress_callback.lock().unwrap()(SimpleState::Init { size: files.len() });

        files.into_par_iter().try_for_each(|file| -> Result<()> {
            debug!("Generate thumbnail for `{}`", file.filename());

            let thumbnail_path =
                thumbnails_dir.join(file.filename().strip_prefix(&root_collection_dir).unwrap());

            // Create parent folder if needed
            let parent_folder = thumbnail_path.parent().unwrap();
            if !parent_folder.exists() {
                fs::create_dir_all(parent_folder)?;
            }

            // Generate thumbnail
            if let Err(e) = file.generate_thumbnail(thumbnail_path) {
                warn!("Cannot generate thumbnail for `{}`: {}", file.filename(), e);
            }

            progress_callback.lock().unwrap()(SimpleState::Progress);

            Ok(())
        })?;

        progress_callback.lock().unwrap()(SimpleState::End);

        Ok(())
    }

    /// Update files metadatas
    pub fn update_metadatas<F>(
        &mut self,
        collection_index: usize,
        folder: Option<&Utf8Path>,
        mut progress_callback: F,
    ) -> Result<()>
    where
        F: FnMut(SimpleState) + Send + Sync,
    {
        let collection = &mut self.collections[collection_index];

        // Get path to update
        let root_path = if let Some(folder) = folder {
            if folder.is_absolute() {
                if !folder.starts_with(collection.folder().path()) {
                    return Err(Error::InvalidSubFolder);
                }
                Cow::Borrowed(folder)
            } else {
                Cow::Owned(collection.folder().path().join(folder))
            }
        } else {
            Cow::Owned(collection.folder().path().to_owned())
        };

        let files = collection
            .folder_mut()
            .get_all_files_mut(|file| file.filename().starts_with(&*root_path));
        progress_callback(SimpleState::Init { size: files.len() });

        let tx = self.connection.transaction()?;
        let mut update_stat = tx.prepare("UPDATE `Files` SET `name`=?2, `time`=?3, `location_x`=?4, `location_y`=?5, `type`=?6, `res_x`=?7, `res_y`=?8, `duration`=?9 WHERE `id`=?1")?;

        for file in files {
            progress_callback(SimpleState::Progress);

            match get_files_info(file.filename()) {
                Ok(file_datas) => {
                    // Update if needed
                    if file_datas.location != file.location().map(|loc| loc.point)
                        || file_datas.time != *file.time()
                        || file_datas.typ != file.typ()
                        || file_datas.resolution.as_ref() != file.resolution()
                        || file_datas.duration.as_ref() != file.duration()
                        || file_datas.name != file.name()
                    {
                        debug!("Update {} metadata", file.filename());

                        // Update database
                        update_stat.execute((
                            file.id(),
                            file_datas.name,
                            file_datas.time.and_utc().timestamp_nanos_opt(),
                            file_datas.location.map(|loc| loc.x()),
                            file_datas.location.map(|loc| loc.y()),
                            file_datas.typ as i64,
                            file_datas.resolution.as_ref().map(|res| res.x()),
                            file_datas.resolution.as_ref().map(|res| res.y()),
                            file_datas.duration.map(|dur| dur.as_millis() as u64),
                        ))?;

                        // Update file
                        file.set_time(file_datas.time);
                        file.set_typ(file_datas.typ);
                        file.set_location(file_datas.location);
                        file.set_resolution(file_datas.resolution);
                        file.set_duration(file_datas.duration);
                    }
                }
                Err(e) => error!("Cannot update file `{}`: {}", file.filename(), e),
            }
        }

        update_stat.finalize()?;
        tx.commit()?;

        progress_callback(SimpleState::End);
        Ok(())
    }
}

// Test that migrations are working
#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn migrations_test() {
        assert!(MIGRATIONS.validate().is_ok());
    }
}
