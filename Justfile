#!/usr/bin/env -S just --justfile

alias b := build
alias br:= build_release
alias t := test
alias tr := test_release

cli-open:
  cargo run --bin spore-cli -- open

gtk-open:
  SPORE_LOG_LEVEL=debug LOCALE_DIR=target/locale/ cargo run --bin spore-gtk -- 

cli-open-r:
  cargo run --release --bin spore-cli -- open

gtk-open-r:
  SPORE_LOG_LEVEL=debug LOCALE_DIR=target/locale/ cargo run --release --bin spore-gtk -- 

build:
  cargo build

build_release:
  cargo build --release

test:
  RUST_BACKTRACE=1 cargo nextest run

test_release:
  cargo nextest run --release

fmt:
  cargo fmt --all

clean_dist:
  rm -Rf ./dist

dist_linux: clean_dist build_release
  mkdir dist
  smc bin_linux files 
  smc sources
  cargo deb --output dist -p spore-cli

dist_win: clean_dist build_release
  mkdir dist
  ./dist_win.sh

dist_deb: clean_dist build_release
  mkdir dist
  cargo deb -o dist -p spore-cli

po:
  xgettext --sort-by-file --add-comments=TRANSLATORS: --keyword=translatable --keyword=_ -o spore-gtk/po/spore-gtk-ui.pot spore-gtk/ui/*.ui
  xtr -o=spore-gtk/po/spore-gtk-code.pot spore-gtk/src/main.rs
  xgettext spore-gtk/po/spore-gtk-ui.pot spore-gtk/po/spore-gtk-code.pot -o spore-gtk/po/spore-gtk.pot
  rm spore-gtk/po/spore-gtk-ui.pot spore-gtk/po/spore-gtk-code.pot 
  for po in spore-gtk/po/*.po; do \
    msgmerge "$po" "spore-gtk/po/spore-gtk.pot" -o "$po"; \
  done
  for po in spore-gtk/po/*.po; do \
    lang=$(basename ${po%.po}); \
    mkdir -p "target/locale/$lang/LC_MESSAGES"; \
    msgfmt "$po" -o "target/locale/$lang/LC_MESSAGES/spore_gtk.mo"; \
  done