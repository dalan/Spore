use crate::preferences;
use crate::tree::FolderVec;
#[cfg(not(windows))]
use crate::view::panoramic;

use super::main_window::MainWindow;
use adw::prelude::*;
use adw::subclass::prelude::*;
use adw::{gio, glib};
use libspore::image_loader::ImageLoader;
use libspore::{Collection, Database, File};
use log::debug;
use smallvec::ToSmallVec;
use tr::tr;

glib::wrapper! {
    pub struct App(ObjectSubclass<imp::App>)
    @extends adw::Application, gtk::Application, gio::Application,
    @implements gio::ActionMap, gio::ActionGroup;
}

impl App {
    pub fn new() -> Self {
        debug!("Setting up application with id '{}'", crate::APP_ID);
        glib::Object::builder()
            .property("application-id", Some(crate::APP_ID))
            .build()
    }

    /// Get main window
    pub fn main_window(&self) -> MainWindow {
        self.imp().main_window()
    }

    /// Get image loader
    pub fn image_loader(&self) -> &ImageLoader {
        &self.imp().image_loader
    }

    /// Get image loader
    #[cfg(not(windows))]
    pub fn panoramic_server(&self) -> &panoramic::Server {
        &self.imp().panoramic_server
    }

    /// Get indexes (collection_index, folder_indexes, file_index)
    pub fn get_indexes(&self) -> (Option<usize>, FolderVec, usize) {
        let pref = &self.imp().config.borrow().pref;
        (
            pref.current.collection_index,
            pref.current.folder_indexes.clone(),
            pref.current.file_index,
        )
    }

    /// Get window size
    pub fn get_window_pref(&self) -> preferences::Window {
        self.imp().config.borrow().pref.window
    }

    /// Open current collection and files
    fn open_current(&self) {
        // Open current collection
        if let (Some(collection_index), _, _) = self.get_indexes() {
            self.main_window()
                .collection_selected(collection_index, None);
        }

        // Open current file
        self.main_window().sidebar_done();

        // Open file
        self.main_window().file_selected();
    }

    /// Call a function on the database
    pub fn inspect_db<T: 'static>(&self, f: impl FnOnce(&Database) -> T) -> T {
        self.imp()
            .db
            .borrow()
            .as_ref()
            .map(|db| f(db))
            .expect("Database not opened")
    }

    /// Call a function on the current collection
    pub fn inspect_collection<T: 'static>(&self, f: impl FnOnce(&Collection) -> T) -> Option<T> {
        if let (Some(collection_index), _, _) = self.get_indexes() {
            Some(
                self.imp()
                    .db
                    .borrow()
                    .as_ref()
                    .map(|db| f(&db.collections()[collection_index]))
                    .expect("Database not opened"),
            )
        } else {
            None
        }
    }

    /// Call a function on all files
    pub fn inspect_files(&self, mut f: impl FnMut(&File, usize)) {
        if let (Some(collection_index), folder_indexes, _) = self.get_indexes() {
            self.imp()
                .db
                .borrow()
                .as_ref()
                .map(|t| {
                    let collection = &t.collections()[collection_index];
                    let folder = folder_indexes
                        .iter()
                        .fold(collection.folder(), |folder, index| {
                            &folder.folders()[*index]
                        });
                    for (index, file) in folder.files().iter().enumerate() {
                        f(&file, index);
                    }
                })
                .expect("Database not opened")
        }
    }

    /// Call a function on a file
    pub fn inspect_file<T: 'static>(
        &self,
        index: Option<usize>,
        f: impl FnOnce(&File, Option<&File>, Option<&File>, usize, usize) -> T,
    ) -> Option<T> {
        if let (Some(collection_index), folder_indexes, file_index) = self.get_indexes() {
            self.imp()
                .db
                .borrow()
                .as_ref()
                .map(|t| {
                    let collection = &t.collections()[collection_index];
                    let folder = folder_indexes
                        .iter()
                        .fold(collection.folder(), |folder, index| {
                            &folder.folders()[*index]
                        });

                    let index = index.unwrap_or(file_index);

                    if let Some(file) = folder.files().get(index) {
                        let previous = if index == 0 {
                            None
                        } else {
                            folder.files().get(index - 1)
                        };
                        let next = folder.files().get(index + 1);
                        Some(f(file, previous, next, index + 1, folder.files().len()))
                    } else {
                        None
                    }
                })
                .expect("Database not opened")
        } else {
            None
        }
    }

    /// Set collection index and return the previous one
    pub fn set_collection_index(&self, new_collection_index: usize) -> Option<usize> {
        let previous = self
            .imp()
            .config
            .borrow_mut()
            .pref
            .current
            .collection_index
            .replace(new_collection_index);
        self.set_folder_indexes(&[]);
        previous
    }

    /// Set current folder indexes
    pub fn set_folder_indexes(&self, indexes: &[usize]) {
        // Clear all cache images
        self.imp().image_loader.clear();

        // Set current folder and file
        self.imp().config.borrow_mut().pref.current.folder_indexes = indexes.to_smallvec();
        self.imp().config.borrow_mut().pref.current.file_index = 0;

        // Set main window
        self.main_window().file_selected();
        self.main_window().sidebar_done();
    }

    /// Set current file index
    pub fn set_file_index(&self, index: usize) {
        self.imp().config.borrow_mut().pref.current.file_index = index;
        self.main_window().file_selected();
    }

    /// Set show metadata
    pub fn set_show_metadata(&self, show_metadata: bool) {
        self.imp().config.borrow_mut().pref.window.show_metadata = show_metadata;
    }

    /// Set window pref
    pub fn set_window_pref(
        &self,
        thumbnails_size: u32,
        sidebar_min_width: u32,
        sidebar_max_width: u32,
    ) {
        let mut config = self.imp().config.borrow_mut();

        config.pref.window.thumbnails_size = thumbnails_size;
        config.pref.window.sidebar_min_width = sidebar_min_width;
        config.pref.window.sidebar_max_width = sidebar_max_width;
    }
}

impl std::default::Default for App {
    fn default() -> Self {
        assert!(
            gtk::is_initialized_main_thread(),
            "Calling gio::Application::default from non-main thread"
        );

        gio::Application::default()
            .expect("Application not initialized")
            .downcast::<App>()
            .expect("Application is wrong subclass")
    }
}

mod imp {
    use crate::preferences;

    use super::*;
    use glib::{WeakRef, clone};
    use gtk::{IconTheme, gdk::Display};
    use libspore::image_loader::ImageLoader;
    use log::error;
    use std::cell::RefCell;

    #[derive(Default)]
    pub struct App {
        main_window: WeakRef<MainWindow>,
        pub(super) db: RefCell<Option<Database>>,
        pub(super) image_loader: ImageLoader,
        #[cfg(not(windows))]
        pub(super) panoramic_server: panoramic::Server,
        pub(super) config: RefCell<preferences::Config>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for App {
        const NAME: &'static str = "SporeApp";
        type Type = super::App;
        type ParentType = adw::Application;
    }

    impl ObjectImpl for App {
        fn constructed(&self) {
            debug!("App::constructed");
            self.parent_constructed();
        }
    }

    impl ApplicationImpl for App {
        fn startup(&self) {
            debug!("App::startup");
            self.parent_startup();

            // Icon
            IconTheme::for_display(&Display::default().unwrap())
                .add_resource_path("/fr/dalan/spore/data/icons");
        }

        fn activate(&self) {
            debug!("App::activate");
            self.parent_activate();

            // Init DB and print an fatal error in case of error
            if let Err(e) = self.init_db() {
                let window = self.main_window();
                window.present();
                error!("Cannot open database ! ({})", e);
                let dialog = adw::AlertDialog::builder()
                    .heading("Error")
                    .body("Cannot open database")
                    .build();
                dialog.add_responses(&[("close", &tr!("Close"))]);
                dialog.set_response_appearance("close", adw::ResponseAppearance::Destructive);
                dialog.present(Some(&window));
                dialog.connect_response(None, |a, _| {
                    a.close();
                    crate::app::App::default().quit();
                });
            } else {
                let window = self.main_window();
                window.set_select_collection();
                window.present();

                // Add Receiver
                let msg_receiver = self.image_loader.get_msg_receiver();
                glib::spawn_future_local(clone!(
                    #[weak]
                    window,
                    async move {
                        loop {
                            if let Ok(msg) = msg_receiver.recv_async().await {
                                match msg {
                                    libspore::image_loader::Msg::ThumbnailAvailable(id) => {
                                        window.thumbnail_available(id);
                                    }
                                    libspore::image_loader::Msg::ImageAvailable(id) => {
                                        window.image_available(id)
                                    }
                                }
                            } else {
                                break;
                            }
                        }
                    }
                ));
            }

            // Open current
            self.obj().open_current();
        }

        fn shutdown(&self) {
            debug!("App::shutdown");

            // Save window size
            let window_size = self.main_window().default_size();
            let mut config = self.config.borrow_mut();
            config.pref.window.size = Some(window_size);
            config.pref.window.is_maximized = self.main_window().is_maximized();

            self.parent_shutdown();
        }
    }
    impl GtkApplicationImpl for App {}
    impl AdwApplicationImpl for App {}

    impl App {
        pub(super) fn main_window(&self) -> MainWindow {
            if let Some(window) = self.main_window.upgrade() {
                window
            } else {
                let window = MainWindow::new(&self.obj());
                self.main_window.set(Some(&window));
                window
            }
        }

        pub fn init_db(&self) -> libspore::Result<()> {
            self.db.replace(Some(Database::open_default()?));
            debug!("Init DB done");
            Ok(())
        }
    }
}
