use glib::Object;
use gtk::{
    Label, ListItem, ListScrollFlags, ListView, SignalListItemFactory, SingleSelection,
    TreeExpander, TreeListModel, TreeListRow, gio, glib, prelude::*,
};
use log::info;
use smallvec::SmallVec;

pub const MAX_DEPTH: usize = 5;
pub type FolderVec = SmallVec<[usize; MAX_DEPTH]>;

mod model {
    use super::*;
    use glib::subclass::types::ObjectSubclassIsExt;

    glib::wrapper! {
        pub struct FolderObject(ObjectSubclass<imp::FolderObject>);
    }

    impl FolderObject {
        pub fn new(indexes: FolderVec) -> Self {
            let obj: FolderObject = Object::builder().build();
            *obj.imp().indexes.borrow_mut() = indexes;
            obj
        }

        pub fn indexes(&self) -> FolderVec {
            self.imp().indexes.borrow().clone()
        }

        pub fn child_nb(&self) -> usize {
            crate::app::App::default()
                .inspect_collection(|collection| {
                    self.imp()
                        .indexes
                        .borrow()
                        .iter()
                        .fold(collection.folder(), |folder, index| {
                            &folder.folders()[*index]
                        })
                        .folders()
                        .len()
                })
                .unwrap_or(0)
        }

        pub fn is_root(&self) -> bool {
            self.imp().indexes.borrow().is_empty()
        }

        pub fn name(&self) -> String {
            crate::app::App::default()
                .inspect_collection(|collection| {
                    self.imp()
                        .indexes
                        .borrow()
                        .iter()
                        .fold(collection.folder(), |folder, index| {
                            &folder.folders()[*index]
                        })
                        .name()
                        .to_string()
                })
                .unwrap_or_default()
        }
    }

    mod imp {
        use super::*;
        use gtk::subclass::prelude::*;
        use std::cell::RefCell;

        // Object holding the state
        #[derive(Default)]
        pub struct FolderObject {
            pub indexes: RefCell<FolderVec>,
        }

        // The central trait for subclassing a GObject
        #[glib::object_subclass]
        impl ObjectSubclass for FolderObject {
            const NAME: &'static str = "FolderObject";
            type Type = super::FolderObject;
        }

        // Trait shared by all GObjects
        impl ObjectImpl for FolderObject {}
    }
}

/// Get tree and current position
///
/// set_pos() must be called after.
pub fn create() -> (ListView, u32) {
    // Create root model
    let model = gio::ListStore::new::<model::FolderObject>();
    model.append(&model::FolderObject::new(
        SmallVec::<[usize; MAX_DEPTH]>::new(),
    ));

    // Create tree model
    let tree_model = TreeListModel::new(model, false, false, |object| {
        // Get `FolderObject`
        let folder_object = object
            .downcast_ref::<model::FolderObject>()
            .expect("The item has to be an `FolderObject`.");

        // Create model for folder children
        let nb_child = folder_object.child_nb();
        if nb_child > 0 {
            let model = gio::ListStore::new::<model::FolderObject>();

            for index in 0..nb_child {
                let mut indexes = folder_object.indexes();
                indexes.push(index);
                model.append(&model::FolderObject::new(indexes));
            }

            Some(model.into())
        } else {
            None
        }
    });

    let factory = SignalListItemFactory::new();
    factory.connect_setup(move |_, list_item| {
        // Get ListItem
        let list_item = list_item
            .downcast_ref::<ListItem>()
            .expect("Needs to be ListItem");

        // Create Label
        let label = Label::builder()
            .halign(gtk::Align::Start)
            .margin_start(5)
            .build();

        // Create tree expander
        let tree_expander = TreeExpander::builder().child(&label).build();

        list_item.set_child(Some(&tree_expander));
    });

    let app = crate::app::App::default();
    let folder_indexes = app.get_indexes().1;

    factory.connect_bind(move |_, list_item| {
        // Get ListItem
        let list_item = list_item
            .downcast_ref::<ListItem>()
            .expect("Needs to be ListItem");

        // Get TreeListRow
        let tree_list_row = list_item
            .item()
            .and_downcast::<TreeListRow>()
            .expect("The item has to be an `FolderObject`.");

        // Get `FolderObject` from `TreeListRow`
        let folder_object = tree_list_row
            .item()
            .and_downcast::<model::FolderObject>()
            .expect("The item has to be an `FolderObject`.");

        // Get `Label` from `ListItem`
        let tree_expander = list_item
            .child()
            .and_downcast::<TreeExpander>()
            .expect("The child has to be a `Label`.");

        // See list row for tree expander
        tree_expander.set_list_row(Some(&tree_list_row));

        // Get `Label` from `ListItem`
        let label = tree_expander
            .child()
            .and_downcast::<Label>()
            .expect("The child has to be a `Label`.");

        // Set "label"
        label.set_label(&folder_object.name());
    });

    // Expand current folder
    let mut pos = 0;
    while let Some(tree_list_row) = tree_model.row(pos) {
        // Get `FolderObject` from `TreeListRow`
        let folder_object = tree_list_row
            .item()
            .and_downcast::<model::FolderObject>()
            .expect("The item has to be an `FolderObject`.");

        if folder_indexes == folder_object.indexes() {
            // Row found, stop it
            break;
        } else if folder_indexes.starts_with(&folder_object.indexes()) {
            tree_list_row.set_expanded(true);
        }

        pos += 1;
    }

    // Selection model
    let selection_model = SingleSelection::new(Some(tree_model));
    selection_model.set_autoselect(false);

    (ListView::new(Some(selection_model), Some(factory)), pos)
}

/// Set tree pos and connect selection changed
pub fn set_pos(tree: &ListView, pos: u32) {
    // Set pos
    tree.scroll_to(pos, ListScrollFlags::SELECT, None);

    // Connect selection changed
    let selection_model = tree
        .model()
        .unwrap()
        .downcast::<SingleSelection>()
        .expect("Should be a SingleSelectionModel");
    selection_model.connect_selection_changed(|selection_model, _, _| {
        // Get TreeListRow
        let tree_list_row = selection_model
            .selected_item()
            .and_downcast::<TreeListRow>()
            .expect("The item has to be an `FolderObject`.");

        // Get `FolderObject` from `TreeListRow`
        let folder_object = tree_list_row
            .item()
            .and_downcast::<model::FolderObject>()
            .expect("The item has to be an `FolderObject`.");

        // Get and set current folder
        let indexes = folder_object.indexes();
        let app = crate::app::App::default();
        app.set_folder_indexes(&indexes);

        info!("Folder {:?} selected", indexes);
    });
}
