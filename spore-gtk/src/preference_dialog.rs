use adw::glib;
use adw::subclass::prelude::*;
use glib::Object;

use crate::app::App;

glib::wrapper! {
    pub struct PreferenceDialog(ObjectSubclass<imp::PreferenceDialog>)
        @extends adw::PreferencesDialog, adw::Dialog, gtk::Widget,
        @implements gtk::Accessible, gtk::Buildable,
                    gtk::ConstraintTarget;
}

impl PreferenceDialog {
    pub fn new() -> Self {
        Object::builder().build()
    }

    fn init_ui(&self) {
        let pref = App::default().get_window_pref();

        self.imp()
            .thumbnails_size_spin_row
            .set_value(pref.thumbnails_size as f64);
        self.imp()
            .sidebar_min_width_spin_row
            .set_value(pref.sidebar_min_width as f64);
        self.imp()
            .sidebar_max_width_spin_row
            .set_value(pref.sidebar_max_width as f64);
    }

    fn save(&self) {
        App::default().set_window_pref(
            self.imp().thumbnails_size_spin_row.value() as u32,
            self.imp().sidebar_min_width_spin_row.value() as u32,
            self.imp().sidebar_max_width_spin_row.value() as u32,
        );
    }
}

mod imp {
    use super::*;
    use adw::SpinRow;
    use glib::subclass::InitializingObject;
    use gtk::{CompositeTemplate, glib};

    // Object holding the state
    #[derive(CompositeTemplate, Default)]
    #[template(resource = "/fr/dalan/spore/ui/preferences_dialog.ui")]
    pub struct PreferenceDialog {
        #[template_child]
        pub thumbnails_size_spin_row: TemplateChild<SpinRow>,
        #[template_child]
        pub sidebar_min_width_spin_row: TemplateChild<SpinRow>,
        #[template_child]
        pub sidebar_max_width_spin_row: TemplateChild<SpinRow>,
    }

    // The central trait for subclassing a GObject
    #[glib::object_subclass]
    impl ObjectSubclass for PreferenceDialog {
        // `NAME` needs to match `class` attribute of template
        const NAME: &'static str = "PreferenceDialog";
        type Type = super::PreferenceDialog;
        type ParentType = adw::PreferencesDialog;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for PreferenceDialog {
        fn constructed(&self) {
            // Call "constructed" on parent
            self.parent_constructed();

            self.obj().init_ui();
        }
    }

    // Trait shared by all widgets
    impl WidgetImpl for PreferenceDialog {}

    // Trait shared by all box
    impl AdwDialogImpl for PreferenceDialog {
        fn closed(&self) {
            AdwDialogImplExt::parent_closed(self);

            self.obj().save();
        }
    }

    // Trait shared by all box
    impl PreferencesDialogImpl for PreferenceDialog {}
}
