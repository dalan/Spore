use anyhow::Result;
use log::{error, info};
use serde::{Deserialize, Serialize};

use crate::tree::FolderVec;

pub const APP_NAME: &'static str = "spore";
pub const CONFIG_NAME: &'static str = "spore-gtk";

#[derive(Debug, Serialize, Deserialize, Default)]
pub struct Current {
    #[serde(default)]
    pub collection_index: Option<usize>,
    #[serde(default)]
    pub folder_indexes: FolderVec,
    #[serde(default)]
    pub file_index: usize,
}

#[derive(Debug, Serialize, Deserialize, Default, Copy, Clone)]
pub struct Window {
    #[serde(default)]
    pub size: Option<(i32, i32)>,
    #[serde(default)]
    pub is_maximized: bool,
    #[serde(default)]
    pub show_metadata: bool,
    #[serde(default = "default_thumbnails_size")]
    pub thumbnails_size: u32,
    #[serde(default = "default_sidebar_min_width")]
    pub sidebar_min_width: u32,
    #[serde(default = "default_sidebar_max_width")]
    pub sidebar_max_width: u32,
}

pub const fn default_thumbnails_size() -> u32 {
    200
}

pub const fn default_sidebar_min_width() -> u32 {
    250
}

pub const fn default_sidebar_max_width() -> u32 {
    400
}

#[derive(Debug, Serialize, Deserialize, Default)]
pub struct Preferences {
    #[serde(default)]
    pub current: Current,
    #[serde(default)]
    pub window: Window,
}

impl Preferences {
    fn load() -> Result<Self> {
        let pref = confy::load(APP_NAME, Some(CONFIG_NAME))?;
        Ok(pref)
    }

    fn save(&self) -> Result<()> {
        confy::store(APP_NAME, Some(CONFIG_NAME), self)?;
        info!("Preferences save successfuly");
        Ok(())
    }
}

#[derive(Debug)]
pub struct Config {
    pub pref: Preferences,
}

impl Default for Config {
    fn default() -> Self {
        let pref = match Preferences::load() {
            Ok(pref) => pref,
            Err(e) => {
                error!("Cannot open preferences ({})", e);
                Preferences::default()
            }
        };
        Self { pref }
    }
}

impl Drop for Config {
    fn drop(&mut self) {
        if let Err(e) = self.pref.save() {
            error!("Cannot save preferences ({})", e);
        }
    }
}
