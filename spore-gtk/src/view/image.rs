use gtk::{
    Picture,
    gdk::{self, MemoryTexture, Texture},
    glib,
    prelude::*,
};
use image::DynamicImage;
use log::trace;

pub fn get_default_paintable() -> impl IsA<gdk::Paintable> {
    Texture::from_resource("/fr/dalan/spore/data/icons/scalable/apps/image-missing.svg")
}

pub fn get_paintable(img: &DynamicImage) -> impl IsA<gdk::Paintable> {
    match img {
        DynamicImage::ImageLuma8(image_buffer) => {
            let width = image_buffer.width();
            MemoryTexture::new(
                width as i32,
                image_buffer.height() as i32,
                gdk::MemoryFormat::G8,
                &glib::Bytes::from_owned(image_buffer.to_vec()),
                width as usize * 1,
            )
        }
        DynamicImage::ImageLumaA8(image_buffer) => {
            let width = image_buffer.width();
            MemoryTexture::new(
                width as i32,
                image_buffer.height() as i32,
                gdk::MemoryFormat::G8a8,
                &glib::Bytes::from_owned(image_buffer.to_vec()),
                width as usize * 2,
            )
        }
        DynamicImage::ImageRgb8(image_buffer) => {
            let width = image_buffer.width();
            MemoryTexture::new(
                width as i32,
                image_buffer.height() as i32,
                gdk::MemoryFormat::R8g8b8,
                &glib::Bytes::from_owned(image_buffer.to_vec()),
                width as usize * 3,
            )
        }
        DynamicImage::ImageRgba8(image_buffer) => {
            let width = image_buffer.width();
            MemoryTexture::new(
                width as i32,
                image_buffer.height() as i32,
                gdk::MemoryFormat::R8g8b8a8,
                &glib::Bytes::from_owned(image_buffer.to_vec()),
                width as usize * 4,
            )
        }
        DynamicImage::ImageLuma16(image_buffer) => {
            let width = image_buffer.width();
            MemoryTexture::new(
                width as i32,
                image_buffer.height() as i32,
                gdk::MemoryFormat::G16,
                &glib::Bytes::from_owned(bytemuck::cast_slice(&image_buffer.as_raw()).to_vec()),
                width as usize * 1 * 2,
            )
        }
        DynamicImage::ImageLumaA16(image_buffer) => {
            let width = image_buffer.width();
            MemoryTexture::new(
                width as i32,
                image_buffer.height() as i32,
                gdk::MemoryFormat::G16a16,
                &glib::Bytes::from_owned(bytemuck::cast_slice(&image_buffer.as_raw()).to_vec()),
                width as usize * 1 * 2,
            )
        }
        DynamicImage::ImageRgb16(image_buffer) => {
            let width = image_buffer.width();
            MemoryTexture::new(
                width as i32,
                image_buffer.height() as i32,
                gdk::MemoryFormat::R16g16b16,
                &glib::Bytes::from_owned(bytemuck::cast_slice(&image_buffer.as_raw()).to_vec()),
                width as usize * 3 * 2,
            )
        }
        DynamicImage::ImageRgba16(image_buffer) => {
            let width = image_buffer.width();
            MemoryTexture::new(
                width as i32,
                image_buffer.height() as i32,
                gdk::MemoryFormat::R16g16b16a16,
                &glib::Bytes::from_owned(bytemuck::cast_slice(&image_buffer.as_raw()).to_vec()),
                width as usize * 4 * 2,
            )
        }
        DynamicImage::ImageRgb32F(image_buffer) => {
            let width = image_buffer.width();
            MemoryTexture::new(
                width as i32,
                image_buffer.height() as i32,
                gdk::MemoryFormat::R32g32b32Float,
                &glib::Bytes::from_owned(bytemuck::cast_slice(&image_buffer.as_raw()).to_vec()),
                width as usize * 3 * 4,
            )
        }
        DynamicImage::ImageRgba32F(image_buffer) => {
            let width = image_buffer.width();
            MemoryTexture::new(
                width as i32,
                image_buffer.height() as i32,
                gdk::MemoryFormat::R32g32b32a32Float,
                &glib::Bytes::from_owned(bytemuck::cast_slice(&image_buffer.as_raw()).to_vec()),
                width as usize * 4 * 4,
            )
        }
        _ => todo!("Unknown image encoding"),
    }
}

pub fn get(img: Option<DynamicImage>) -> Picture {
    let picture = Picture::builder().build();

    trace!("Set image paintable");
    if let Some(img) = img {
        picture.set_paintable(Some(&get_paintable(&img)));
    } else {
        picture.set_paintable(Some(&get_default_paintable()));
    }

    picture
}
