use gtk::Video;
use std::path::Path;

/*
Not working for now : see https://gitlab.gnome.org/GNOME/gtk/-/merge_requests/7186
pub fn from_reader(reader : impl Read + Send + 'static) -> Video {
    let inpu_stream = ReadInputStream::new(reader);
    let media_file = MediaFile::for_input_stream(&inpu_stream);
    let video = Video::for_media_stream(Some(&media_file));
    video.set_autoplay(true);
    video
}
*/

pub fn from_path(path: &Path) -> Video {
    let video = Video::for_filename(Some(path));
    video.set_autoplay(true);
    video
}
