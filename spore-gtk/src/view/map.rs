use geo_types::MultiLineString;
use gtk::{Image, gdk::RGBA};
use libshumate::{
    Coordinate, MAP_SOURCE_OSM_MAPNIK, MapSourceRegistry, MarkerLayer, PathLayer, Point, SimpleMap,
};

pub fn from_track(track: &MultiLineString) -> SimpleMap {
    // Create map
    let map_source_reg = MapSourceRegistry::with_defaults();
    let map = SimpleMap::new();
    let viewport = map.viewport().unwrap();
    let map_source = map_source_reg.by_id(MAP_SOURCE_OSM_MAPNIK).unwrap();
    map.set_map_source(Some(&map_source));

    // Create path mayer
    let path = PathLayer::builder()
        .stroke_width(4.)
        .stroke_color(&RGBA::new(0.11, 0.07, 0.67, 1.0))
        .viewport(&viewport)
        .build();
    track.iter().for_each(|line| {
        line.coords().for_each(|coord| {
            path.add_node(&Coordinate::new_full(coord.y, coord.x));
        });
    });
    map.add_overlay_layer(&path);

    let first_loc = track.iter().next().unwrap().coords().next().unwrap();
    let last_loc = track.iter().last().unwrap().coords().last().unwrap();

    // Create marker
    let marker = MarkerLayer::builder()
        .viewport(&viewport)
        .selection_mode(gtk::SelectionMode::Single)
        .build();
    marker.add_marker(
        &Point::builder()
            .latitude(first_loc.y)
            .longitude(first_loc.x)
            .child(&Image::from_icon_name("media-playback-start"))
            .build(),
    );
    marker.add_marker(
        &Point::builder()
            .latitude(last_loc.y)
            .longitude(last_loc.x)
            .child(&Image::from_icon_name("media-playback-stop"))
            .build(),
    );
    map.add_overlay_layer(&marker);

    map
}

pub fn set_map_zoom(map: &SimpleMap, track: &MultiLineString, width: i32, height: i32) {
    // Get min/max
    let (min_lat, max_lat, min_lon, max_lon) = track.iter().fold(
        (f64::MAX, f64::MIN, f64::MAX, f64::MIN),
        |(min_lat, max_lat, min_lon, max_long), line| {
            line.coords().fold(
                (min_lat, max_lat, min_lon, max_long),
                |(min_lat, max_lat, min_lon, max_lon), coord| {
                    (
                        f64::min(min_lat, coord.y),
                        f64::max(max_lat, coord.y),
                        f64::min(min_lon, coord.x),
                        f64::max(max_lon, coord.x),
                    )
                },
            )
        },
    );

    // Zoom
    let zoom = if width as f64 / height as f64 > (max_lon - min_lon) / (max_lat - min_lat) {
        (height as f64 / (max_lat - min_lat)).log2() - 0.5
    } else {
        (width as f64 / (max_lon - min_lon)).log2() - 0.5
    };

    map.map().unwrap().go_to_full_with_duration(
        (max_lat + min_lat) / 2.,
        (max_lon + min_lon) / 2.,
        zoom,
        0,
    );
}
