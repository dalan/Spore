use image::{DynamicImage, ImageFormat};
use libspore::file::PanoInfos;
use log::{debug, info};
use rand::distr::SampleString;
use rust_embed::RustEmbed;
use std::{
    io::Cursor,
    sync::{Arc, Mutex},
};
use warp::Filter;
use webkit6::{WebView, prelude::*};

#[derive(RustEmbed)]
#[folder = "embed/panoramic"]
struct Embed;

/// Get data from embed
fn get_file_data(filename: &str) -> String {
    if let Some(file) = Embed::get(filename) {
        std::str::from_utf8(file.data.as_ref())
            .unwrap_or("")
            .to_string()
    } else {
        "".to_string()
    }
}

/// Server for panoramic viewer
#[derive(Debug)]
pub struct Server {
    port: u16,
    img: Arc<Mutex<Option<Arc<DynamicImage>>>>,
    infos: Arc<Mutex<Option<PanoInfos>>>,
    user_agent: String,
}

impl Default for Server {
    fn default() -> Self {
        Self::new()
    }
}

impl Server {
    pub fn new() -> Self {
        let port = portpicker::pick_unused_port().expect("Cannot find ununsed port");
        let img: Arc<Mutex<Option<Arc<DynamicImage>>>> = Arc::new(Mutex::new(None));
        let user_agent = rand::distr::Alphanumeric.sample_string(&mut rand::rng(), 32);
        let infos: Arc<Mutex<Option<PanoInfos>>> = Arc::new(Mutex::new(None));

        let _hdl = std::thread::spawn({
            let img = img.clone();
            let user_agent = user_agent.clone();
            let infos = infos.clone();
            move || {
                info!("Create tokio runtime");
                let async_runtime = tokio::runtime::Builder::new_current_thread()
                    .enable_all()
                    .build()
                    .unwrap();
                info!("Create server response");

                // HTML
                let index = warp::path("index.html").map({
                    let img = img.clone();
                    move || {
                        debug!("Request `index.html`");
                        let datas = if img.lock().unwrap().is_some() {
                            let datas = get_file_data("index.html");
                            if let Some(infos) = infos.lock().unwrap().clone() {
                                datas
                                    .replace(
                                        "__FULL_WIDTH__",
                                        &infos.full_pano_width_pixels.to_string(),
                                    )
                                    .replace(
                                        "__FULL_HEIGHT__",
                                        &infos.full_pano_height_pixels.to_string(),
                                    )
                                    .replace(
                                        "__CROPPED_WIDTH__",
                                        &infos.cropped_area_image_width_pixels.to_string(),
                                    )
                                    .replace(
                                        "__CROPPED_HEIGHT__",
                                        &infos.cropped_area_image_height_pixels.to_string(),
                                    )
                                    .replace(
                                        "__CROPPED_X__",
                                        &infos.cropped_area_left_pixels.to_string(),
                                    )
                                    .replace(
                                        "__CROPPED_Y__",
                                        &infos.cropped_area_top_pixels.to_string(),
                                    )
                            } else {
                                datas
                            }
                        } else {
                            get_file_data("loader.html")
                        };
                        warp::reply::html(datas)
                    }
                });

                // CSS
                let css = warp::path!("css" / String).map(move |val: String| {
                    debug!("Request css `{}`", &val);
                    let datas = get_file_data(&format!("css/{}", val));
                    warp::http::Response::builder().body(datas)
                });

                // JS
                let js = warp::path!("js" / String).map(move |val: String| {
                    debug!("Request js `{}`", &val);
                    let datas = get_file_data(&format!("js/{}", val));
                    warp::http::Response::builder().body(datas)
                });

                // Image
                let img = warp::path!("img").and(warp::header("user-agent")).map({
                    let img = img.clone();
                    move |agent: String| {
                        if agent == user_agent {
                            if let Some(img_data) = img.lock().unwrap().clone() {
                                let mut png = Cursor::new(Vec::new());
                                if img_data.write_to(&mut png, ImageFormat::Png).is_err() {
                                    Vec::new()
                                } else {
                                    png.into_inner()
                                }
                            } else {
                                Vec::new()
                            }
                        } else {
                            Vec::new()
                        }
                    }
                });

                info!("Launch webserver");
                async_runtime.block_on(
                    warp::serve(img.or(index).or(css).or(js)).run(([127, 0, 0, 1], port)),
                );
            }
        });
        Server {
            port,
            img,
            user_agent,
            infos,
        }
    }

    /// Set image
    pub fn set_img(&self, img: Option<Arc<DynamicImage>>) {
        *self.img.lock().unwrap() = img;
    }

    /// Set panormaic infos
    pub fn set_infos(&self, infos: Option<PanoInfos>) {
        *self.infos.lock().unwrap() = infos;
    }

    /// Get a new webview on the server
    pub fn get_webview(&self) -> WebView {
        let webview = webkit6::WebView::builder()
            .settings(
                &webkit6::Settings::builder()
                    .user_agent(&self.user_agent)
                    .build(),
            )
            .build();

        webview.load_uri(&format!("http://127.0.0.1:{}/index.html", self.port));

        webview
    }
}
