use adw::glib;
use adw::prelude::ActionRowExt;
use adw::subclass::prelude::*;
use glib::Object;
use gtk::prelude::WidgetExt;
use humantime::format_duration;
use libspore::File;
use tr::tr;

glib::wrapper! {
    pub struct MetadataBox(ObjectSubclass<imp::MetadataBox>)
        @extends gtk::Box, gtk::Widget,
        @implements gtk::Accessible, gtk::Buildable,
                    gtk::ConstraintTarget, gtk::Orientable;
}

impl MetadataBox {
    pub fn new() -> Self {
        Object::builder().build()
    }

    pub fn update(&self, file: &File) {
        let imp = self.imp();

        imp.path_row
            .set_subtitle(file.filename().file_name().unwrap_or(""));
        imp.time_row.set_subtitle(&file.time().to_string());

        let typ = match file.typ() {
            libspore::file::Type::Image => tr!("Image"),
            libspore::file::Type::Video => tr!("Video"),
            libspore::file::Type::PanoramicImage => tr!("Panoramic image"),
            libspore::file::Type::Gpx => tr!("GPX"),
        };
        imp.type_row.set_subtitle(&typ);

        if let Some(res) = file.resolution() {
            imp.resolution_row.set_subtitle(&res.to_string());
            imp.resolution_row.set_visible(true);
        } else {
            imp.resolution_row.set_visible(false);
        }

        if let Some(location) = file.location() {
            imp.location_row.set_subtitle(&tr!(
                "Latitude: {}\nLongitude: {}\n{}",
                format!("{:.6}", location.point.x()),
                format!("{:.6}", location.point.y()),
                &location.to_string()
            ));
            imp.location_row.set_visible(true);
        } else {
            imp.location_row.set_visible(false);
        }

        if let Some(duration) = file.duration() {
            imp.duration_row
                .set_subtitle(&format_duration(*duration).to_string());
            imp.duration_row.set_visible(true);
        } else {
            imp.duration_row.set_visible(false);
        }
    }
}

mod imp {
    use crate::app::App;

    use super::*;
    use adw::ActionRow;
    use glib::subclass::InitializingObject;
    use gtk::{Button, CompositeTemplate, glib};
    use log::{debug, warn};

    // Object holding the state
    #[derive(CompositeTemplate, Default)]
    #[template(resource = "/fr/dalan/spore/ui/metadata.ui")]
    pub struct MetadataBox {
        #[template_child]
        pub path_row: TemplateChild<ActionRow>,
        #[template_child]
        pub time_row: TemplateChild<ActionRow>,
        #[template_child]
        pub type_row: TemplateChild<ActionRow>,
        #[template_child]
        pub resolution_row: TemplateChild<ActionRow>,
        #[template_child]
        pub location_row: TemplateChild<ActionRow>,
        #[template_child]
        pub duration_row: TemplateChild<ActionRow>,
    }

    // The central trait for subclassing a GObject
    #[glib::object_subclass]
    impl ObjectSubclass for MetadataBox {
        // `NAME` needs to match `class` attribute of template
        const NAME: &'static str = "MetadataBox";
        type Type = super::MetadataBox;
        type ParentType = gtk::Box;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
            klass.bind_template_callbacks();
        }

        fn instance_init(obj: &InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for MetadataBox {
        fn constructed(&self) {
            // Call "constructed" on parent
            self.parent_constructed();
        }
    }

    // Trait shared by all widgets
    impl WidgetImpl for MetadataBox {}

    // Trait shared by all box
    impl BoxImpl for MetadataBox {}

    #[gtk::template_callbacks]
    impl MetadataBox {
        #[template_callback]
        fn on_filename_copy(_button: &Button) {
            debug!("Copy filename");
            App::default().inspect_file(None, |file, _, _, _, _| {
                let path = file.filename();
                debug!("Copy `{}`", path);
                if let Err(e) =
                    arboard::Clipboard::new().and_then(|mut cb| cb.set_text(path.as_str()))
                {
                    warn!("Cannot copy `{}`: {}", path, e);
                }
            });
        }

        #[template_callback]
        fn on_filename_open(_button: &Button) {
            App::default().inspect_file(None, |file, _, _, _, _| {
                let path = file.filename();
                debug!("Open `{}`", path);
                if let Err(e) = opener::open(path) {
                    warn!("Cannot open `{}`: {}", path, e);
                }
            });
        }

        #[template_callback]
        fn on_location_open(_button: &Button) {
            debug!("Open location");
            App::default().inspect_file(None, |file, _, _, _, _| {
                if let Err(e) = opener::open(format!(
                    "geo:{:.6},{:.6}",
                    file.location().unwrap().point.x(),
                    file.location().unwrap().point.y()
                )) {
                    warn!("Cannot open location: {}", e);
                }
            });
        }
    }
}
