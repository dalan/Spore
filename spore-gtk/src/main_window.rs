use adw::prelude::*;
use adw::subclass::prelude::*;
use adw::{gio, glib};
use glib::Object;
use gtk::gio::ActionEntry;
use gtk::glib::{clone, MainContext};
use gtk::{EventControllerScroll, EventControllerScrollFlags, GestureClick, Image, Label, Orientation, Picture, Widget};
use libspore::Item;
use log::{debug, error, info, trace};
#[cfg(not(windows))]
use webkit6::{WebView, prelude::WebViewExt};

use crate::app::App;
use crate::{thumbnails, view};

glib::wrapper! {
    pub struct MainWindow(ObjectSubclass<imp::MainWindow>)
        @extends adw::ApplicationWindow, gtk::Window, gtk::Widget,
        @implements gio::ActionGroup, gio::ActionMap, gtk::Accessible, gtk::Buildable,
                    gtk::ConstraintTarget, gtk::Native, gtk::Root, gtk::ShortcutManager;
}

impl MainWindow {
    pub fn new(app: &App) -> Self {
        // Create new window
        Object::builder().property("application", app).build()
    }

    /// Setup action on the main window
    ///
    /// * open_about
    /// * previous
    /// * next
    fn setup_actions(&self) {
        // Open about
        let action_open_about = ActionEntry::builder("open_about")
            .activate(move |window: &Self, _action, _parameter| {
                debug!("Open about window");
                let about_dialog = crate::about_dialog::new();
                about_dialog.present(Some(window))
            })
            .build();

        // Open preferences
        let action_open_pref = ActionEntry::builder("open_preferences")
            .activate(move |window: &Self, _action, _parameter| {
                debug!("Open preferences");
                let pref_dialog = crate::preference_dialog::PreferenceDialog::new();
                pref_dialog.present(Some(window))
            })
            .build();

        // Previous
        let action_previous = ActionEntry::builder("previous")
            .activate(move |window: &Self, _action, _parameter| {
                debug!("Click previous");
                window.imp().thumbnail_list.borrow().scroll(-1);
            })
            .build();

        // Next
        let action_next = ActionEntry::builder("next")
            .activate(move |window: &Self, _action, _parameter| {
                debug!("Click next");
                window.imp().thumbnail_list.borrow().scroll(1);
            })
            .build();

        self.add_action_entries([
            action_open_about,
            action_open_pref,
            action_previous,
            action_next,
        ]);
    }

    fn init_ui(&self) {
        debug!("Init UI");

        //if self.collections().n_items() > 0 {
        self.imp().stack.set_visible_child_name("main");
        //} else {
        //    self.imp().stack.set_visible_child_name("placeholder");
        //}

        // Add click callback
        let thumbnails_progress_bar_bin = self.imp().thumbnails_progress_bar_bin.clone();
        let progress_bar_click_controller = GestureClick::new();
        progress_bar_click_controller.connect_pressed(clone!(
            #[weak]
            thumbnails_progress_bar_bin,
            #[weak(rename_to = main_window)]
            self,
            move |_controller: &GestureClick,_n_press,x,y| {
                debug!("Click on progress bar at {x} {y}");
                
                // Get index
                let ratio = x as f64 / thumbnails_progress_bar_bin.size(Orientation::Horizontal) as f64;
                let index = (crate::app::App::default().inspect_file(None, | _ , _, _, _, len| {len}).unwrap_or(0) as f64 * ratio) as usize;

                // Scroll
                main_window.imp().thumbnail_list.borrow().scroll_to(index);
        }));
        thumbnails_progress_bar_bin.add_controller(progress_bar_click_controller);
        let scroll_controler = EventControllerScroll::new(
            EventControllerScrollFlags::VERTICAL | EventControllerScrollFlags::DISCRETE,
        );
        scroll_controler.connect_scroll(clone!(
            #[weak(rename_to = main_window)]
            self,
            #[upgrade_or]
            glib::Propagation::Proceed,
            move |_event_controller, _dx, dy| {
                let ratio = (crate::app::App::default().inspect_file(None, | _ , _, _, _, len| {len}).unwrap_or(0) / 10) as i32;
                if dy > 0. {
                    main_window.imp().thumbnail_list.borrow().scroll(ratio);
                } else {
                    main_window.imp().thumbnail_list.borrow().scroll(-ratio);
                }
                glib::Propagation::Stop
            }
        ));
        thumbnails_progress_bar_bin.add_controller(scroll_controler);

        // Set thumbnail
        self.imp()
            .thumbnails_scrolled_window
            .set_child(Some(self.imp().thumbnail_list.borrow().view()));

        // Set size
        let window_config = App::default().get_window_pref();
        if let Some(window_size) = window_config.size {
            self.set_default_size(window_size.0, window_size.1);
        }
        if window_config.is_maximized {
            self.maximize();
        }
        self.imp()
            .split_view
            .set_min_sidebar_width(window_config.sidebar_min_width as f64);
        self.imp()
            .split_view
            .set_max_sidebar_width(window_config.sidebar_max_width as f64);

        // Set show metadata
        self.imp()
            .metadata_button
            .set_active(window_config.show_metadata);
    }

    /// Create select collection values based on database
    pub fn set_select_collection(&self) {
        self.app().inspect_db(|db| {
            for name in db.collections().iter().map(|collection| collection.name()) {
                let list_box = gtk::Box::builder()
                    .margin_start(10)
                    .margin_end(10)
                    .margin_top(5)
                    .margin_bottom(5)
                    .build();
                let list_label = gtk::Label::builder()
                    .label(name)
                    .xalign(0.0)
                    .margin_end(50)
                    .hexpand(true)
                    .build();
                let selected_image = gtk::Image::builder()
                    .icon_name("emblem-ok-symbolic")
                    .icon_size(gtk::IconSize::Normal)
                    .visible(false)
                    .build();
                list_box.append(&list_label);
                list_box.append(&selected_image);
                self.imp().select_collection_listbox.append(&list_box);
            }
        });
    }

    /// Hide sidebar and update files thumbnails
    pub fn sidebar_done(&self) {
        self.imp().split_view.set_show_content(true);

        // Update thumbnail model
        self.imp().thumbnail_list.borrow_mut().update_model();

        // Wait rendering of main window
        if self.size(gtk::Orientation::Horizontal) == 0 {
            let main_context = MainContext::default();
            while main_context.pending() {
                main_context.iteration(true);
            }
        }

        // Scroll thumbail to current and set focus
        self.imp().thumbnail_list.borrow_mut().scroll_to_current();
        self.imp().thumbnail_list.borrow().view().grab_focus();
    }

    pub fn collection_selected(
        &self,
        collection_index: usize,
        old_collection_index: Option<usize>,
    ) {
        // Get widgets
        let listbox = self.imp().select_collection_listbox.clone();
        let row = listbox.row_at_index(collection_index as i32).unwrap();
        let list_box = row.child().unwrap().downcast::<gtk::Box>().unwrap();
        let label: Label = list_box.first_child().unwrap().downcast::<Label>().unwrap();
        let selected_image = label.next_sibling().unwrap().downcast::<Image>().unwrap();

        // Get name
        let collection_name = label.text();

        info!(
            "Collection index {} `{}` selected",
            collection_index, collection_name
        );

        // Show current selected collection
        self.imp()
            .select_collection_label
            .set_text(&collection_name);
        selected_image.set_visible(true);

        // Delete thumbnails and view
        self.imp().view_overlay.set_child(Widget::NONE);

        // Hide previous selected image
        if old_collection_index.unwrap_or(0) != collection_index {
            listbox
                .row_at_index(old_collection_index.unwrap_or(0) as i32)
                .unwrap()
                .child()
                .unwrap()
                .downcast::<gtk::Box>()
                .unwrap()
                .first_child()
                .unwrap()
                .next_sibling()
                .unwrap()
                .downcast::<Image>()
                .unwrap()
                .set_visible(false);
        }

        // Set tree
        let (tree, tree_pos) = crate::tree::create();
        self.imp()
            .collection_scrolled_window_tree
            .set_child(Some(&tree));
        // Wait rendering of tree
        let main_context = MainContext::default();
        while main_context.pending() {
            main_context.iteration(true);
        }
        crate::tree::set_pos(&tree, tree_pos);
    }

    /// Show file
    pub fn file_selected(&self) {
        let app = self.app();

        if app
            .inspect_file(None, |file, previous, next, index, len| {
                trace!("Open file `{}`", file.filename());
                let overlay = self.imp().view_overlay.clone();
                match file.typ() {
                    libspore::file::Type::Image => {
                        if let Some(img) = app.image_loader().get(file.id()) {
                            overlay.set_child(Some(
                                &Picture::builder()
                                    .paintable(&view::image::get_paintable(&img))
                                    .build(),
                            ));
                        } else {
                            overlay.set_child(Some(
                                &Picture::builder()
                                    .paintable(&view::image::get_default_paintable())
                                    .build(),
                            ));
                            if let Err(e) = app.image_loader().load(file) {
                                error!("Image loader closed ({})", e);
                            }
                        }
                    }
                    libspore::file::Type::Video => overlay
                        .set_child(Some(&view::video::from_path(file.filename().as_std_path()))),
                    libspore::file::Type::PanoramicImage => {
                        #[cfg(not(windows))]
                        {
                            let panoramic_server = app.panoramic_server();

                            panoramic_server.set_infos(file.pano_info().ok());

                            if let Some(img) = app.image_loader().get(file.id()) {
                                panoramic_server.set_img(Some(img));
                            } else {
                                panoramic_server.set_img(None);

                                if let Err(e) = app.image_loader().load(file) {
                                    error!("Image loader closed ({})", e);
                                }
                            }

                            overlay.set_child(Some(&panoramic_server.get_webview()))
                        }
                    }
                    libspore::file::Type::Gpx => {
                        if let Some(track) = file.track().ok().flatten() {
                            let map = view::map::from_track(&track);
                            overlay.set_child(Some(&map));
                            view::map::set_map_zoom(
                                &map,
                                &track,
                                overlay.width(),
                                overlay.height(),
                            );
                        } else {
                            overlay.set_child(Widget::NONE);
                        }
                    }
                }

                // Preload files
                if let Some(previous) = previous {
                    if previous.typ() == libspore::file::Type::Image
                        && app.image_loader().get(previous.id()).is_none()
                    {
                        if let Err(e) = app.image_loader().load(previous) {
                            error!("Image loader closed ({})", e);
                        }
                    }
                }
                if let Some(next) = next {
                    if next.typ() == libspore::file::Type::Image
                        && app.image_loader().get(next.id()).is_none()
                    {
                        if let Err(e) = app.image_loader().load(next) {
                            error!("Image loader closed ({})", e);
                        }
                    }
                }

                // Show overlay buttons
                self.imp().previous_button.set_visible(true);
                self.imp().next_button.set_visible(true);
                self.imp().previous_button.set_sensitive(previous.is_some());
                self.imp().next_button.set_sensitive(next.is_some());

                // Show metadata
                self.imp().metadata_box.update(file);
                self.imp()
                    .metadata_box
                    .set_visible(app.get_window_pref().show_metadata);

                // Set progress
                self.imp().thumbnails_progress_bar.set_fraction(index as f64 / len as f64);
                self.imp().thumbnails_progress_label.set_label(&format!("<span font=\"18\">{index} / {len}</span>"));
            })
            .is_none()
        {
            debug!("No file");
            self.imp().view_overlay.set_child(Widget::NONE);

            // Hide overlay buttons
            self.imp().previous_button.set_visible(false);
            self.imp().next_button.set_visible(false);

            // Hide metadata
            self.imp().metadata_box.set_visible(false);

            // Reset progress
            self.imp().thumbnails_progress_bar.set_fraction(0.0);
            self.imp().thumbnails_progress_label.set_label("");
        }
        trace!("Open file done");
    }

    /// Indicate a thumbnail available for a file
    pub fn thumbnail_available(&self, id: i64) {
        self.imp().thumbnail_list.borrow_mut().add_thumbnail(id);
    }

    /// Image available
    pub fn image_available(&self, id: i64) {
        let app = self.app();

        let (is_current_file, typ) = app
            .inspect_file(None, |file, _previous, _next, _index, _len| (file.id() == id, file.typ()))
            .unwrap_or((false, libspore::file::Type::Image));

        if is_current_file {
            if let Some(img) = app.image_loader().get(id) {
                match typ {
                    libspore::file::Type::Image => {
                        self.imp().view_overlay.set_child(Some(
                            &Picture::builder()
                                .paintable(&view::image::get_paintable(&img))
                                .build(),
                        ));
                    }
                    libspore::file::Type::PanoramicImage => {
                        #[cfg(not(windows))]
                        {
                            app.panoramic_server().set_img(Some(img));
                            self.imp()
                                .view_overlay
                                .child()
                                .and_downcast::<WebView>()
                                .unwrap()
                                .reload();
                        }
                    }
                    _ => (),
                }
            }
        }
    }

    /// Get app
    pub fn app(&self) -> App {
        self.application()
            .and_downcast()
            .expect("Application must be App subclass")
    }
}

mod imp {
    use std::cell::RefCell;

    use crate::metadata::MetadataBox;

    use super::*;
    use adw::NavigationSplitView;
    use glib::subclass::InitializingObject;
    use gtk::{
        glib, Button, CompositeTemplate, Label, ListBox, ListBoxRow, Overlay, Popover, ProgressBar, ScrolledWindow, Stack, ToggleButton
    };

    // Object holding the state
    #[derive(CompositeTemplate, Default)]
    #[template(resource = "/fr/dalan/spore/ui/main_window.ui")]
    pub struct MainWindow {
        #[template_child]
        pub stack: TemplateChild<Stack>,
        #[template_child]
        pub split_view: TemplateChild<NavigationSplitView>,
        #[template_child]
        pub select_collection_listbox: TemplateChild<ListBox>,
        #[template_child]
        pub select_collection_label: TemplateChild<Label>,
        #[template_child]
        pub select_collection_popover: TemplateChild<Popover>,
        #[template_child]
        pub collection_scrolled_window_tree: TemplateChild<ScrolledWindow>,
        #[template_child]
        pub thumbnails_scrolled_window: TemplateChild<ScrolledWindow>,
        #[template_child]
        pub thumbnails_progress_bar: TemplateChild<ProgressBar>,
        #[template_child]
        pub thumbnails_progress_bar_bin: TemplateChild<adw::Bin>,
        #[template_child]
        pub thumbnails_progress_label: TemplateChild<Label>,
        #[template_child]
        pub view_overlay: TemplateChild<Overlay>,
        #[template_child]
        pub previous_button: TemplateChild<Button>,
        #[template_child]
        pub next_button: TemplateChild<Button>,
        #[template_child]
        pub metadata_box: TemplateChild<MetadataBox>,
        #[template_child]
        pub metadata_button: TemplateChild<ToggleButton>,
        pub thumbnail_list: RefCell<thumbnails::List>,
    }

    // The central trait for subclassing a GObject
    #[glib::object_subclass]
    impl ObjectSubclass for MainWindow {
        // `NAME` needs to match `class` attribute of template
        const NAME: &'static str = "SporeMainWindow";
        type Type = super::MainWindow;
        type ParentType = adw::ApplicationWindow;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
            klass.bind_template_callbacks();
        }

        fn instance_init(obj: &InitializingObject<Self>) {
            obj.init_template();
        }
    }

    #[gtk::template_callbacks]
    impl MainWindow {
        /// Callback when a collection is selected
        #[template_callback]
        fn collection_selected(&self, row: &ListBoxRow, _listbox: &ListBox) {
            // Get collection index
            let collection_index = row.index();

            // Set collection
            let old_collection_index = self
                .obj()
                .app()
                .set_collection_index(collection_index as usize);

            self.obj()
                .collection_selected(collection_index as usize, old_collection_index);
            self.select_collection_popover.popdown();
        }

        /// Callback when a collection is selected
        #[template_callback]
        fn metadata_toggled(&self, button: &ToggleButton) {
            let show_metadata = button.is_active();

            let app = App::default();
            app.set_show_metadata(show_metadata);
            if app.inspect_file(None, |_, _, _, _index, _len| {}).is_some() {
                // Show metadata
                self.metadata_box.set_visible(show_metadata);
            } else {
                // Hide metadata
                self.metadata_box.set_visible(false);
            }
        }
    }

    impl ObjectImpl for MainWindow {
        fn constructed(&self) {
            // Call "constructed" on parent
            self.parent_constructed();

            // Add actions
            self.obj().setup_actions();

            // Init UI
            self.obj().init_ui();
        }
    }

    // Trait shared by all widgets
    impl WidgetImpl for MainWindow {}

    // Trait shared by all windows
    impl WindowImpl for MainWindow {}

    // Trait shared by all application windows
    impl ApplicationWindowImpl for MainWindow {}

    // Trait shared by all application windows
    impl AdwApplicationWindowImpl for MainWindow {}
}
