use itertools::Itertools;
use shadow_rs::{formatcp, shadow};
use tr::tr;

pub fn new() -> adw::AboutDialog {
    adw::AboutDialog::builder()
        .application_name("Spore")
        .application_icon("fr.dalan.spore")
        .version(env!("CARGO_PKG_VERSION"))
        .developer_name("Rémi BERTHO")
        .website("https://dalan.fr")
        .issue_url("https://codeberg.org/dalan/Spore/issues")
        .license_type(gtk::License::Gpl30)
        .copyright("Copyright © 2023–2025 Rémi BERTHO")
        .debug_info(debug_info())
        .build()
}

shadow!(build);

fn debug_info() -> String {
    let build_info = formatcp!(
        r#" - branch: {}
 - commit hash: {}
 - rust version: {}
 - rust channel: {}
 - build time: {}"#,
        build::BRANCH,
        build::COMMIT_HASH,
        build::RUST_VERSION,
        build::RUST_CHANNEL,
        build::BUILD_TIME
    );
    let runtime_info = format!(
        "- ARCH: {}
- OS: {}
- distro: {}
- desktop env: {}
- lang: {},
- gtk: {}.{}.{}
- libadwaita: {}.{}.{}",
        whoami::arch(),
        whoami::platform(),
        whoami::distro(),
        whoami::desktop_env(),
        whoami::langs()
            .map(|langs| langs.map(|lang| lang.to_string()).format(", ").to_string())
            .unwrap_or_else(|_| tr!("Unknown")),
        gtk::major_version(),
        gtk::minor_version(),
        gtk::micro_version(),
        adw::major_version(),
        adw::minor_version(),
        adw::micro_version()
    );
    format!(
        "Build info:\n{}\nRuntime info:\n{}",
        build_info, runtime_info
    )
}
