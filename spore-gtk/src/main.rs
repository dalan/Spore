use adw::prelude::*;
use adw::{gio, glib};
use app::App;
use log::{LevelFilter, error};
use simplelog::*;
use std::str::FromStr;
use tr::{tr, tr_init};

mod about_dialog;
pub mod app;
mod main_window;
pub mod metadata;
pub mod preference_dialog;
pub mod preferences;
pub mod thumbnails;
pub mod tree;
pub mod view;

const APP_ID: &str = "fr.dalan.spore-gtk";

fn main() -> glib::ExitCode {
    human_panic::setup_panic!();

    // Init log
    let level_filter = LevelFilter::from_str(
        &std::env::var("SPORE_LOG_LEVEL").unwrap_or_else(|_| "info".to_string()),
    )
    .unwrap_or(LevelFilter::Info);
    simplelog::TermLogger::init(
        level_filter,
        simplelog::ConfigBuilder::new()
            .set_time_offset_to_local()
            .unwrap()
            .set_time_format_custom(format_description!(
                "[hour]:[minute]:[second].[subsecond digits:3]"
            ))
            .build(),
        simplelog::TerminalMode::Stderr,
        simplelog::ColorChoice::Auto,
    )
    .unwrap();

    tr_init!(
        std::env::var("LOCALE_DIR")
            .as_deref()
            .unwrap_or("/usr/share/locale/")
    );

    // Init spore
    if let Err(e) = libspore::init() {
        error!("{}", tr!("Cannot initialize spore ({})", e));
        return glib::ExitCode::FAILURE;
    }

    glib::set_application_name("Spore");

    // Register and include resources
    gio::resources_register_include!("ui.gresource").expect("Failed to register UI resources.");
    gio::resources_register_include!("data.gresource").expect("Failed to register data resources.");

    let app = App::new();
    app.run()
}
