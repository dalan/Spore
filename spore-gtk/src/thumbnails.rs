use glib::Object;
use gtk::{
    Align, Box, EventControllerScroll, EventControllerScrollFlags, Label, ListItem,
    ListScrollFlags, ListView, Orientation, Picture, SignalListItemFactory, SingleSelection, gio,
    glib::{self, clone},
    prelude::*,
};
use libspore::Item;
use log::{debug, error, info};
use std::{cell::RefCell, collections::HashMap, rc::Rc};

use crate::{app::App, view};

mod model {
    use super::*;
    use glib::subclass::types::ObjectSubclassIsExt;

    glib::wrapper! {
        pub struct ThumbnailObject(ObjectSubclass<imp::ThumbnailObject>);
    }

    impl ThumbnailObject {
        pub fn new(index: usize, id: i64) -> Self {
            let obj: ThumbnailObject = Object::builder().build();
            obj.imp().index.set(index);
            obj.imp().id.set(id);
            obj
        }

        pub fn index(&self) -> usize {
            self.imp().index.get()
        }

        pub fn id(&self) -> i64 {
            self.imp().id.get()
        }
    }

    mod imp {
        use super::*;
        use gtk::subclass::prelude::*;
        use std::cell::Cell;

        // Object holding the state
        #[derive(Default)]
        pub struct ThumbnailObject {
            pub index: Cell<usize>,
            pub id: Cell<i64>,
        }

        // The central trait for subclassing a GObject
        #[glib::object_subclass]
        impl ObjectSubclass for ThumbnailObject {
            const NAME: &'static str = "ThumbnailObject";
            type Type = super::ThumbnailObject;
        }

        // Trait shared by all GObjects
        impl ObjectImpl for ThumbnailObject {}
    }
}

pub struct List {
    view: ListView,
    selection_model: SingleSelection,
    nb_item: usize,
    missing_thumbnails: Rc<RefCell<HashMap<i64, Picture>>>,
}

impl Default for List {
    fn default() -> Self {
        debug!("Create thumbnails list view");

        // Get app
        let app = App::default();
        let width = app.get_window_pref().thumbnails_size as i32;

        // Create new model
        let model = gio::ListStore::new::<model::ThumbnailObject>();

        let factory = SignalListItemFactory::new();
        factory.connect_setup(move |_, list_item| {
            let label: Label = Label::builder()
                .ellipsize(gtk::pango::EllipsizeMode::End)
                .xalign(0.5)
                .halign(Align::Fill)
                .build();

            let thumbnail = Picture::builder()
                .vexpand(true)
                .hexpand(true)
                .width_request(width)
                .height_request(width)
                .paintable(&view::image::get_default_paintable())
                .build();

            let list_box = Box::builder()
                .margin_start(10)
                .margin_end(10)
                .margin_top(10)
                .margin_bottom(20)
                .width_request(width)
                .spacing(10)
                .halign(Align::Fill)
                .orientation(Orientation::Vertical)
                .build();
            list_box.append(&thumbnail);
            list_box.append(&label);

            list_item
                .downcast_ref::<ListItem>()
                .expect("Needs to be ListItem")
                .set_child(Some(&list_box));
        });

        let missing_thumbnails = Rc::new(RefCell::new(HashMap::new()));

        factory.connect_bind({
            let missing_thumbnails = Rc::downgrade(&missing_thumbnails);
            move |_, list_item| {
                // Get ListItem
                let list_item = list_item
                    .downcast_ref::<ListItem>()
                    .expect("Needs to be ListItem");

                // Get `IntegerObject` from `ListItem`
                let thumbnail_object = list_item
                    .item()
                    .and_downcast::<model::ThumbnailObject>()
                    .expect("The item has to be an `ThumbnailObject`.");

                // Get `Box` from `ListItem`
                let list_box = list_item
                    .child()
                    .and_downcast::<Box>()
                    .expect("The child has to be a `Box`.");

                // Get `thumbnail`
                let thumbnail = list_box
                    .first_child()
                    .and_downcast::<Picture>()
                    .expect("The child has to be a `Picture`.");

                // Get `Label`
                let label = thumbnail
                    .next_sibling()
                    .and_downcast::<Label>()
                    .expect("The child has to be a `Label`.");

                // Get app
                let app = App::default();

                // Set label and thumbnail
                app.inspect_file(Some(thumbnail_object.index()), |file, _, _, _, _| {
                    label.set_label(file.name());

                    if let Some(img) = app.image_loader().get_thumbnail(file.id()) {
                        thumbnail.set_paintable(Some(&view::image::get_paintable(&img)));
                    } else {
                        if let Some(missing_thumbnails) = missing_thumbnails.upgrade() {
                            missing_thumbnails.borrow_mut().insert(file.id(), thumbnail);
                        }
                        if let Err(e) = app.image_loader().load_thumbnail(file) {
                            error!("Image loader closed ({})", e);
                        }
                    }
                });
            }
        });

        factory.connect_unbind({
            let missing_thumbnails = Rc::downgrade(&missing_thumbnails);
            move |_, list_item| {
                // Get ListItem
                let list_item = list_item
                    .downcast_ref::<ListItem>()
                    .expect("Needs to be ListItem");

                // Get `IntegerObject` from `ListItem`
                let thumbnail_object = list_item
                    .item()
                    .and_downcast::<model::ThumbnailObject>()
                    .expect("The item has to be an `ThumbnailObject`.");

                // Remove missing thumbnail
                if let Some(missing_thumbnails) = missing_thumbnails.upgrade() {
                    missing_thumbnails
                        .borrow_mut()
                        .remove(&thumbnail_object.id());
                }
            }
        });

        let selection_model = SingleSelection::new(Some(model));
        selection_model.set_autoselect(true);

        // Connect selection
        selection_model.connect_selection_changed(|selection_model, _, _| {
            // Get `FolderObject` from `TreeListRow`
            let folder_object = selection_model
                .selected_item()
                .and_downcast::<model::ThumbnailObject>()
                .expect("The item has to be an `ThumbnailObject`.");

            // Get and set current folder
            let index = folder_object.index();
            info!("File {} selected", index);
            let app = crate::app::App::default();
            app.set_file_index(index);
        });

        // Create ListView
        let list_view = ListView::builder()
            .model(&selection_model)
            .factory(&factory)
            .orientation(Orientation::Horizontal)
            .show_separators(true)
            .build();

        // Add scroll controller
        let scroll_controler = EventControllerScroll::new(
            EventControllerScrollFlags::VERTICAL | EventControllerScrollFlags::DISCRETE,
        );
        scroll_controler.connect_scroll(clone!(
            #[weak]
            list_view,
            #[upgrade_or]
            glib::Propagation::Proceed,
            move |_event_controller, _dx, dy| {
                if dy > 0. {
                    let _ = list_view.activate_action("win.next", None);
                } else {
                    let _ = list_view.activate_action("win.previous", None);
                }
                glib::Propagation::Stop
            }
        ));
        list_view.add_controller(scroll_controler);

        List {
            view: list_view,
            selection_model,
            nb_item: 0,
            missing_thumbnails,
        }
    }
}

impl List {
    pub fn view(&self) -> &ListView {
        &self.view
    }

    pub fn update_model(&mut self) {
        debug!("Update thumbnails list view");

        // Empty missing item
        self.missing_thumbnails.borrow_mut().clear();

        // Create new model
        let model = gio::ListStore::new::<model::ThumbnailObject>();
        App::default().inspect_files(|file, index| {
            model.append(&model::ThumbnailObject::new(index, file.id()));
        });

        // Set model
        self.selection_model.set_model(Some(&model));

        // Save nb item
        self.nb_item = model.n_items() as usize;
    }

    /// Scroll to current image
    pub fn scroll_to_current(&self) {
        if self.nb_item > 0 {
            let file_index = App::default().get_indexes().2.min(self.nb_item);
            self.view.scroll_to(
                file_index as u32,
                ListScrollFlags::FOCUS | ListScrollFlags::SELECT,
                None,
            );
        }
    }

    /// Scroll to a a given position
    pub fn scroll_to(&self, index: usize) {
        if self.nb_item > 0 {
            self.view.scroll_to(
                index as u32,
                ListScrollFlags::FOCUS | ListScrollFlags::SELECT,
                None,
            );
        }
    }

    /// Scroll
    pub fn scroll(&self, value: i32) {
        let mut new_pos = self.selection_model.selected() as i32 + value;
        if new_pos < 0 {
            new_pos = 0;
        }
        if new_pos >= self.nb_item as i32 {
            new_pos = self.nb_item as i32 - 1;
        }
        if new_pos != self.selection_model.selected() as i32 {
            self.view.scroll_to(
                new_pos as u32,
                ListScrollFlags::FOCUS | ListScrollFlags::SELECT,
                None,
            );
        }
    }

    pub fn add_thumbnail(&mut self, id: i64) {
        if let Some(thumbnail) = self.missing_thumbnails.borrow_mut().remove(&id) {
            if let Some(img) = App::default().image_loader().get_thumbnail(id) {
                thumbnail.set_paintable(Some(&view::image::get_paintable(&img)));
            }
        }
    }
}
