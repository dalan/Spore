use anyhow::Result;
use shadow_rs::{BuildPattern, ShadowBuilder};

fn main() -> Result<()> {
    // Windows icon
    if std::env::var("CARGO_CFG_TARGET_OS").unwrap() == "windows" {
        let mut res = winresource::WindowsResource::new();
        res.set_icon("../mushrooms.ico");
        res.compile().unwrap();
    }

    glib_build_tools::compile_resources(&["ui"], "ui/resources.gresource.xml", "ui.gresource");
    glib_build_tools::compile_resources(
        &["data"],
        "data/resources.gresource.xml",
        "data.gresource",
    );

    ShadowBuilder::builder()
        .build_pattern(BuildPattern::Lazy)
        .build()
        .unwrap();

    Ok(())
}
