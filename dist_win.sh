#!/bin/bash
mkdir dist/win_dll
wldd -s target/release/spore-cli.exe | rg -v "not found" | cut -d ' ' -f 11 | sed "s/\\\\/\//g" | xargs -I {}  cp `cygpath "{}"`  dist/win_dll
echo '[profiles]
[profiles.dep_win]
directory = "dist/win_dll/"
files = [' > dist/win_smc.toml
wldd -s target/release/spore-cli.exe | rg -v "not found" | cut -d ' ' -f 9 | sd '\s*(.*)\n' '"$1", ' | cut -d ',' -f 1- >> dist/win_smc.toml
echo ']' >> dist/win_smc.toml
cat dist/win_smc.toml | smc bin_win dep_win files
rm -R dist/win_dll
rm dist/win_smc.toml
