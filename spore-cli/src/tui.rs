mod search;
mod treeview;
mod utils;

use anyhow::Result;
use arboard::Clipboard;
use itertools::Itertools;
use libspore::Database;
use ratatui::crossterm::{
    event::{self, Event, KeyCode, KeyEventKind, KeyModifiers},
    execute,
    terminal::{EnterAlternateScreen, LeaveAlternateScreen, disable_raw_mode, enable_raw_mode},
};
use ratatui::{
    Frame, Terminal,
    backend::CrosstermBackend,
    layout::{Alignment, Constraint, Direction, Layout, Rect},
    style::{Color, Style},
    text::{Line, Span},
    widgets::{Block, Borders, ListState, Paragraph, Tabs},
};
use search::SearchView;
use std::io::{self};
use treeview::TreeView;
use tui_tree_widget::TreeState;

trait View {
    fn help(&self) -> &'static [(&'static str, &'static str)];
    fn on_page_up(&mut self) {}
    fn on_page_down(&mut self) {}
    fn on_up(&mut self) {}
    fn on_down(&mut self) {}
    fn on_left(&mut self) {}
    fn on_right(&mut self) {}
    fn on_begin(&mut self) {}
    fn on_end(&mut self) {}
    fn on_enter(&mut self) {}
    fn on_char(&mut self, _c: char) {}
    fn on_delete(&mut self, _left: bool) {}
    fn on_tab(&mut self, _back: bool) {}
    fn on_copy(&mut self) -> Option<String> {
        None
    }
    fn on_open(&mut self) {}
    fn on_toggle_select(&mut self) {}
    fn on_toggle_res(&mut self) {}
    fn draw(&mut self, _f: &mut Frame, _rect: Rect) {}
}

#[derive(Copy, Clone, Debug)]
enum ViewType {
    Treeview,
    Search,
}

impl ViewType {
    fn previous(&mut self) {
        *self = match self {
            ViewType::Treeview => ViewType::Search,
            ViewType::Search => ViewType::Treeview,
        };
    }

    fn next(&mut self) {
        *self = match self {
            ViewType::Treeview => ViewType::Search,
            ViewType::Search => ViewType::Treeview,
        };
    }
}

struct App<'a> {
    _db: &'a Database,
    clipboard: Clipboard,
    treeview: TreeView<'a>,
    search_view: SearchView<'a>,
    current_view: ViewType,
}

impl<'a> App<'a> {
    fn new(db: &'a Database) -> Result<Self> {
        let mut collection_state = ListState::default();
        collection_state.select(Some(0));

        let mut tree_state = TreeState::default();
        tree_state.select(vec![0]);

        Ok(Self {
            _db: db,
            clipboard: Clipboard::new()?,
            treeview: TreeView::new(db),
            search_view: SearchView::new(db),
            current_view: ViewType::Treeview,
        })
    }

    fn current_view(&mut self) -> &mut dyn View {
        match self.current_view {
            ViewType::Treeview => &mut self.treeview,
            ViewType::Search => &mut self.search_view,
        }
    }

    fn init(&mut self) {
        self.treeview.generate_tree();
        self.search_view.update_files();
    }

    fn on_page_up(&mut self) {
        self.current_view().on_page_up();
    }

    fn on_page_down(&mut self) {
        self.current_view().on_page_down();
    }

    fn on_up(&mut self) {
        self.current_view().on_up();
    }

    fn on_down(&mut self) {
        self.current_view().on_down();
    }

    fn on_left(&mut self) {
        self.current_view().on_left();
    }

    fn on_right(&mut self) {
        self.current_view().on_right();
    }

    fn on_begin(&mut self) {
        self.current_view().on_begin();
    }

    fn on_end(&mut self) {
        self.current_view().on_end();
    }

    fn on_enter(&mut self) {
        self.current_view().on_enter();
    }

    fn on_char(&mut self, c: char) {
        self.current_view().on_char(c);
    }

    fn on_delete(&mut self, left: bool) {
        self.current_view().on_delete(left);
    }

    fn on_tab(&mut self, back: bool) {
        self.current_view().on_tab(back);
    }

    fn on_copy(&mut self) {
        if let Some(text) = self.current_view().on_copy() {
            self.clipboard.set_text(text).unwrap();
        }
    }

    fn on_open(&mut self) {
        self.current_view().on_open();
    }

    fn on_toggle_select(&mut self) {
        self.current_view().on_toggle_select();
    }

    fn on_toggle_res(&mut self) {
        self.current_view().on_toggle_res();
    }

    fn on_next_view(&mut self) {
        self.current_view.next();
    }

    fn on_previous_view(&mut self) {
        self.current_view.previous();
    }

    fn draw(&mut self, f: &mut Frame) {
        let chunks = Layout::default()
            .direction(Direction::Vertical)
            .constraints(
                [
                    Constraint::Min(0),
                    Constraint::Length(3),
                    Constraint::Length(1),
                ]
                .as_ref(),
            )
            .split(f.area());

        // Tabs
        let tabs = Tabs::new(["Navigator", "Search"])
            .block(Block::default().title("Tabs").borders(Borders::ALL))
            .style(Style::default().fg(Color::White))
            .highlight_style(Style::default().fg(Color::Yellow))
            .select(self.current_view as usize);
        f.render_widget(tabs, chunks[1]);

        // Help
        let common_helps = &[
            ("^q", "Quit"),
            ("^c", "Copy"),
            ("^o", "Open"),
            ("^h", "Change resolution"),
            ("^s", "Select"),
            ("^←→", "Change tab"),
        ];
        let help_text = Line::from(
            Itertools::intersperse(
                common_helps
                    .iter()
                    .chain(self.current_view().help())
                    .map(|(key, help)| {
                        Span::styled(
                            format!("{} [{}]", help, key),
                            Style::default().bg(Color::Blue),
                        )
                    }),
                Span::from(" "),
            )
            .collect::<Vec<Span>>(),
        );
        f.render_widget(
            Paragraph::new(help_text).alignment(Alignment::Left),
            chunks[2],
        );

        self.current_view().draw(f, chunks[0]);
    }
}

pub fn run(db: Database) -> Result<()> {
    // Setup terminal
    enable_raw_mode()?;
    let mut stdout = io::stdout();
    execute!(stdout, EnterAlternateScreen)?;
    let backend = CrosstermBackend::new(stdout);
    let mut terminal = Terminal::new(backend)?;

    let mut app = App::new(&db)?;
    app.init();

    loop {
        terminal.draw(|f| app.draw(f))?;
        if let Event::Key(key) = event::read()? {
            if key.kind == KeyEventKind::Press {
                match key.modifiers {
                    KeyModifiers::CONTROL => match key.code {
                        KeyCode::Char('c') => app.on_copy(),
                        KeyCode::Char('o') => app.on_open(),
                        KeyCode::Char('h') => app.on_toggle_res(),
                        KeyCode::Char('s') => app.on_toggle_select(),
                        KeyCode::Char('q') => break,
                        KeyCode::Left => app.on_previous_view(),
                        KeyCode::Right => app.on_next_view(),
                        _ => (),
                    },
                    _ => match key.code {
                        KeyCode::PageUp => app.on_page_up(),
                        KeyCode::PageDown => app.on_page_down(),
                        KeyCode::Up => app.on_up(),
                        KeyCode::Down => app.on_down(),
                        KeyCode::Left => app.on_left(),
                        KeyCode::Right => app.on_right(),
                        KeyCode::Home => app.on_begin(),
                        KeyCode::End => app.on_end(),
                        KeyCode::Enter => app.on_enter(),
                        KeyCode::Delete => app.on_delete(false),
                        KeyCode::Backspace => app.on_delete(true),
                        KeyCode::Tab => app.on_tab(false),
                        KeyCode::BackTab => app.on_tab(true),
                        KeyCode::Esc => break,
                        KeyCode::Char(c) => app.on_char(c),
                        _ => {}
                    },
                }
            }
        }
    }

    // restore terminal
    disable_raw_mode()?;
    execute!(terminal.backend_mut(), LeaveAlternateScreen)?;
    terminal.show_cursor()?;

    Ok(())
}
