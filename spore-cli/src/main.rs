use std::sync::Mutex;

use anyhow::Result;
use camino::Utf8PathBuf;
use clap::{Parser, Subcommand};
use indicatif::{MultiProgress, ProgressBar, ProgressStyle};
use libspore::Database;
use log::{error, info};
use shadow_rs::{formatcp, shadow};
use simplelog::*;

shadow!(build);

const VERSION_INFO: &str = formatcp!(
    r#"{}
branch: {}
commit_hash: {}
build_env: {},{}"#,
    build::PKG_VERSION,
    build::BRANCH,
    build::COMMIT_HASH,
    build::RUST_VERSION,
    build::RUST_CHANNEL
);

mod tui;

#[derive(Parser)]
#[clap(author, version = VERSION_INFO, about, long_about = None)]
#[command(propagate_version = true)]
struct Cli {
    #[clap(subcommand)]
    command: Commands,
    /// Turn debugging information on
    #[arg(short, long, action = clap::ArgAction::Count)]
    verbose: u8,
}

#[derive(Subcommand)]
enum Commands {
    /// Initialize the database
    Init {
        /// The database name
        #[clap(short, long, action)]
        name: String,
    },
    /// List the collections
    List,
    /// Add a collection
    Add {
        /// The collection name
        #[clap(short, long, action)]
        name: String,
        /// The collection path
        #[clap(short, long, action)]
        path: Utf8PathBuf,
    },
    /// Update a collection
    Update {
        /// The collection name
        #[clap(short, long, action)]
        name: String,
        /// A subfolder path
        #[clap(short, long, action)]
        path: Option<Utf8PathBuf>,
    },
    /// Generate thumbnails for a collection
    Thumbnails {
        /// The collection name
        #[clap(short, long, action)]
        name: String,
    },
    /// Update metadata for a collection
    Metadata {
        /// The collection name
        #[clap(short, long, action)]
        name: String,
        /// A subfolder path
        #[clap(short, long, action)]
        path: Option<Utf8PathBuf>,
    },
    /// Remove a collection
    Remove {
        /// The collection name
        #[clap(short, long, action)]
        name: String,
    },
    /// Open a database
    Open,
}

fn main() -> Result<()> {
    human_panic::setup_panic!();

    let cli = Cli::parse();

    let multi_prog = MultiProgress::new();

    let level_filter = match cli.verbose {
        0 => LevelFilter::Info,
        1 => LevelFilter::Debug,
        _ => LevelFilter::Trace,
    };
    let logger = TermLogger::new(
        level_filter,
        ConfigBuilder::new()
            .set_time_offset_to_local()
            .unwrap()
            .build(),
        TerminalMode::Stderr,
        ColorChoice::Auto,
    );
    indicatif_log_bridge::LogWrapper::new(multi_prog.clone(), logger)
        .try_init()
        .unwrap();

    libspore::init()?;

    match &cli.command {
        Commands::Init { name } => {
            Database::new_default(name)?;
        }
        Commands::List => {
            let db = Database::open_default()?;
            info!("Collection list:");
            for collection in db.collections() {
                info!(
                    " - {} in `{}`",
                    collection.name(),
                    collection.folder().path()
                );
            }
        }
        Commands::Add { name, path } => {
            let mut db = Database::open_default()?;
            db.add_collection(name, path)?;
        }
        Commands::Update { name, path } => {
            let mut db = Database::open_default()?;
            if let Some(collection_index) = db.collections().iter().position(|c| c.name() == name) {
                update_collection(&mut db, path, collection_index, multi_prog)?;
            } else {
                error!("Invalid collection name");
            }
        }
        Commands::Thumbnails { name } => {
            let mut db = Database::open_default()?;
            if let Some(collection_index) = db.collections().iter().position(|c| c.name() == name) {
                generate_thumbnails(&mut db, collection_index, multi_prog)?;
            } else {
                error!("Invalid collection name");
            }
        }
        Commands::Metadata { name, path } => {
            let mut db = Database::open_default()?;
            if let Some(collection_index) = db.collections().iter().position(|c| c.name() == name) {
                update_metadatas(&mut db, path, collection_index, multi_prog)?;
            } else {
                error!("Invalid collection name");
            }
        }
        Commands::Remove { name } => {
            let mut db = Database::open_default()?;
            if let Some(collection_index) = db.collections().iter().position(|c| c.name() == name) {
                db.remove_collection(collection_index)?;
            } else {
                error!("Invalid collection name");
            }
        }
        Commands::Open => {
            let db = Database::open_default()?;
            if db.collections().is_empty() {
                info!("No collections");
            } else {
                tui::run(db)?;
            }
        }
    }

    Ok(())
}

fn generate_thumbnails(
    db: &mut Database,
    collection_index: usize,
    multi_prog: MultiProgress,
) -> Result<()> {
    let mut progress_bar = None;
    db.generate_thumbnail(
        collection_index,
        Mutex::new(|state| match state {
            libspore::database::SimpleState::Init { size } => {
                progress_bar = Some(multi_prog.add(create_len_progress_bar(size)))
            }
            libspore::database::SimpleState::Progress => progress_bar.as_mut().unwrap().inc(1),
            libspore::database::SimpleState::End => progress_bar.take().unwrap().finish_and_clear(),
        }),
    )?;
    Ok(())
}

fn update_metadatas(
    db: &mut Database,
    subfolder: &Option<Utf8PathBuf>,
    collection_index: usize,
    multi_prog: MultiProgress,
) -> Result<()> {
    let mut progress_bar = None;
    db.update_metadatas(
        collection_index,
        subfolder.as_deref(),
        |state| match state {
            libspore::database::SimpleState::Init { size } => {
                progress_bar = Some(multi_prog.add(create_len_progress_bar(size)))
            }
            libspore::database::SimpleState::Progress => progress_bar.as_mut().unwrap().inc(1),
            libspore::database::SimpleState::End => progress_bar.take().unwrap().finish_and_clear(),
        },
    )?;
    Ok(())
}

fn update_collection(
    db: &mut Database,
    subfolder: &Option<Utf8PathBuf>,
    collection_index: usize,
    multi_prog: MultiProgress,
) -> Result<()> {
    let mut progress_bar = None;
    db.update(
        collection_index,
        subfolder.as_deref(),
        |state| match state {
            libspore::database::UpdateState::Init { part, size } => match part {
                libspore::database::UpdatePart::Get => {
                    progress_bar = Some(multi_prog.add(create_spinner_progress_bar()))
                }
                libspore::database::UpdatePart::DeleteFiles => {
                    progress_bar = Some(multi_prog.add(create_len_progress_bar(size)))
                }
                libspore::database::UpdatePart::DeleteFolders => {
                    progress_bar = Some(multi_prog.add(create_spinner_progress_bar()))
                }
                libspore::database::UpdatePart::Add => {
                    progress_bar = Some(multi_prog.add(create_len_progress_bar(size)))
                }
                libspore::database::UpdatePart::DbSave => {
                    progress_bar = Some(multi_prog.add(create_spinner_progress_bar()))
                }
            },
            libspore::database::UpdateState::Progress => progress_bar.as_mut().unwrap().inc(1),
            libspore::database::UpdateState::End => {
                if let Some(progress_bar) = progress_bar.take() {
                    progress_bar.finish_and_clear();
                    multi_prog.remove(&progress_bar);
                }
            }
        },
    )?;
    Ok(())
}

fn create_spinner_progress_bar() -> ProgressBar {
    ProgressBar::new(0).with_style(
        ProgressStyle::with_template(
            "{spinner} Elapsed {elapsed_precise:10} Files found {pos:10} Speed {per_sec:10}",
        )
        .unwrap(),
    )
}

fn create_len_progress_bar(size: usize) -> ProgressBar {
    ProgressBar::new(size as u64).with_style(
        ProgressStyle::with_template(
            "Elapsed {elapsed_precise} {wide_bar}  {pos:>7}/{len:7} Remaining {eta_precise:12} Speed {per_sec:10}",
        )
        .unwrap(),
    )
}

#[test]
fn verify_cli() {
    use clap::CommandFactory;
    Cli::command().debug_assert()
}
