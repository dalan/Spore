use std::{borrow::Cow, str::FromStr};

use anyhow::Result;
use either::Either;
use libspore::{Collection, File, Folder, resize};
use ratatui::{
    layout::{Constraint, Rect},
    prelude::Buffer,
    style::{Color, Modifier, Style},
    widgets::{Block, Borders, Cell, List, ListItem, Paragraph, Row, Table, Widget},
};

pub struct PreviewWidget<'a> {
    file: &'a File,
    high_res: bool,
}

impl<'a> PreviewWidget<'a> {
    pub fn new(file: &'a File, high_res: bool) -> Self {
        Self { file, high_res }
    }

    fn render_image(&self, rect: Rect) -> Result<(u16, u16, u16, u16)> {
        let (res_width, res_height) =
            if let Ok(window_size) = ratatui::crossterm::terminal::window_size() {
                if (window_size.width != 0) && (window_size.height != 0) {
                    (
                        window_size.width / window_size.columns,
                        window_size.height / window_size.rows,
                    )
                } else {
                    (10, 22)
                }
            } else {
                (10, 22)
            };

        let img = if self.high_res {
            if let Some(img) = self.file.image()? {
                resize(
                    &img,
                    (res_width * rect.width) as u32,
                    (res_height * rect.height) as u32,
                )?
                .into()
            } else {
                self.file.thumbnail_image()?
            }
        } else {
            self.file.thumbnail_image()?
        };

        let img_ratio = img.width() as f64 / img.height() as f64;
        let rect_ratio = (rect.width * res_width) as f64 / (rect.height * res_height) as f64;
        let (width, height) = if img_ratio > rect_ratio {
            (Some(rect.width as u32), None)
        } else {
            (None, Some(rect.height as u32))
        };
        let real_width = width.unwrap_or_else(|| {
            ((height.unwrap() as f64 * img_ratio * res_height as f64) / res_width as f64) as u32
        });
        let conf = viuer::Config {
            // set offset
            x: rect.x + (rect.width - real_width as u16) / 2,
            y: rect.y as i16,
            // set dimensions
            width,
            height,
            ..Default::default()
        };
        let (w, h) = viuer::print(&img, &conf)?;
        Ok((conf.x, conf.y as u16, w as u16, h as u16))
    }
}

impl<'a> Widget for PreviewWidget<'a> {
    fn render(self, area: Rect, buf: &mut Buffer) {
        let preview_block = Block::default().title("Preview").borders(Borders::ALL);
        let inner_area = preview_block.inner(area);
        preview_block.render(area, buf);
        if self.file.thumbnail().is_some() {
            match self.render_image(inner_area) {
                Err(e) => {
                    let err_paragraph = Paragraph::new(format!("Cannot create preview: {}", e));
                    err_paragraph.render(inner_area, buf);
                }
                Ok((begin_x, begin_y, width, height)) => {
                    for x in inner_area.left()..inner_area.right() {
                        for y in inner_area.top()..inner_area.bottom() {
                            if (x >= begin_x)
                                && (x < begin_x + width)
                                && (y >= begin_y)
                                && (y < begin_y + height)
                            {
                                buf.cell_mut((x, y)).unwrap().set_skip(true);
                            } else {
                                buf.cell_mut((x, y)).unwrap().set_char(' ').set_skip(false);
                            }
                        }
                    }
                }
            }
        } else {
            let no_thumbnail_paragraph = Paragraph::new("No thumbnail");
            no_thumbnail_paragraph.render(inner_area, buf);
        }
    }
}

pub fn create_panel<'a>(current_item: Either<&'a File, &'a Folder>) -> Table<'a> {
    let (name, rows) = match current_item {
        Either::Left(file) => {
            let mut rows = vec![
                Row::new(vec!["Path", file.filename().as_str()]),
                Row::new(vec![
                    Cow::Borrowed("Time"),
                    Cow::Owned(format!("{}", file.time().format("%A %d/%m/%Y %T"))),
                ]),
                Row::new(vec![
                    Cow::Borrowed("Type"),
                    Cow::Owned(file.typ().to_string()),
                ]),
            ];

            // Add Location
            if let Some(location) = file.location() {
                rows.push(Row::new(vec![
                    Cow::Borrowed("Location"),
                    Cow::Owned(location.to_string()),
                ]));
            }

            // Add resolution
            if let Some(resolution) = file.resolution() {
                rows.push(Row::new(vec![
                    Cow::Borrowed("Resolution"),
                    Cow::Owned(resolution.to_string()),
                ]));
            }

            // Add Duration
            if let Some(duration) = file.duration() {
                rows.push(Row::new(vec![
                    Cow::Borrowed("Duration"),
                    Cow::Owned(humantime::format_duration(*duration).to_string()),
                ]));
            }

            (format!("File - {}", file.name()), rows)
        }
        Either::Right(folder) => (
            format!("Folder - {}", folder.name()),
            vec![Row::new(vec!["Path", folder.path().as_str()])],
        ),
    };
    Table::new(rows, [Constraint::Length(20), Constraint::Percentage(100)])
        .header(Row::new(vec!["Description", "Value"]).style(Style::default().fg(Color::Yellow)))
        .block(Block::default().title(name).borders(Borders::ALL))
}

pub fn create_collection_list<'a>(
    collections: &'a [Collection],
    collection_block: Block<'a>,
) -> List<'a> {
    let collection_list: Vec<ListItem> = collections
        .iter()
        .map(|c| ListItem::new(c.name()))
        .collect();
    List::new(collection_list)
        .highlight_style(
            Style::default()
                .add_modifier(Modifier::BOLD)
                .fg(Color::Black)
                .bg(Color::LightGreen),
        )
        .highlight_symbol(">> ")
        .block(collection_block)
}

pub fn create_cell<FS: FromStr>(value: &str) -> Cell<'_> {
    let mut cell = Cell::from(value);
    if value.parse::<FS>().is_err() {
        cell = cell.style(Style::default().fg(Color::Red));
    }
    cell
}
