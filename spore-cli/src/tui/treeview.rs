use std::collections::HashSet;

use super::utils::*;
use either::Either;
use itertools::Itertools;
use libspore::{Database, File, Folder};
use log::{debug, warn};
use ratatui::{
    Frame,
    layout::{Constraint, Direction, Layout, Rect},
    style::{Color, Modifier, Style},
    text::Text,
    widgets::{Block, Borders, ListState},
};
use tui_tree_widget::{Tree, TreeItem, TreeState};

pub struct TreeView<'a> {
    db: &'a Database,
    collection_state: ListState,
    tree_state: TreeState<usize>,
    tree_items: Vec<TreeItem<'a, usize>>,
    selected_items: HashSet<Vec<usize>>,
    high_res: bool,
}

impl<'a> TreeView<'a> {
    pub fn new(db: &'a Database) -> Self {
        let mut collection_state = ListState::default();
        collection_state.select(Some(0));

        let mut tree_state = TreeState::default();
        tree_state.select(vec![0]);

        Self {
            db,
            collection_state,
            tree_state,
            tree_items: Vec::new(),
            selected_items: HashSet::new(),
            high_res: false,
        }
    }

    pub fn generate_tree(&mut self) {
        let current_pos = Vec::new();
        let collection = &self.db.collections()[self.collection_state.selected().unwrap()];
        self.tree_items.clear();
        Self::add_folder_in_tree(
            collection.folder(),
            &mut self.tree_items,
            &self.selected_items,
            current_pos,
        );
    }

    fn add_folder_in_tree<'b>(
        main_folder: &'b Folder,
        main_tree_items: &mut Vec<TreeItem<'b, usize>>,
        selected_items: &HashSet<Vec<usize>>,
        mut current_pos: Vec<usize>,
    ) {
        current_pos.push(0);
        for (index, folder) in main_folder.folders().iter().enumerate() {
            let mut tree_items = Vec::new();
            Self::add_folder_in_tree(folder, &mut tree_items, selected_items, current_pos.clone());
            main_tree_items
                .push(TreeItem::new(index, folder.name().to_string(), tree_items).unwrap());
            *current_pos.last_mut().unwrap() += 1;
        }
        let nb_folder = main_folder.folders().len();
        for (index, file) in main_folder.files().iter().enumerate() {
            let style = if selected_items.contains(&current_pos) {
                ratatui::style::Style::default().bg(Color::Red)
            } else {
                ratatui::style::Style::default()
            };
            let text = Text::styled(file.name(), style);
            let item = TreeItem::new_leaf(index + nb_folder, text);

            main_tree_items.push(item);
            *current_pos.last_mut().unwrap() += 1;
        }
    }

    fn selected_to_item(&self, selected: &[usize]) -> Either<&File, &Folder> {
        selected.iter().fold(
            Either::Right(
                self.db.collections()[self.collection_state.selected().unwrap()].folder(),
            ),
            |either, index| {
                let current_folder = either.unwrap_right();
                let nb_folders = current_folder.folders().len();
                if *index >= nb_folders {
                    Either::Left(&current_folder.files()[index - nb_folders])
                } else {
                    Either::Right(&current_folder.folders()[*index])
                }
            },
        )
    }

    fn current_item(&self) -> Either<&File, &Folder> {
        self.selected_to_item(&self.tree_state.selected())
    }
}

impl<'a> super::View for TreeView<'a> {
    fn help(&self) -> &'static [(&'static str, &'static str)] {
        &[("↟↡", "Change collection"), ("←↑→↓⏎", "Nav")]
    }

    fn on_page_up(&mut self) {
        let current = self.collection_state.selected().unwrap();
        if current == 0 {
            self.collection_state
                .select(Some(self.db.collections().len() - 1))
        } else {
            self.collection_state.select(Some(current - 1))
        }
        self.generate_tree();
        self.tree_state.select(vec![0]);
    }

    fn on_page_down(&mut self) {
        let current = self.collection_state.selected().unwrap();
        if current == self.db.collections().len() - 1 {
            self.collection_state.select(Some(0))
        } else {
            self.collection_state.select(Some(current + 1))
        }
        self.generate_tree();
        self.tree_state.select(vec![0]);
        self.tree_state.close_all();
    }

    fn on_up(&mut self) {
        self.tree_state.key_up();
    }

    fn on_down(&mut self) {
        self.tree_state.key_down();
    }

    fn on_left(&mut self) {
        self.tree_state.key_left();
    }

    fn on_right(&mut self) {
        self.tree_state.key_right();
    }

    fn on_begin(&mut self) {
        self.tree_state.select_first();
    }

    fn on_end(&mut self) {
        self.tree_state.select_last();
    }

    fn on_enter(&mut self) {
        self.tree_state.toggle_selected();
    }

    fn on_copy(&mut self) -> Option<String> {
        Some(if self.selected_items.is_empty() {
            match self.current_item() {
                Either::Left(file) => file.filename(),
                Either::Right(folder) => folder.path(),
            }
            .to_string()
        } else {
            self.selected_items
                .iter()
                .map(|selected| self.selected_to_item(selected))
                .filter_map(|item| item.left().map(|file| file.filename()))
                .join("\n")
        })
    }

    fn on_open(&mut self) {
        let path = match self.current_item() {
            Either::Left(file) => file.filename(),
            Either::Right(folder) => folder.path(),
        };
        if let Err(e) = opener::open(path) {
            warn!("Cannot open `{}`: {}", path, e);
        }
    }

    fn on_toggle_select(&mut self) {
        let current_item = self.tree_state.selected();
        debug!("Select toggle item {:?}", current_item);
        if self.selected_items.contains(current_item) {
            self.selected_items.remove(current_item);
        } else {
            self.selected_items.insert(current_item.into());
        }
        self.generate_tree();
    }

    fn on_toggle_res(&mut self) {
        self.high_res = !self.high_res;
    }

    fn draw(&mut self, f: &mut Frame, rect: Rect) {
        let main_rect = Layout::default()
            .direction(Direction::Horizontal)
            .constraints([Constraint::Percentage(30), Constraint::Percentage(70)].as_ref())
            .split(rect);

        let left_rect = Layout::default()
            .direction(Direction::Vertical)
            .constraints([Constraint::Length(8), Constraint::Percentage(100)].as_ref())
            .split(main_rect[0]);

        let right_rect = Layout::default()
            .direction(Direction::Vertical)
            .constraints([Constraint::Min(1), Constraint::Length(9)].as_ref())
            .split(main_rect[1]);

        // Collection list
        let collection_list = create_collection_list(
            self.db.collections(),
            Block::default().title("Collections").borders(Borders::ALL),
        );
        f.render_stateful_widget(collection_list, left_rect[0], &mut self.collection_state);

        // Tree
        let tree_block = Block::default().title("Tree").borders(Borders::ALL);
        let tree = Tree::new(&self.tree_items)
            .unwrap()
            .block(tree_block)
            .highlight_style(
                Style::default()
                    .fg(Color::Black)
                    .bg(Color::LightGreen)
                    .add_modifier(Modifier::BOLD),
            )
            .highlight_symbol(">> ");
        f.render_stateful_widget(tree, left_rect[1], &mut self.tree_state);

        // Panel
        let panel_widget = create_panel(self.current_item());
        f.render_widget(panel_widget, right_rect[1]);

        // Image preview
        if let Either::Left(file) = self.current_item() {
            f.render_widget(PreviewWidget::new(file, self.high_res), right_rect[0]);
        }
    }
}
