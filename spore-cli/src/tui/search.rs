// Tests filter

use std::{cell::RefCell, collections::HashSet};

use either::Either;
use itertools::Itertools;
use libspore::{Database, File, file::Type, folder::FileFilterOption};
use log::{debug, warn};
use ratatui::{
    layout::{Constraint, Direction, Layout, Rect},
    style::{Color, Modifier, Style},
    widgets::{
        Block, Borders, Cell, List, ListItem, ListState, Paragraph, Row, Scrollbar, ScrollbarState,
        Table, TableState,
    },
};
use tui_input::{Input, InputRequest};

use super::utils::{PreviewWidget, create_cell, create_collection_list, create_panel};

const FILTER_NUMBER: usize = 11;

struct Filter {
    year: Input,
    month: Input,
    day: Input,
    country: Input,
    region: Input,
    department: Input,
    city: Input,
    filename: Input,
    typ: Input,
    folder: Input,
    min_res: Input,
    state: RefCell<TableState>,
}

impl Filter {
    fn new() -> Self {
        let mut state = TableState::default();
        state.select(Some(0));

        Self {
            year: Input::default(),
            month: Input::default(),
            day: Input::default(),
            country: Input::default(),
            region: Input::default(),
            department: Input::default(),
            city: Input::default(),
            filename: Input::default(),
            typ: Input::default(),
            folder: Input::default(),
            min_res: Input::default(),
            state: RefCell::new(state),
        }
    }

    fn current_text(&self) -> &Input {
        match self.state.borrow().selected().unwrap() {
            0 => &self.year,
            1 => &self.month,
            2 => &self.day,
            3 => &self.country,
            4 => &self.region,
            5 => &self.department,
            6 => &self.city,
            7 => &self.filename,
            8 => &self.typ,
            9 => &self.folder,
            10 => &self.min_res,
            _ => panic!("No such filter"),
        }
    }

    fn current_text_mut(&mut self) -> &mut Input {
        match self.state.borrow().selected().unwrap() {
            0 => &mut self.year,
            1 => &mut self.month,
            2 => &mut self.day,
            3 => &mut self.country,
            4 => &mut self.region,
            5 => &mut self.department,
            6 => &mut self.city,
            7 => &mut self.filename,
            8 => &mut self.typ,
            9 => &mut self.folder,
            10 => &mut self.min_res,
            _ => panic!("No such filter"),
        }
    }

    fn to_filter_option(&self) -> FileFilterOption {
        let mut option = FileFilterOption::default();
        if let Ok(year) = self.year.value().parse() {
            option.with_year(year);
        }
        if let Ok(month) = self.month.value().parse() {
            option.with_month(month);
        }
        if let Ok(day) = self.day.value().parse() {
            option.with_day(day);
        }
        option.with_country(self.country.value());
        option.with_region(self.region.value());
        option.with_department(self.department.value());
        option.with_city(self.city.value());
        option.with_filename(self.filename.value());
        if let Ok(typ) = self.typ.value().parse() {
            option.with_typ(typ);
        }
        option.with_folder(self.folder.value());
        if let Ok(min_res) = self.min_res.value().parse() {
            option.with_min_res(min_res);
        }
        option
    }

    fn create_table(&self, selected_block: SelectedBlock) -> Table {
        Table::new(
            vec![
                Row::new(vec![
                    Cell::from("Year"),
                    create_cell::<i32>(self.year.value()),
                ]),
                Row::new(vec![
                    Cell::from("Month"),
                    create_cell::<u32>(self.month.value()),
                ]),
                Row::new(vec![
                    Cell::from("Day"),
                    create_cell::<u32>(self.day.value()),
                ]),
                Row::new(vec!["Country", self.country.value()]),
                Row::new(vec!["Region", self.region.value()]),
                Row::new(vec!["Department", self.department.value()]),
                Row::new(vec!["City", self.city.value()]),
                Row::new(vec!["Filename", self.filename.value()]),
                Row::new(vec![
                    Cell::from("Type"),
                    create_cell::<Type>(self.typ.value()),
                ]),
                Row::new(vec!["Folder", self.folder.value()]),
                Row::new(vec![
                    Cell::from("Min res (MP)"),
                    create_cell::<f32>(self.min_res.value()),
                ]),
            ],
            [Constraint::Length(12), Constraint::Percentage(100)],
        )
        .header(
            Row::new(vec!["Description", "Value"])
                .style(Style::default().bg(Color::LightBlue).fg(Color::Black)),
        )
        .column_spacing(1)
        .block(selected_block.create_block(SelectedBlock::Filters))
        .row_highlight_style(Style::default().add_modifier(Modifier::BOLD))
        .highlight_symbol(">>")
    }
}

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
enum SelectedBlock {
    Collections,
    Filters,
    Photos,
}

impl SelectedBlock {
    fn get_title(self) -> &'static str {
        match self {
            SelectedBlock::Collections => "Collections",
            SelectedBlock::Filters => "Filters",
            SelectedBlock::Photos => "Photos",
        }
    }

    fn create_block(self, current_block: SelectedBlock) -> Block<'static> {
        if self == current_block {
            Block::default()
                .title(current_block.get_title())
                .borders(Borders::ALL)
                .border_style(Style::default().fg(Color::Black).bg(Color::Yellow))
        } else {
            Block::default()
                .title(current_block.get_title())
                .borders(Borders::ALL)
        }
    }

    fn next(&mut self) {
        *self = match self {
            SelectedBlock::Collections => SelectedBlock::Filters,
            SelectedBlock::Filters => SelectedBlock::Photos,
            SelectedBlock::Photos => SelectedBlock::Collections,
        }
    }

    fn previous(&mut self) {
        *self = match self {
            SelectedBlock::Collections => SelectedBlock::Photos,
            SelectedBlock::Filters => SelectedBlock::Collections,
            SelectedBlock::Photos => SelectedBlock::Filters,
        }
    }
}

pub struct SearchView<'a> {
    collection_state: ListState,
    filter: Filter,
    files: Vec<&'a File>,
    db: &'a Database,
    file_state: ListState,
    selected_block: SelectedBlock,
    high_res: bool,
    selected_items: HashSet<usize>,
    file_scrollbar_state: ScrollbarState,
    scrollbar_ratio: usize,
}

impl<'a> SearchView<'a> {
    pub fn new(db: &'a Database) -> Self {
        let mut collection_state = ListState::default();
        collection_state.select(Some(0));

        Self {
            collection_state,
            filter: Filter::new(),
            files: Vec::new(),
            db,
            file_state: ListState::default(),
            selected_block: SelectedBlock::Collections,
            high_res: false,
            selected_items: HashSet::new(),
            file_scrollbar_state: ScrollbarState::default(),
            scrollbar_ratio: 1,
        }
    }

    pub fn update_files(&mut self) {
        self.files = self.db.collections()[self.collection_state.selected().unwrap()]
            .folder()
            .iter()
            .filter_file(self.filter.to_filter_option())
            .collect();
        self.file_state.select(None);
        if self.files.len() > u16::MAX as usize {
            self.scrollbar_ratio = 1000;
        } else {
            self.scrollbar_ratio = 1;
        }
        self.file_scrollbar_state = self
            .file_scrollbar_state
            .content_length(self.files.len() / self.scrollbar_ratio)
            .position(0);
    }
}

impl<'a> super::View for SearchView<'a> {
    fn help(&self) -> &'static [(&'static str, &'static str)] {
        &[("Tab", "Change selection"), ("↟↡←↑→↓", "Nav")]
    }

    fn on_begin(&mut self) {
        match self.selected_block {
            SelectedBlock::Collections => {
                if !self.db.collections().is_empty() {
                    self.collection_state.select(Some(0))
                }
            }
            SelectedBlock::Filters => self.filter.state.borrow_mut().select(Some(0)),
            SelectedBlock::Photos => {
                if !self.files.is_empty() {
                    self.file_state.select(Some(0));
                    self.file_scrollbar_state.first();
                }
            }
        }
    }

    fn on_end(&mut self) {
        match self.selected_block {
            SelectedBlock::Collections => {
                if !self.db.collections().is_empty() {
                    self.collection_state
                        .select(Some(self.db.collections().len() - 1))
                }
            }
            SelectedBlock::Filters => self
                .filter
                .state
                .borrow_mut()
                .select(Some(FILTER_NUMBER - 1)),
            SelectedBlock::Photos => {
                if !self.files.is_empty() {
                    self.file_state.select(Some(self.files.len() - 1));
                    self.file_scrollbar_state.last();
                }
            }
        }
    }

    fn on_page_up(&mut self) {
        match self.selected_block {
            SelectedBlock::Collections => (),
            SelectedBlock::Filters => (),
            SelectedBlock::Photos => {
                match self.file_state.selected() {
                    None | Some(0..=9) => {
                        self.file_state.select(Some(0));
                    }
                    Some(current) => {
                        self.file_state.select(Some(current - 10));
                    }
                }
                self.file_scrollbar_state = self
                    .file_scrollbar_state
                    .position(self.file_state.selected().unwrap_or(0) / self.scrollbar_ratio);
            }
        }
    }

    fn on_page_down(&mut self) {
        match self.selected_block {
            SelectedBlock::Collections => (),
            SelectedBlock::Filters => (),
            SelectedBlock::Photos => {
                match self.file_state.selected() {
                    None => {
                        if !self.files.is_empty() {
                            self.file_state.select(Some(0));
                        }
                    }
                    Some(current) => {
                        if current < self.files.len() - 10 {
                            self.file_state.select(Some(current + 10));
                        } else {
                            self.file_state.select(Some(self.files.len() - 1));
                        }
                    }
                }

                self.file_scrollbar_state = self
                    .file_scrollbar_state
                    .position(self.file_state.selected().unwrap_or(0) / self.scrollbar_ratio);
            }
        }
    }

    fn on_up(&mut self) {
        match self.selected_block {
            SelectedBlock::Collections => {
                let current = self.collection_state.selected().unwrap();
                if current == 0 {
                    self.collection_state
                        .select(Some(self.db.collections().len() - 1))
                } else {
                    self.collection_state.select(Some(current - 1))
                }
                self.update_files();
            }
            SelectedBlock::Filters => {
                let current = self.filter.state.borrow().selected().unwrap();
                if current == 0 {
                    self.filter
                        .state
                        .borrow_mut()
                        .select(Some(FILTER_NUMBER - 1))
                } else {
                    self.filter.state.borrow_mut().select(Some(current - 1))
                }
            }
            SelectedBlock::Photos => match self.file_state.selected() {
                None | Some(0) => (),
                Some(current) => {
                    self.file_state.select(Some(current - 1));
                    self.file_scrollbar_state.prev();
                }
            },
        }
    }

    fn on_down(&mut self) {
        match self.selected_block {
            SelectedBlock::Collections => {
                let current = self.collection_state.selected().unwrap();
                if current == self.db.collections().len() - 1 {
                    self.collection_state.select(Some(0))
                } else {
                    self.collection_state.select(Some(current + 1))
                }
                self.update_files();
            }
            SelectedBlock::Filters => {
                let current = self.filter.state.borrow().selected().unwrap();
                if current == FILTER_NUMBER - 1 {
                    self.filter.state.borrow_mut().select(Some(0))
                } else {
                    self.filter.state.borrow_mut().select(Some(current + 1))
                }
            }
            SelectedBlock::Photos => {
                match self.file_state.selected() {
                    None => {
                        if !self.files.is_empty() {
                            self.file_state.select(Some(0));
                        }
                    }
                    Some(current) => {
                        if current < self.files.len() - 1 {
                            self.file_state.select(Some(current + 1));
                        }
                    }
                }

                self.file_scrollbar_state.next();
            }
        }
    }

    fn on_left(&mut self) {
        self.filter
            .current_text_mut()
            .handle(InputRequest::GoToPrevChar);
    }

    fn on_right(&mut self) {
        self.filter
            .current_text_mut()
            .handle(InputRequest::GoToNextChar);
    }

    fn on_char(&mut self, c: char) {
        self.filter
            .current_text_mut()
            .handle(InputRequest::InsertChar(c));
        self.update_files();
    }

    fn on_delete(&mut self, left: bool) {
        self.filter.current_text_mut().handle(if left {
            InputRequest::DeletePrevChar
        } else {
            InputRequest::DeleteNextChar
        });
        self.update_files();
    }

    fn on_tab(&mut self, back: bool) {
        if back {
            self.selected_block.previous();
        } else {
            self.selected_block.next();
        }
    }

    fn on_toggle_res(&mut self) {
        self.high_res = !self.high_res;
    }

    fn on_toggle_select(&mut self) {
        if let Some(current_item) = self.file_state.selected() {
            debug!("Select toggle item {:?}", current_item);
            if self.selected_items.contains(&current_item) {
                self.selected_items.remove(&current_item);
            } else {
                self.selected_items.insert(current_item);
            }
        }
    }

    fn on_copy(&mut self) -> Option<String> {
        if self.selected_items.is_empty() {
            if let Some(index) = self.file_state.selected() {
                Some(self.files[index].filename().to_string())
            } else {
                None
            }
        } else {
            Some(
                self.selected_items
                    .iter()
                    .map(|selected| self.files[*selected])
                    .map(|file| file.filename())
                    .join("\n"),
            )
        }
    }

    fn on_open(&mut self) {
        if let Some(index) = self.file_state.selected() {
            let path = self.files[index].filename();
            if let Err(e) = opener::open(path) {
                warn!("Cannot open `{}`: {}", path, e);
            }
        }
    }

    fn draw(&mut self, f: &mut ratatui::Frame, rect: Rect) {
        let columns = Layout::default()
            .direction(Direction::Horizontal)
            .constraints(
                [
                    Constraint::Percentage(30),
                    Constraint::Percentage(30),
                    Constraint::Percentage(40),
                ]
                .as_ref(),
            )
            .split(rect);

        let left_column = Layout::default()
            .direction(Direction::Vertical)
            .constraints([Constraint::Length(8), Constraint::Percentage(100)].as_ref())
            .split(columns[0]);

        let middle_column = Layout::default()
            .direction(Direction::Vertical)
            .constraints([Constraint::Min(1), Constraint::Length(1)].as_ref())
            .split(columns[1]);

        let right_column = Layout::default()
            .direction(Direction::Vertical)
            .constraints([Constraint::Min(1), Constraint::Length(9)].as_ref())
            .split(columns[2]);

        // Collection list
        let collection_list = create_collection_list(
            self.db.collections(),
            self.selected_block.create_block(SelectedBlock::Collections),
        );
        f.render_stateful_widget(collection_list, left_column[0], &mut self.collection_state);

        // Filtre list
        let filter_table = self.filter.create_table(self.selected_block);
        f.render_stateful_widget(
            filter_table,
            left_column[1],
            &mut *self.filter.state.borrow_mut(),
        );

        f.set_cursor_position((
            left_column[1].x + 16 + self.filter.current_text().cursor() as u16,
            left_column[1].y + 2 + self.filter.state.borrow().selected().unwrap() as u16,
        ));

        // Photos list
        let photos_list_items = self
            .files
            .iter()
            .enumerate()
            .map(|(index, file)| {
                let mut item = ListItem::new(file.filename().file_stem().unwrap().to_string());
                if self.selected_items.contains(&index) {
                    item = item.style(Style::default().bg(Color::Red));
                }
                item
            })
            .collect_vec();
        let photo_list = List::new(photos_list_items)
            .block(self.selected_block.create_block(SelectedBlock::Photos))
            .highlight_style(
                Style::default()
                    .add_modifier(Modifier::BOLD)
                    .fg(Color::Black)
                    .bg(Color::LightGreen),
            )
            .highlight_symbol(">> ");
        f.render_stateful_widget(photo_list, middle_column[0], &mut self.file_state);
        f.render_stateful_widget(
            Scrollbar::default(),
            middle_column[0],
            &mut self.file_scrollbar_state,
        );
        let photo_index_text = Paragraph::new(format!(
            "{} / {}",
            self.file_state.selected().map(|s| s + 1).unwrap_or(0),
            self.files.len()
        ))
        .alignment(ratatui::layout::Alignment::Center);
        f.render_widget(photo_index_text, middle_column[1]);

        // Image preview
        if let Some(file_index) = self.file_state.selected() {
            f.render_widget(
                PreviewWidget::new(self.files[file_index], self.high_res),
                right_column[0],
            );

            // Panel
            let panel_widget = create_panel(Either::Left(self.files[file_index]));
            f.render_widget(panel_widget, right_column[1]);
        }
    }
}
