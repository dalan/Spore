use anyhow::Result;
use shadow_rs::{BuildPattern, ShadowBuilder};

fn main() -> Result<()> {
    ShadowBuilder::builder()
        .build_pattern(BuildPattern::Lazy)
        .build()
        .unwrap();

    if std::env::var("CARGO_CFG_TARGET_OS").unwrap() == "windows" {
        let mut res = winresource::WindowsResource::new();
        res.set_icon("../mushrooms.ico");
        res.compile().unwrap();
    }

    Ok(())
}
